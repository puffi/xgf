module xgf.core.messagebroker;

import xgf.util.hash;

struct MessageBroker
{
	private
	{
		IMessageHandler[Id] mHandlers;
		struct Id
		{
			uint nameHash;
			uint paramHash;
		}
	}
	
	void subscribe(string name, T...)(void delegate(T) dg)
	{
		enum nameHash = HashOf!name;
		enum paramHash = HashOf!T;
		enum id = Id(nameHash, paramHash);
		
		auto ptr = id in mHandlers;
		if(ptr is null)
		{
			auto impl = new MessageHandlerImpl!T;
			mHandlers[id] = impl;
			impl.add(dg);
			return;
		}
		auto handler = cast(MessageHandlerImpl!T)(*ptr);
		assert(handler, "MessageHandlerImpl type id conflict");
		handler.add(dg);
	}
	
	void unsubscribe(string name, T...)(void delegate(T) dg)
	{
		enum nameHash = HashOf!name;
		enum paramHash = HashOf!T;
		enum id = Id(nameHash, paramHash);
		
		if(auto ptr = id in mHandlers)
		{
			auto handler = cast(MessageHandlerImpl!T)(*ptr);
			assert(handler, "MessageHandlerImpl type id conflict");
			handler.remove(dg);
		}
	}
	
	void broadcast(string name, T...)(T msg)
	{
		enum nameHash = HashOf!name;
		enum paramHash = HashOf!T;
		enum id = Id(nameHash, paramHash);
		
		if(auto ptr = id in mHandlers)
		{
			auto handler = cast(MessageHandlerImpl!T)(*ptr);
			assert(handler, "MessageHandlerImpl type id conflict");
			handler.broadcast(msg);
		}
	}
}

interface IMessageHandler {}

class MessageHandlerImpl(T...) : IMessageHandler
{
	import xgf.util.array;
	
	private
	{
		DynamicArray!(void delegate(T)) mCallbacks;
	}
	
	void add(void delegate(T) dg)
	{
		mCallbacks.pushBack(dg);
	}
	
	void remove(void delegate(T) dg)
	{
		import std.algorithm : swap;
		foreach(i, callback; mCallbacks)
		{
			if(callback == dg)
			{
				swap(mCallbacks[i], mCallbacks.back);
				mCallbacks.popBack();
				break;
			}
		}
	}
	
	void broadcast(T msg)
	{
		foreach(cb; mCallbacks)
		{
			cb(msg);
		}
	}
}
