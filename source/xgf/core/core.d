module xgf.core.core;

import xgf.core.messagebroker;
import xgf.util.alloc : gc;

@gc
struct Core
{
	private
	{
		MessageBroker mMsgBroker;
	}
	
	void subscribe(string name, T...)(void delegate(T) dg) { mMsgBroker.subscribe!(name, T)(dg); }
	void subscribe(string name, T...)(void function(T) func) { import std.functional: toDelegate; mMsgBroker.subscribe!(name, T)(func.toDelegate); }
	
	void unsubscribe(string name, T...)(void delegate(T) dg) { mMsgBroker.unsubscribe!(name, T)(dg); }
	void unsubscribe(string name, T...)(void function(T) func) { import std.functional: toDelegate; mMsgBroker.unsubscribe!(name, T)(func.toDelegate); }
	
	void broadcast(string name, T...)(T msg) { mMsgBroker.broadcast!(name, T)(msg); }
}
