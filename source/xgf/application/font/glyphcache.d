module xgf.application.font.glyphcache;

import xgf.application.font.glyph;

struct GlyphCache
{
	Glyph*[GlyphIndex] glyphs;
	Glyph* first;
	Glyph* last;
}

struct GlyphIndex
{
	dchar c;
	uint size;
	uint font;
	
	auto opCmp(GlyphIndex rhs) const
	{
		int f = font - rhs.font;
		if(f != 0)
			return f;
		else
		{
			int s = size - rhs.size;
			if(s != 0)
				return s;
			else
			{
				return c - rhs.c;
			}
		}
	}
}
