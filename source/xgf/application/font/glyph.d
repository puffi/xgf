module xgf.application.font.glyph;

import xgf.math;

struct Glyph
{
	Glyph* previous;
	Glyph* next;
	dchar c;
	int width;
	int height;
	int bearingX;
	int bearingY;
	int advanceX;
	int ascender;
	int descender;
	int linespace;
	ivec4 rect;
	vec4 uv;
}