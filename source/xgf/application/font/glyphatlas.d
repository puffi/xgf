module xgf.application.font.glyphatlas;

import xgf.util;
import xgf.util.binpack;
import xgf.graphics;
import xgf.math;

struct GlyphAtlas
{
	private
	{
		Skyline mMap;
		Array!ubyte mTexData;
		uint mWidth;
		uint mHeight;
		
		Graphics* mGraphics;
		TextureHandle mTexture;
		
		bool mOutOfDate;
	}
	
	this(uint width, uint height, Graphics* graphics)
	{
		mMap.clear(width, height);
		
		mTexData = Array!ubyte(width * height);
		
		mWidth = width;
		mHeight = height;
		mGraphics = graphics;
		
		mTexture = mGraphics.createTexture2D(mTexData[], mWidth, mHeight, Format.r8);
	}
	
	bool pack(uint width, uint height, ubyte[] data, ref ivec4 rect, ref vec4 uv)
	{
		auto r = mMap.insert(width, height, LevelChoiceHeuristic.LevelBottomLeft);
		rect.x = r.x;
		rect.y = r.y;
		rect.z = r.width;
		rect.w = r.height;
		if(rect.z <= 0 && rect.w <= 0)
			return false;
		
		uv.x = (rect.x + 0.5f) / mWidth;
		uv.y = (rect.y + 0.5f) / mHeight;
		uv.z = (rect.x + width - 0.5f) / mWidth;
		uv.w = (rect.y + height - 0.5f) / mHeight;
		
		foreach(h; 0..height)
		{
			auto fh = height - h - 1;
			mTexData[][rect.x + (rect.y + h) * mWidth .. rect.x + width + (rect.y + h) * mWidth] = data[fh * width .. (fh+1) * width];
			mGraphics.updateTexture2D(mTexture, mTexData[][rect.x + (rect.y + h) * mWidth .. rect.x + width + (rect.y + h) * mWidth], rect.z, 1, rect.x, h);
		}
		
// 		mGraphics.updateTexture2D(mTexture, data, rect.width, rect.height, rect.x, rect.y);
		
		return true;
	}

	
	@property auto texture() { return mTexture; }
}
