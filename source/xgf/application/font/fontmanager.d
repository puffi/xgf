module xgf.application.font.fontmanager;

import derelict.freetype.ft;
import xgf.application.font.glyphatlas;
import xgf.application.font.glyphcache;
import xgf.application.font.glyph;
import xgf.application.font.font;

import xgf.graphics;
import xgf.util;

@gc
struct FontManager
{
	private
	{
		Font*[uint] mFonts;
		GlyphAtlas* mAtlas;
		GlyphCache* mCache;
		uint mWidth;
		uint mHeight;
		uint mPages;
		FT_Library mLibrary;
	}
	
	this(uint width, uint height, uint pages, Graphics* graphics)
	{
		mWidth = width;
		mHeight = height;
		mPages = pages;
		
		auto err = FT_Init_FreeType(&mLibrary);
		if(err)
		{
			import std.stdio;
			writeln("Error initializing FreeType: ", err);
		}
		
		mAtlas = alloc!GlyphAtlas(width, height, graphics);
		mCache = alloc!GlyphCache();
	}

	~this()
	{
		FT_Done_Library(mLibrary);

		foreach(font; mFonts.byValue)
		{
			free(font);
		}
		free(mCache);
		free(mAtlas);
	}
	
	auto register(string fontname)
	{
		assert(!(fontname.hash in mFonts));
		assert(mLibrary);
		
		auto font = alloc!Font(mAtlas, mCache, mLibrary, fontname);
		mFonts[fontname.hash] = font;
		return font;
	}
	
	@property auto atlas() { return mAtlas; }
}

static this()
{
	DerelictFT.load();
}

static ~this()
{
	DerelictFT.unload();
}