module xgf.application.font.font;

import derelict.freetype.ft;
import xgf.application.font.glyph;
import xgf.application.font.glyphatlas;
import xgf.application.font.glyphcache;
import xgf.util;

struct Font
{
	private
	{
		GlyphAtlas* mAtlas;
		GlyphCache* mCache;
		FT_Library mLibrary;
		FT_Face mFace;
		
		string mFontName;
		uint mHash;
	}
	
	this(GlyphAtlas* atlas, GlyphCache* cache, FT_Library lib, string fontname)
	{
		mAtlas = atlas;
		mCache = cache;
		mLibrary = lib;
		mFontName = fontname;
		mHash = fontname.hash;
		
		Array!char arr = Array!char(fontname.length + 1);
// 		arr.resize(cast(uint)fontname.length+1);
		arr[][0..arr.size-1] = fontname[];
		arr[arr.size-1] = '\0';
		auto err = FT_New_Face(mLibrary, arr[].ptr, 0, &mFace);
		if(err)
		{
			import std.stdio;
			writeln("Error creating face: ", err);
			writeln("Font: ", fontname);
		}
	}
	
	auto getGlyph(uint size, dchar c)
	{
		import std.stdio;
		if(auto ptr = GlyphIndex(c, size, mHash) in mCache.glyphs)
		{
			return *ptr;
		}
		
		FT_Set_Pixel_Sizes(mFace, 0, size);
		auto idx = FT_Get_Char_Index(mFace, c);
		if(idx == 0)
		{
			writeln("Error, character not found in font face: ", c);
		}
		auto err = FT_Load_Glyph(mFace, idx, FT_LOAD_DEFAULT);
		if(err)
		{
			writeln("Error loading glyph: ", err);
			return null;
		}
		err = FT_Render_Glyph(mFace.glyph, FT_RENDER_MODE_NORMAL);
		if(err)
		{
			writeln("Error rendering glyph: ", err);
			return null;
		}
		
		auto glyph = alloc!Glyph();
		glyph.c = c;
		glyph.width = mFace.glyph.bitmap.width;
		glyph.height = mFace.glyph.bitmap.rows;
		mAtlas.pack(glyph.width, glyph.height, mFace.glyph.bitmap.buffer[0..glyph.width*glyph.height], glyph.rect, glyph.uv);
// 		glyph.rect = mAtlas.map.insert(cast(int)glyph.width, cast(int)glyph.height, LevelChoiceHeuristic.LevelBottomLeft);
		glyph.bearingX = mFace.glyph.bitmap_left;
		glyph.bearingY = mFace.glyph.bitmap_top;
		glyph.advanceX = cast(int)mFace.glyph.advance.x >> 6;
		glyph.ascender = cast(int)mFace.size.metrics.ascender >> 6;
		glyph.descender = cast(int)mFace.size.metrics.descender >> 6;
		glyph.linespace = cast(int)mFace.size.metrics.height >> 6;
// 		glyph.bearingX = cast(int)mFace.glyph.metrics.horiBearingX >> 6;
// 		glyph.bearingY = cast(int)mFace.glyph.metrics.horiBearingY >> 6;
// 		glyph.advanceX = cast(int)mFace.glyph.metrics.horiAdvance >> 6;
		mCache.glyphs[GlyphIndex(c, size, mHash)] = glyph;
		
		return glyph;
	}

	@property auto atlas() { return mAtlas; }
	@property void atlas(GlyphAtlas* atlas) { mAtlas = atlas; }
	@property auto cache() { return mCache; }
	@property void cache(GlyphCache* cache) { mCache = cache; }
}
