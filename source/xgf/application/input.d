module xgf.application.input;

import xgf.core.core;
import xgf.graphics.window;

import std.stdio;

alias Scancode = xgf.graphics.window.Scancode;

struct Input
{
	private
	{
		bool[Scancode.num_scancodes] mScancodes;
		Core* mCore;
		
		void onKeyDown(Scancode scancode, Modifier m)
		{
			mScancodes[scancode] = true;
		}
		
		void onKeyUp(Scancode scancode, Modifier m)
		{
			mScancodes[scancode] = false;
		}
	}
	
	this(Core* core)
	{
		mCore = core;
		
		mCore.subscribe!"KeyDown"(&onKeyDown);
		mCore.subscribe!"KeyUp"(&onKeyUp);
	}
	
	~this()
	{
		mCore.unsubscribe!"KeyDown"(&onKeyDown);
		mCore.unsubscribe!"KeyUp"(&onKeyUp);
	}
	
	bool isKeyDown(Scancode s)
	{
		return mScancodes[s];
	}
}
