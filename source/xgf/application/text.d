module xgf.application.text;

import xgf.graphics.graphics;
import xgf.graphics.vertexbuffer;
import xgf.graphics.indexbuffer;
import xgf.graphics.vertexdeclaration;

import xgf.math : vec2, vec3;

import xgf.rendering.font;

import xgf.util.array;

struct Text
{
	private
	{
		Font* mFont;
		Graphics* mGraphics;
		string mText;
		VertexBufferHandle mVertexBuffer;
		IndexBufferHandle mIndexBuffer;
	}
	
	this(Font* font, Graphics* graphics)
	{
		mFont = font;
		mGraphics = graphics;
	}
	
// 	@property string text()
// 	{
// 		return mText;
// 	}
// 	@property void text(in string t)
// 	{
// 		mText = t;
// 		updateVertices();
// 	}
	
	void setText(T...)(T args)
	{
		updateVertices(args);
	}
	
	private
	{
		void updateVertices(T...)(T args)
		{
			static DynamicArray!Vertex vertices;
			static Vertex[4] charQuad;
			static DynamicArray!uint indices;
			vertices.length = 0;
			indices.length = 0;
			
			float size = mFont.size;
			float scale = mFont.scale;
			float baseline = mFont.descent;
			float advanceX = 0;
			uint index = 0;
			
			import std.utf : byDchar;
			import std.range : enumerate;
			foreach(i, dchar c; args.combineArguments.enumerate)
			{
				import std.stdio;
				writeln(i, " ", c, " ", cast(uint)c);
				auto glyph = mFont.getGlyph(c);
				
				charQuad[0] = Vertex(vec3(i * size + glyph.xMin + glyph.leftBearing, -glyph.yMax - baseline, 0), vec2(glyph.uv.x, glyph.uv.y));
				charQuad[1] = Vertex(vec3(i * size + glyph.xMin + glyph.width + glyph.leftBearing, -glyph.yMax - baseline, 0), vec2(glyph.uv.z, glyph.uv.y));
				charQuad[2] = Vertex(vec3(i * size + glyph.xMin + glyph.width + glyph.leftBearing, -glyph.yMax + glyph.height - baseline, 0), vec2(glyph.uv.z, glyph.uv.w));
				charQuad[3] = Vertex(vec3(i * size + glyph.xMin + glyph.leftBearing, -glyph.yMax + glyph.height - baseline, 0), vec2(glyph.uv.x, glyph.uv.w));
				
				vertices.pushBack(charQuad[0]);
				vertices.pushBack(charQuad[1]);
				vertices.pushBack(charQuad[2]);
				vertices.pushBack(charQuad[3]);
				
				indices.pushBack(index+0);
				indices.pushBack(index+1);
				indices.pushBack(index+2);
				indices.pushBack(index+2);
				indices.pushBack(index+3);
				indices.pushBack(index+0);
				
				advanceX += glyph.advance;
				index += 4;
			}
			
			if(mVertexBuffer != VertexBufferHandle.invalid)
				mGraphics.destroyVertexBuffer(mVertexBuffer);
			if(mIndexBuffer != IndexBufferHandle.invalid)
				mGraphics.destroyIndexBuffer(mIndexBuffer);
			
			mVertexBuffer = mGraphics.createVertexBuffer(vertices[]);
			mIndexBuffer = mGraphics.createIndexBuffer(indices[]);
			mFont.update();
		}
	}
	
	@property auto vb() { return mVertexBuffer; }
	@property auto ib() { return mIndexBuffer; }
}

private
{
	struct Vertex
	{
		@Attribute(0, Attribute.Type.f32, 3, false)
		vec3 pos;
		@Attribute(1, Attribute.Type.f32, 2, false)
		vec2 uv;
	}
	
	auto combineArguments(T...)(T args)
	{
		static if(T.length == 1)
			return args[0].toRange;
		else
		{
			import std.range : chain;
			import std.typecons : Tuple, tuple;
			
			Tuple!(RangeTuple!T) ret;
			foreach(i, arg; args)
			{
				ret[i] = arg.toRange;
			}
			
			pragma(msg, typeof(chain(ret.expand)));
			return chain(ret.expand);
		}
	}
	
	auto toRange(T)(T arg)
	{
		import std.traits : isIntegral, isFloatingPoint, isSomeString;
		import std.conv : toChars;
		
		static if(isSomeString!T)
		{
			return arg;
		}
		else static if(isIntegral!T)
		{
			return arg.toChars;
		}
		else static assert(false);
	}
	
	template RangeTuple(T...)
	{
		import std.typetuple;
		import std.traits : ReturnType;
		static if(T.length == 0)
			alias RangeTuple = TypeTuple!();
		else
			alias RangeTuple = TypeTuple!(ReturnType!(toRange!(T[0])), RangeTuple!(T[1..$]));
	}
}
