module xgf.application.timermanager;

import xgf.core;
import xgf.util;

@gc
struct TimerManager
{
	private
	{
		Core* mCore;
		TimerEvent[string] mEvents;
	}
	
	this(Core* core)
	{
		mCore = core;
		
		mCore.subscribe!"UpdateTimers"(&onUpdateTimers);
	}
	
	void register(string eventName, Time t, Callback cb, bool periodic = false)
	{
		mEvents[eventName] = TimerEvent(t, cb, periodic);
	}
	
	void unregister(string eventName)
	{
		mEvents.remove(eventName);
	}
	
	bool contains(string eventName)
	{
		return !!(eventName in mEvents);
	}
	
	private
	{
		void onUpdateTimers(Time dt)
		{
			foreach(key, ref evt; mEvents)
			{
				if(evt.update(dt))
				{
					if(!evt.periodic)
					{
						mEvents.remove(key);
					}
				}
			}
		}
		
		alias Callback = void delegate();

		struct TimerEvent
		{
			Callback cb;
			Time eventTime;
			Time timeSince;
			bool periodic;
			
			this(Time eventTime, Callback cb, bool periodic)
			{
				this.cb = cb;
				this.eventTime = eventTime;
				this.periodic = periodic;
				timeSince = time(0);
			}
			
			bool update(Time dt)
			{
				timeSince = timeSince + dt;
				bool fired = false;
				while(timeSince >= eventTime)
				{
					fire();
					timeSince = timeSince - eventTime;
					fired = true;
				}
				return fired;
			}
			
			void fire()
			{
				cb();
			}
		}
	}
}