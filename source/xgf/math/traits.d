module xgf.math.traits;

import std.traits;

auto allScalarTypes(T...)()
{
	bool ret = true;
	foreach(type; T)
		ret &= isScalarType!type;
	return ret;
}

template CTRange(size_t end)
{
	alias CTRange = CTRange!(0, end);
}

template CTRange(size_t start, size_t end)
{
	static assert(start <= end);
	import std.typetuple;
	static if(start == end)
		alias CTRange = TypeTuple!();
	else
		alias CTRange = TypeTuple!(start, CTRange!(start+1, end));
}

template CTIndex(size_t col, size_t row, size_t cols, size_t rows)
{
	enum CTIndex = col * rows + row;
}
