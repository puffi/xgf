module xgf.math.matrix;

import std.traits;
import xgf.math.traits;
import xgf.math.vector;

struct Matrix(T, size_t Cols, size_t Rows) if(isScalarType!T)
{
	alias type = T;
	alias cols = Cols;
	alias rows = Rows;
	
	private
	{
		Vector!(T, Rows)[Cols] mPayload;
	}
	
	this(Arg)(Arg arg) if(isScalarType!Arg)
	{
		foreach(ref v; mPayload)
			v = cast(T)arg;
	}
	
	this(Args...)(Args args) if(allScalarTypes!Args)
	{
		static assert(Args.length == Cols * Rows, "Dimensions don't match.");
		
		foreach(c; CTRange!(0, Cols))
		{
			foreach(r; CTRange!(0, Rows))
			{
				mPayload[c][r] = args[CTIndex!(c, r, Cols, Rows)];
			}
		}
	}
	
	static if(Cols == Rows)
	{
		static @property typeof(this) identity()
		{
			typeof(this) ret;
			foreach(c; CTRange!(0, Cols))
				ret[c][c] = cast(T)(1);
			return ret;
		}
	}
	
	ref auto opIndex(size_t idx)
	{
		return mPayload[idx];
	}
	
	auto opIndex(size_t idx) const
	{
		return mPayload[idx];
	}
	
	void opIndexAssign(Vector!(T, Rows) v, size_t idx)
	{
		mPayload[idx] = v;
	}
	
	@property auto payload() const { return mPayload; }
	@property auto ptr() const { return cast(T*)mPayload.ptr; }
	
	auto opBinary(string s)(in auto ref Matrix!(T, Cols, Rows) rhs) const if(s == "+" || s == "-")
	{
		Matrix!(T, Cols, Rows) mat;
		foreach(c; CTRange!(0, Cols))
			foreach(r; CTRange!(0, Rows))
				mixin("mat.mPayload[c][r] = mPayload[c][r]" ~ s ~ "rhs.mPayload[c][r];");
		return mat;
	}
	
	auto opBinary(string s, Arg)(Arg val) const if((s == "*" || s == "/") && isScalarType!Arg)
	{
		Matrix!(T, Cols, Rows) mat;
		foreach(c; CTRange!(0, Cols))
			foreach(r; CTRange!(0, Rows))
				mixin("mat.mPayload[c][r] = mPayload[c][r]" ~ s ~ "val;");
		return mat;
	}

	auto opBinaryRight(string s, Arg)(Arg val) const if((s == "*" || s == "/") && isScalarType!Arg)
	{
		return opBinary!(s)(val);
	}

	auto opBinary(string s)(in auto ref Vector!(T, Rows) rhs) const if(s == "*")
	{
		Vector!(T, Rows) vec;
		foreach(c; CTRange!(0, Cols))
			foreach(r; CTRange!(0, Rows))
				vec[c] = vec[c] + (mPayload[c][r] * rhs[c]); 
			
		return vec;
	}
	
	auto opBinary(string s, Mat)(in auto ref Mat rhs) const if(s == "*" && is(Mat == Matrix!(T, rCols, Cols), size_t rCols))
	{
		Matrix!(T, rhs.cols, Rows) mat;
		foreach(rC; CTRange!(0, rhs.cols))
			foreach(r; CTRange!(0, Rows))
				foreach(c; CTRange!(0, Cols))
					mat[rC][r] = mat[rC][r] + (mPayload[c][r] * rhs.mPayload[rC][c]);

		return mat;
	}
}
