module xgf.math.angle;

import std.traits;

struct Radian(T) if(isScalarType!T)
{
	alias type = T;
	
	private
	{
		T mPayload = 0;
	}
	
	this(T arg)
	{
		mPayload = arg;
	}
	
	this(Degree!T arg)
	{
		import xgf.math.functions;
		mPayload = arg.toRadian.value;
	}
	
	auto opUnary(string op : "-")() const
	{
		return Radian!T(-mPayload);
	}
	
	auto opBinary(string op)(Radian!T rhs) const
	{
		mixin("return Radian!T(mPayload "~op~" rhs.value);");
	}
	
	auto opBinary(string op)(T rhs) const
	{
		mixin("return Radian!T(mPayload "~op~" rhs);");
	}
	
	auto opBinaryRight(string op)(T rhs) const
	{
		return opBinary!op(rhs);
	}
	
	@property auto value() const { return mPayload; }
}

struct Degree(T) if(isScalarType!T)
{
	alias type = T;
	
	private
	{
		T mPayload = 0;
	}
	
	this(T arg)
	{
		mPayload = arg;
	}
	
	this(Radian!T arg)
	{
		import xgf.math.functions;
		mPayload = arg.toDegree.value;
	}
	
	auto opUnary(string op : "-")() const
	{
		return Degree!T(-mPayload);
	}
	
	auto opBinary(string op)(Degree!T rhs) const
	{
		mixin("return Degree!T(mPayload "~op~" rhs.value);");
	}
	
	auto opBinary(string op)(T rhs) const
	{
		mixin("return Degree!T(mPayload "~op~" rhs);");
	}
	
	auto opBinaryRight(string op)(T rhs) const
	{
		return opBinary!op(rhs);
	}
	
	@property auto value() const { return mPayload; }
}
