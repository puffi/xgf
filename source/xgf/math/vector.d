module xgf.math.vector;

import xgf.math.traits;

import std.traits;

struct Vector(T, size_t Dims) if(isScalarType!T)
{
	alias type = T;
	alias size = Dims;
	
	private
	{
		T[Dims] mPayload = 0;
	}
	
	this(Arg)(Arg arg) if(isScalarType!Arg)
	{
		mPayload[] = cast(T)arg;
	}
	
	this(Args...)(Args args) if(allScalarTypes!Args)
	{
		static assert(args.length <= Dims);
		foreach(i, arg; args)
			mPayload[i] = cast(T)arg;
	}
	
	this(Arg)(Arg arg) if(is(Arg == Vector!(T2, Dims), T2))
	{
		foreach(i; CTRange!(0, Arg.size))
		{
			mPayload[i] = cast(Type)arg.mPayload[i];
		}
	}

	this(Args...)(Args args) if(!allScalarTypes!Args)
	{
		static assert(matchesDimensions!(Dims, Args), "Dimensions don't match.");
		foreach(i, arg; args)
		{
			static if(isScalarType!(typeof(arg)))
			{
				mPayload[argSize!(Args[0..i])] = cast(T)arg;
			}
			else static if(is(typeof(arg) == struct))
			{
				foreach(idx; CTRange!(0, arg.size))
					mPayload[argSize!(Args[0..i]) + idx] = cast(T)arg.mPayload[idx];
			}
			else static assert(false, "Type "~__traits(identifier, typeof(arg))~" not supported.");
		}
	}
	
	@property auto payload() const { return mPayload; }
	@property auto ptr() const { return mPayload.ptr; }
	
	void opAssign(T arg)
	{
		mPayload[] = arg;
	}
	
	void opAssign(T[Dims] arg)
	{
		mPayload[] = arg;
	}
	
	auto opIndex(size_t idx) const
	{
		return mPayload[idx];
	}
	
	auto opIndexAssign(T arg, size_t idx)
	{
		mPayload[idx] = arg;
	}
	
	auto opSlice() const
	{
		return mPayload[];
	}
	
	auto opSlice(size_t start, size_t end) const
	{
		return mPayload[start..end];
	}
	
	auto opUnary(string op : "-")() const
	{
		Vector!(T, Dims) ret;
		foreach(i; CTRange!(0, Dims))
			ret[i] = -mPayload[i];
		return ret;
	}
	
	auto opBinary(string op, Arg)(Arg arg) const if(isScalarType!Arg)
	{
		Vector!(T, Dims) ret = this;
		foreach(i; CTRange!(0, Dims))
			mixin("ret[i] = mPayload[i] "~op~" arg;");
		return ret;
	}
	
	auto opBinaryRight(string op, Arg)(Arg arg) const if(isScalarType!Arg && (op == "+" || op == "*"))
	{
		return opBinary!op(arg);
	}
	
	auto opBinary(string op, Arg)(Arg arg) const if(is(Arg == Vector!(T, Dims)))
	{
		Vector!(T, Dims) ret = this;
		foreach(i; CTRange!(0, Dims))
			mixin("ret[i] = cast(T)(mPayload[i] "~op~" arg[i]);");
		return ret;
	}
	
	/// Accessors
	
	static if(Dims >= 1)
	{
		/**
		* Returns the first element.
		*/
		@property T x() const { return mPayload[0]; }
		
		/**
		* Sets the first element.
		*/
		@property void x(T val) { mPayload[0] = val; }
		alias r = x;
		alias s = x;
	}
	static if(Dims >= 2)
	{
		/**
		* Returns the second element.
		*/
		@property T y() const { return mPayload[1]; }
		/**
		* Sets the second element.
		*/
		@property void y(T val) { mPayload[1] = val; }
		alias g = y;
		alias t = y;
	}
	static if(Dims >= 3)
	{
		/**
		* Returns the third element.
		*/
		@property T z() const { return mPayload[2]; }
		/**
		* Sets the third element.
		*/
		@property void z(T val) { mPayload[2] = val; }
		alias b = z;
		alias p = z;
	}
	static if(Dims == 4)
	{
		/**
		* Returns the fourth element.
		*/
		@property T w() const { return mPayload[3]; }
		/**
		* Sets the fourth element.
		*/
		@property void w(T val) { mPayload[3] = val; }
		alias a = w;
		alias q = w;
	}
	
	/// Swizzling
	
	@property auto opDispatch(string s)() const if(allValidAccessors!(s, Dims))
	{
		Vector!(T, s.length) ret;
		foreach(i; CTRange!(0, s.length))
		{
			mixin("ret[i] = this."~s[i]~";");
		}
		return ret;
	}
	
	@property void opDispatch(string s)(T arg) if(allValidAccessors!(s, Dims)
			&& hasNoDuplicates!s)
	{
		foreach(i; CTRange!(0, s.length))
			mixin(s[i]~" = arg;");
	}
	
	@property void opDispatch(string s, Arg)(in auto ref Arg arg) if(allValidAccessors!(s, Dims)
			&& hasNoDuplicates!s && is(Arg == Vector!(T, dim), size_t dim))
	{
		static assert(s.length <= Arg.size, "Swizzle too long.");
		static assert(s.length <= Dims, "Swizzle too long.");
		foreach(i; CTRange!(0, s.length))
		{
			mixin(s[i]~" = arg[i];");
		}
	}
}

private
{
	bool matchesDimensions(size_t Dims, U...)()
	{
		size_t dimensions = 0;
		
		foreach(type; U)
		{
			static if(isScalarType!type)
				dimensions += 1;
			else static if(is(type == Vector!(T1, Dims1), T1, size_t Dims1))
				dimensions += type.size;
			else static assert(false, "Type "~__traits(identifier, type)~" not supported.");
		}
		return Dims == dimensions;
	}

	template isValidAccessor(char acc, size_t Dims)
	{
		static if((acc == 'x' || acc == 'r' || acc == 's') && Dims >= 1)
			enum isValidAccessor = true;
		else static if((acc == 'y' || acc == 'g' || acc == 't') && Dims >= 2)
			enum isValidAccessor = true;
		else static if((acc == 'z' || acc == 'b' || acc == 'p') && Dims >= 3)
			enum isValidAccessor = true;
		else static if((acc == 'w' || acc == 'a' || acc == 'q') && Dims >= 4)
			enum isValidAccessor = true;
		else static assert(false, acc ~ " is not a valid accessor.");
	}

	template allValidAccessors(string accessors, size_t Dims)
	{
		static if(accessors.length == 0)
			enum allValidAccessors = true;
		else
			enum allValidAccessors = isValidAccessor!(accessors[0], Dims) && allValidAccessors!(accessors[1..$], Dims);
	}

	bool hasNoDuplicates(string s)()
	{
		foreach(i; CTRange!(s.length))
		{
			foreach(i2; CTRange!(i+1, s.length))
				static assert(s[i] != s[i2], "Contains duplicate: "~s[i]);
		}
		return true;
	}
	
	size_t argSize(Args...)()
	{
		size_t ret = 0;
		foreach(arg; Args)
		{
			static if(isScalarType!arg)
				ret += 1;
			else static if(is(arg == Vector!(T1, Dims1), T1, size_t Dims1))
				ret += arg.size;
		}
		return ret;
	}
}
