module xgf.math.quaternion;

import std.traits;
import xgf.math.traits;
import xgf.math.vector;

struct Quaternion(T) if(isFloatingPoint!T)
{
	alias type = T;
	
	private
	{
		T[4] mPayload = [1, 0, 0, 0];
	}
	
	this(Args...)(Args vals) if(allScalarTypes!Args)
	{
		static assert(Args.length == 4);
		foreach(i; CTRange!(0, Args.length))
			mPayload[i] = vals[i];
	}
	
	@property w(Arg)(Arg val) if(isScalarType!Arg) { mPayload[0] = cast(Arg)val; }
	@property w() const { return mPayload[0]; }
	@property x(Arg)(Arg val) if(isScalarType!Arg) { mPayload[1] = cast(Arg)val; }
	@property x() const { return mPayload[1]; }
	@property y(Arg)(Arg val) if(isScalarType!Arg) { mPayload[2] = cast(Arg)val; }
	@property y() const { return mPayload[2]; }
	@property z(Arg)(Arg val) if(isScalarType!Arg) { mPayload[3] = cast(Arg)val; }
	@property z() const { return mPayload[3]; }
	
	auto opUnary(string s : "-")() const
	{
		return Quaternion!T(w, -x, -y, -z);
	}
	
	auto opBinary(string s)(Quaternion!T rhs) const if(s == "*")
	{
		return Quaternion!T
		(
			w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z,
			w * rhs.x + x * rhs.w + y * rhs.z - z * rhs.y,
			w * rhs.y - x * rhs.z + y * rhs.w + z * rhs.x,
			w * rhs.z + x * rhs.y - y * rhs.x + z * rhs.w
		);
	}
	
	auto opBinary(string op)(Quaternion!T rhs) const if(op == "+" || op == "-")
	{
		return Quaternion!T
		(
			mixin("w "~op~" rhs.w"),
			mixin("x "~op~" rhs.x"),
			mixin("y "~op~" rhs.y"),
			mixin("z "~op~" rhs.z")
		);
	}

	auto opBinary(string s)(Vector!(T, 3) rhs) const if(s == "*")
	{
		Quaternion!T vecQuat = Quaternion!T(0, rhs.x, rhs.y, rhs.z);
		vecQuat = this * vecQuat * (-this);
		return Vector!(T, 3)(vecQuat.x, vecQuat.y, vecQuat.z);
	}

	auto opBinaryRight(string s)(Vector!(T, 3) rhs) const if(s == "*")
	{
		return opBinary!(s)(rhs);
	}
	
	auto opBinary(string op, Arg)(Arg rhs) const if((op == "+" || op == "-" || op == "*" || op == "/") && isScalarType!Arg)
	{
		mixin("return Quaternion!T(w "~op~" rhs, x "~op~" rhs, y "~op~" rhs, z "~op~" rhs);");
	}
	
	auto opBinaryRight(string op, Arg)(Arg rhs) const if((op == "+" || op == "-" || op == "*" || op == "/") && isScalarType!Arg)
	{
		return opBinary!op(rhs);
	}
	
	static @property auto identity() { return Quaternion!T(); }
}
