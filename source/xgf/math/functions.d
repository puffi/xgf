module xgf.math.functions;

import std.traits;
import xgf.math.traits;
import xgf.math.vector;
import xgf.math.matrix;
import xgf.math.quaternion;
import xgf.math.angle;

// vector functions

auto rotate(Vec, U)(Vec vec, U rot) if(is(Vec == Vector!(W, 2), W) && is(U == Rad!T, T))
{
	return rot.rotate(vec);
}

auto normalize(Arg)(in auto ref Arg v) if(is(Arg == Vector!(T, Dims), T, size_t Dims))
{
	import std.math : sqrt;
	auto mag = dot(v, v).sqrt;
	return v / mag;
}

auto dot(Arg)(in auto ref Arg v1, in auto ref Arg v2) if(is(Arg == Vector!(T, Dims), T, size_t Dims))
{
	Arg.type value = 0;
	foreach(i; CTRange!(0, Arg.size))
		value += v1[i] * v2[i];
	return value;
}

auto magnitude2(Arg)(in auto ref Arg v) if(is(Arg == Vector!(T, Dims), T, size_t Dims))
{
	return dot(v, v);
}

auto magnitude(Arg)(in auto ref Arg v) if(is(Arg == Vector!(T, Dims), T, size_t Dims))
{
	import std.math : sqrt;
	return dot(v, v).sqrt;
}

auto cross(T)(in auto ref T vec1, in auto ref T vec2) if(is(T == Vector!(U, 2), U))
{
	return vec1.x * vec2.y - vec1.y * vec2.x;
}

auto cross(T)(in auto ref T vec1, in auto ref T vec2) if(is(T == Vector!(U, 3), U))
{
	return T(vec1.y * vec2.z - vec1.z * vec2.y, vec1.z * vec2.x - vec1.x * vec2.z, vec1.x * vec2.y - vec1.y * vec2.x);
}

auto negate(T)(in auto ref T v) if(is(T == Vector!(U, dims), U, size_t dims))
{
	return -v;
}

auto abs(T)(in auto ref T v) if(is(T == Vector!(U, dims), U, size_t dims))
{
	import std.math : abs;
	T ret;
	foreach(i; CTRange!(0, T.size))
	{
		ret[i] = abs(v[i]);
	}
	return ret;
}

auto min(T)(in auto ref T v1, in auto ref T v2) if(is(T == Vector!(U, dims), U, size_t dims))
{
	import std.algorithm : min;
	T ret;
	foreach(i; CTRange!(0, T.size))
	{
		ret[i] = min(v1[i], v2[i]);
	}
	return ret;
}

auto max(T)(in auto ref T v1, in auto ref T v2) if(is(T == Vector!(U, dims), U, size_t dims))
{
	import std.algorithm : max;
	T ret;
	foreach(i; CTRange!(0, T.size))
	{
		ret[i] = max(v1[i], v2[i]);
	}
	return ret;
}

T clamp(T)(in auto ref T v, in auto ref T lower, in auto ref T upper) if(is(T == Vector!(U, dims), U, size_t dims))
{
	T ret;
	foreach(i; CTRange!(0, T.size))
	{
		ret[i] = v[i].clamp(lower[i], upper[i]);
	}
	return ret;
}

T clamp(T)(in T v, in T lower, in T upper) if(isScalarType!T)
{
	T ret;
	if(v < lower)
		ret = lower;
	else if(v > upper)
		ret = upper;
	else
		ret = v;
	return ret;
}

// matrix functions

auto rotate(T)(in auto ref T quat) if(is(T == Quaternion!U, U))
{
	Matrix!(T.type, 4, 4) mat = Matrix!(T.type, 4, 4).identity;

	mat[0][0] = 1 - 2 * quat.y * quat.y - 2 * quat.z * quat.z;
	mat[0][1] = 2 * quat.x * quat.y + 2 * quat.w * quat.z;
	mat[0][2] = 2 * quat.x * quat.z - 2 * quat.w * quat.y;
	mat[1][0] = 2 * quat.x * quat.y - 2 * quat.w * quat.z;
	mat[1][1] = 1 - 2 * quat.x * quat.x - 2 * quat.z * quat.z;
	mat[1][2] = 2 * quat.y * quat.z + 2 * quat.w * quat.x;
	mat[2][0] = 2 * quat.x * quat.z + 2* quat.w * quat.y;
	mat[2][1] = 2 * quat.y * quat.z - 2 * quat.w * quat.x;
	mat[2][2] = 1 - 2 * quat.x * quat.x - 2 * quat.y * quat.y;

	return mat;
}

auto rotate(Mat, T)(in auto ref Mat m, in auto ref T quat) if(is(Mat == Matrix!(U, cols, rows), U, size_t cols, size_t rows) && is(T == Quaternion!V, V))
{
	Mat mat = Mat.identity;

	mat[0][0] = 1 - 2 * quat.y * quat.y - 2 * quat.z * quat.z;
	mat[0][1] = 2 * quat.x * quat.y + 2 * quat.w * quat.z;
	mat[0][2] = 2 * quat.x * quat.z - 2 * quat.w * quat.y;
	mat[1][0] = 2 * quat.x * quat.y - 2 * quat.w * quat.z;
	mat[1][1] = 1 - 2 * quat.x * quat.x - 2 * quat.z * quat.z;
	mat[1][2] = 2 * quat.y * quat.z + 2 * quat.w * quat.x;
	mat[2][0] = 2 * quat.x * quat.z + 2* quat.w * quat.y;
	mat[2][1] = 2 * quat.y * quat.z - 2 * quat.w * quat.x;
	mat[2][2] = 1 - 2 * quat.x * quat.x - 2 * quat.y * quat.y;

	return m * mat;
}

auto rotate(T)(T rot) if(is(T == Radian!U, U) || is(T == Degree!U, U))
{
	Matrix!(T.type, 4, 4) mat = Matrix!(T.type, 4, 4).identity;
	
	auto c = rot.cos;
	auto s = rot.sin;
	
	mat[0][0] = c;
	mat[0][1] = s;
	mat[1][0] = -s;
	mat[1][1] = c;
	
	return mat;
}

auto rotate(Mat, T)(in auto ref Mat m, T rot) if(is(Mat == Matrix!(U, cols, rows), U, size_t cols, size_t rows) && (is(T == Radian!V, V) || is(T == Degree!V, V)))
{
	Mat mat = Mat.identity;
	
	auto c = rot.cos;
	auto s = rot.sin;
	
	mat[0][0] = c;
	mat[0][1] = s;
	mat[1][0] = -s;
	mat[1][1] = c;
	
	return m * mat;
}

auto translate(T)(in auto ref T vec) if(is(T == Vector!(U, 3), U))
{
	Matrix!(T.type, 4, 4) mat = Matrix!(T.type, 4, 4).identity;

	mat[3][0] = vec.x;
	mat[3][1] = vec.y;
	mat[3][2] = vec.z;

	return mat;
}

auto translate(Mat, T)(in auto ref Mat m, in auto ref T vec) if(is(Mat == Matrix!(U, cols, rows), U, size_t cols, size_t rows) && is(T == Vector!(V, 3), V))
{
	Mat mat = Mat.identity;

	mat[3][0] = vec.x;
	mat[3][1] = vec.y;
	mat[3][2] = vec.z;

	return m * mat;
}

auto translate(T)(in auto ref T vec) if(is(T == Vector!(U, 2), U))
{
	Matrix!(T.type, 4, 4) mat = Matrix!(T.type, 4, 4).identity;
	
	mat[3][0] = vec.x;
	mat[3][1] = vec.y;
	
	return mat;
}

auto translate(Mat, T)(in auto ref Mat m, in auto ref T vec) if(is(Mat == Matrix!(U, cols, rows), U, size_t cols, size_t rows) && is(T == Vector!(V, 2), V))
{
	Mat mat = Mat.identity;
	
	mat[3][0] = vec.x;
	mat[3][1] = vec.y;
	
	return m * mat;
}

auto scale(T)(in auto ref T vec) if(is(T == Vector!(U, 3), U))
{
	Matrix!(T.type, 4, 4) mat = Matrix!(T.type, 4, 4).identity;

	mat[0][0] = vec.x;
	mat[1][1] = vec.y;
	mat[2][2] = vec.z;

	return mat;
}

auto scale(Mat, T)(in auto ref Mat m, in auto ref T vec) if(is(Mat == Matrix!(U, cols, rows), U, size_t cols, size_t rows) && is(T == Vector!(V, 3), V))
{
	Mat mat = Mat.identity;

	mat[0][0] = vec.x;
	mat[1][1] = vec.y;
	mat[2][2] = vec.z;

	return m * mat;
}

auto scale(T)(in auto ref T vec) if(is(T == Vector!(U, 2), U))
{
	Matrix!(T.type, 4, 4) mat = Matrix!(T.type, 4, 4).identity;
	
	mat[0][0] = vec.x;
	mat[1][1] = vec.y;
	
	return mat;
}

auto scale(Mat, T)(in auto ref Mat m, in auto ref T vec) if(is(Mat == Matrix!(U, cols, rows), U, size_t cols, size_t rows) && is(T == Vector!(V, 2), V))
{
	Mat mat = Mat.identity;
	
	mat[0][0] = vec.x;
	mat[1][1] = vec.y;
	
	return m * mat;
}

auto abs(T)(in auto ref T mat) if(is(T == Matrix!(U, cols, rows), U, size_t cols, size_t rows))
{
	import std.math : abs;
	Matrix!(T.type, T.cols, T.rows) ret;
	foreach(c; CTRange!(0, mat.cols))
	{
		foreach(r; CTRange!(0, mat.rows))
		{
			ret[c][r] = abs(mat[c][r]);
		}
	}
	return ret;
}

// not centered projection
auto ortho(T)(T left, T right, T bottom, T top, T near, T far) if(isFloatingPoint!T)
{
	return Matrix!(T, 4, 4)
	(
		   2/(right-left),				0,				0,	-(right+left)/(right-left),
						0, 2/(top-bottom), 				0,	-(top+bottom)/(top-bottom),
						0, 				0, 	-2/(far-near),		-(far+near)/(far-near),
						0, 				0, 				0,							 1
		
	);
}

// only valid if centered around origin
auto orthoPP(T)(T halfWidth, T halfHeight, T near, T far) if(isFloatingPoint!T)
{
	return Matrix!(T, 4, 4)
	(
		  1/halfWidth, 				0, 					0,						0,
					0,	 1/halfHeight, 					0,						0,
					0,				0, 		-2/(far-near), -(far+near)/(far-near),
					0,				0,					0,						1
	);
}

// non pixel perfect! (depending on vertical size)
auto ortho(T)(T halfHeight, T aspectRatio, T near, T far) if(isFloatingPoint!T)
{
	T halfWidth = aspectRatio * halfHeight;
	return Matrix!(T, 4, 4)
	(
		  1/halfWidth, 				0, 					0,						0,
					0,	 1/halfHeight, 					0,						0,
					0,				0, 		-2/(far-near), -(far+near)/(far-near),
					0,				0,					0,						1
	);
}

// yFov -> Radian
auto perspective(T)(T yFov, T aspectRatio, T near, T far) if(isFloatingPoint!T)
{
	assert(aspectRatio != 0);
	assert(near != far);
	
	import std.math : tan;
	
	T val =  tan(yFov / 2);
	
	return Matrix!(T, 4, 4)
	(
		1/(aspectRatio*val), 0, 0, 0,
		0, 1/val, 0, 0,
		0, 0, -(far+near)/(far-near), -1,
		0, 0, -(2 * far * near)/(far-near), 0
	);
}

// quaternion

auto axisAngle(T, U)(in auto ref Vector!(T, 3) axis, U angle) if(is(U == Radian!T))
{
	import std.math : cos, sin;
	
	auto axisNorm = axis.normalize;
	
	return Quaternion!T
	(
		cos(angle.value / 2),
		axisNorm.x * sin(angle.value / 2),
		axisNorm.y * sin(angle.value / 2),
		axisNorm.z * sin(angle.value / 2)
	);
}

auto normalize(T)(in auto ref Quaternion!T quat)
{
	T divLength = 1 / quat.magnitude;

	return Quaternion!T(quat.w * divLength, quat.x * divLength, quat.y * divLength, quat.z * divLength);
}

auto conjugate(T)(in auto ref Quaternion!T quat)
{
	return -quat;
}

auto invert(T)(in auto ref Quaternion!T quat)
{
	return quat.conjugate / quat.magnitude2;
}

auto magnitude2(T)(in auto ref Quaternion!T quat)
{
	return dot(quat, quat);
}

auto magnitude(T)(in auto ref Quaternion!T quat)
{
	import std.math : sqrt;
	return quat.magnitude2.sqrt;
}

auto dot(T)(in auto ref Quaternion!T first, in auto ref Quaternion!T second)
{
	return first.w * second.w + first.x * second.x + first.y * second.y + first.z * second.z;
}

auto yaw(T)(in auto ref T quat) if(is(T == Quaternion!U, U))
{
	import std.math : atan2;
	return atan2(2*(quat.w*quat.y - quat.x*quat.z), 1 - 2*quat.y*quat.y - 2*quat.z*quat.z);
}

auto pitch(T)(in auto ref T quat) if(is(T == Quaternion!U, U))
{
	import std.math : atan2;
	return atan2(2*quat.x*quat.w - 2*quat.y*quat.z, 1 - 2*quat.x*quat.x - 2*quat.z*quat.z);
}

auto roll(T)(in auto ref T quat) if(is(T == Quaternion!U, U))
{
	import std.math : atan2;
	return atan2(2*quat.y*quat.w - 2*quat.x*quat.z, 1 - 2*quat.y*quat.y - 2*quat.z*quat.z);
}

// angle

auto radian(T)(in T r) if(isScalarType!T)
{
	return Radian!T(r);
}

auto degree(T)(in T d) if(isScalarType!T)
{
	return Degree!T(d);
}

auto toDegree(T)(in T r) if(is(T == Radian!U, U))
{
	alias Type = T.type;
	return Degree!Type(r.value.toDegree);
}

auto toRadian(T)(in T d) if(is(T == Degree!U, U))
{
	alias Type = T.type;
	return Radian!Type(d.value.toRadian);
}

T toRadian(T)(in T angleInDegree) if(isScalarType!T)
{
	import std.math : PI;
	return angleInDegree * (PI / 180);
}

T toDegree(T)(in T angleInRad) if(isScalarType!T)
{
	import std.math : M_1_PI;
	return angleInRad * (180 * M_1_PI);
}

auto sin(Angle)(in Angle v) if(is(Angle == Degree!U, U) || is(Angle == Radian!U, U))
{
	import std.math : sin;
	auto value = v.value;
	static if(is(Angle == Degree!U, U))
		value = value.toRadian;
	
	return sin(value);
}

auto cos(Angle)(in Angle v) if(is(Angle == Degree!U, U) || is(Angle == Radian!U, U))
{
	import std.math : cos;
	auto value = v.value;
	static if(is(Angle == Degree!U, U))
		value = value.toRadian;
	
	return cos(value);
}
