module xgf.math;

public import xgf.math.vector;
public import xgf.math.matrix;
public import xgf.math.quaternion;
public import xgf.math.angle;
public import xgf.math.functions;

alias vec2 = Vector!(float, 2);
alias vec3 = Vector!(float, 3);
alias vec4 = Vector!(float, 4);

alias ivec2 = Vector!(int, 2);
alias ivec3 = Vector!(int, 3);
alias ivec4 = Vector!(int, 4);

alias uvec2 = Vector!(uint, 2);
alias uvec3 = Vector!(uint, 3);
alias uvec4 = Vector!(uint, 4);

alias ubvec2 = Vector!(ubyte, 2);
alias ubvec3 = Vector!(ubyte, 3);
alias ubvec4 = Vector!(ubyte, 4);

alias bvec2 = Vector!(byte, 2);
alias bvec3 = Vector!(byte, 3);
alias bvec4 = Vector!(byte, 4);

alias mat4 = Matrix!(float, 4, 4);

alias quat = Quaternion!float;
