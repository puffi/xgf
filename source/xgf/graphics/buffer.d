module xgf.graphics.buffer;

import xgf.graphics.glad.gl.all;

import xgf.util.array;

struct Buffer
{
	private
	{
		uint mHandle;
		uint mSize;
		BufferFlag mFlags;
		Buffering mBuffering;
		ubyte[] mMappedData;
		DynamicArray!(FreeSpace)[3] mFreeSpaces;
	}
	
	this(uint size, Buffering buffering = Buffering.single, BufferFlag flags = BufferFlag.none)
	{
		mSize = size;
		mFlags = flags;
		mBuffering = buffering;
		
		glCreateBuffers(1, &mHandle);
		
		GLenum bufferflags;
		if(mFlags & BufferFlag.persistent)
			bufferflags |= GL_MAP_PERSISTENT_BIT;
		if(mFlags & BufferFlag.coherent)
			bufferflags |= GL_MAP_COHERENT_BIT;
		if(mFlags & BufferFlag.write)
			bufferflags |= GL_MAP_WRITE_BIT;
		if(mFlags & BufferFlag.read)
			bufferflags |= GL_MAP_READ_BIT;
		
		glNamedBufferStorage(mHandle, mSize * mBuffering, null, bufferflags);
		
		mFreeSpaces[0].pushBack(FreeSpace(0, mSize));
		if(mBuffering == Buffering.double_)
		{
			mFreeSpaces[1].pushBack(FreeSpace(mSize, mSize));
		}
		else if(mBuffering == Buffering.triple)
		{
			mFreeSpaces[1].pushBack(FreeSpace(mSize, mSize));
			mFreeSpaces[2].pushBack(FreeSpace(mSize*2, mSize));
		}
	}
	
	~this()
	{
		if(mHandle)
		{
			if(mMappedData.length > 0)
			{
				glUnmapBuffer(mHandle);
			}
			glDeleteBuffers(1, &mHandle);
		}
	}
	
	void upload(ubyte[] data, uint offset, uint frame = 0)
	{
		assert(mFlags & (BufferFlag.persistent | BufferFlag.coherent | BufferFlag.write));
		if(mMappedData.length == 0)
		{
			GLenum flags = GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_MAP_WRITE_BIT;
			
			mMappedData = (cast(ubyte*)(glMapNamedBufferRange(mHandle, 0, mSize * mBuffering, flags)))[0..mSize*mBuffering];
		}
		
		mMappedData[offset+mSize*frame..offset+mSize*frame+data.length] = data[];
	}
	
	@property auto size() { return mSize; }
	@property auto handle() { return mHandle; }
	@property auto flags() { return mFlags; }
	@property auto buffering() { return mBuffering; }
	
	// allocate, free, optimize
	BufferView allocate(uint size, uint alignment, uint frame = 0)
	{
		foreach(i, ref s; mFreeSpaces[frame][])
		{
			if(s.size >= size)
			{
				// special case 0, as % 0 is invalid
				auto mod = alignment == 0 ? 0 : s.offset % alignment;
				auto diff = alignment - mod;
				if(mod == 0)
				{
					BufferView view = BufferView(&this, size, s.offset);
					if(s.size == size)
					{
						import std.algorithm : swap;
						swap(mFreeSpaces[frame][i], mFreeSpaces[frame].back);
						mFreeSpaces[frame].popBack();
					}
					else if(s.size > size)
					{
						s.offset += size;
						s.size -= size;
					}
					return view;
				}
				else if(diff <= (s.size - size))
				{
					BufferView view = BufferView(&this, size, s.offset + diff);
					if(s.size > (size + diff))
					{
						auto oldSize = s.size;
						s.size = diff;
						mFreeSpaces[frame].pushBack(FreeSpace(s.offset + s.size + size, oldSize - s.size - size));
					}
					else if(s.size == (size + diff))
					{
						s.size = diff;
					}
					return view;
				}
			}
		}
		
		return BufferView.init;
	}
	
	void free(BufferView view, uint frame = 0)
	{
		assert(view.buffer == &this);
		
		if(view.buffer == &this)
		{
			mFreeSpaces[frame].pushBack(FreeSpace(view.offset, view.size));
			optimize(frame);
		}
	}
	
	void clear(uint frame = 0)
	{
		mFreeSpaces[frame].clear();
		mFreeSpaces[frame].pushBack(FreeSpace(mSize*frame, mSize));
	}
	
	private
	{
		void optimize(uint frame = 0)
		{
			import std.algorithm : sort;
			
			alias sortFunc = (a, b) { return a.offset < b.offset; };
			auto sorted = mFreeSpaces[frame][].sort!(sortFunc);
			
			import std.range : enumerate, retro;
			foreach(i, s; mFreeSpaces[frame][].enumerate.retro)
			{
				if(i > 0)
				{
					auto s2 = mFreeSpaces[frame][i-1];
					if(s2.offset + s2.size == s.offset)
					{
						// merge
						s2.size = s2.size + s.size;
						mFreeSpaces[frame][i-1] = s2;
						import std.algorithm : swap;
						swap(mFreeSpaces[frame][i], mFreeSpaces[frame].back);
						mFreeSpaces[frame].popBack();
					}
				}
			}
			
			mFreeSpaces[frame][].sort!(sortFunc);
		}
	}
}

enum BufferFlag
{
	none = 0,
	persistent = 1 << 0,
	coherent = 1 << 1,
	read = 1 << 2,
	write = 1 << 3
}

enum Buffering
{
	single = 1,
	double_ = 2,
	triple = 3
}

struct BufferView
{
	Buffer* buffer;
	uint size;
	uint offset;
	
	@property auto isValid() { return buffer !is null && size > 0; }
}

private
{
	struct FreeSpace
	{
		uint offset;
		uint size;
	}
}
