module xgf.graphics.config;

enum MaxRenderTargets = 64;
enum DefaultRenderTargetIndex = 0;
