module xgf.graphics.vertexarrayobject;

import xgf.graphics.glad.gl.all;

import xgf.graphics.vertexbuffer;
import xgf.graphics.indexbuffer;
import xgf.graphics.instanceindexbuffer;
import xgf.graphics.vertexdeclaration;

struct VertexArrayObject
{
	private
	{
		uint mHandle;
	}
	
	~this()
	{
		glBindVertexArray(0);
		if(mHandle)
			glDeleteVertexArrays(1, &mHandle);
	}
	
	void initialize()
	{
		glCreateVertexArrays(1, &mHandle);
		glBindVertexArray(mHandle);
	}
	
	void setVertexBuffer(VertexBuffer* vb)
	{
		foreach(uint i, attr; vb.vertexDecl.attributes)
		{
// 			if(attr.type == AttributeType.none)
// 				continue;
			glEnableVertexArrayAttrib(mHandle, attr.location);
			glVertexArrayAttribFormat(mHandle, attr.location, attr.num, attr.type.glType, attr.normalized.glType, vb.vertexDecl.offsets[i]);
			glVertexArrayAttribBinding(mHandle, attr.location, 0);
		}
		
		glVertexArrayVertexBuffer(mHandle, 0, vb.buffer.handle, 0, vb.vertexDecl.vertexSize);
	}
	
	void setIndexBuffer(IndexBuffer* ib)
	{
		glVertexArrayElementBuffer(mHandle, ib.buffer.handle);
	}
	
	void setInstanceIndexBuffer()(InstanceIndexBuffer* iib)
	{
		glVertexArrayAttribIFormat(mHandle, 15, 1, iib.type.glType, 0);
		glEnableVertexArrayAttrib(mHandle, 15);
		glVertexArrayAttribBinding(mHandle, 15, 15);
		glVertexArrayVertexBuffer(mHandle, 15, iib.buffer.handle, iib.offset, iib.type.size);
		glVertexArrayBindingDivisor(mHandle, 15, 1);
	}
	
	void draw()(PrimitiveType type, uint instances, uint baseInstance, VertexBuffer* vb)
	{
		glDrawArraysInstancedBaseInstance(type.glType, vb.baseVertex, vb.size / vb.vertexDecl.vertexSize, instances, baseInstance);
	}
	
	void drawIndexed()(PrimitiveType type, uint instances, uint baseInstance, VertexBuffer* vb, IndexBuffer* ib)
	{		
		glDrawElementsInstancedBaseVertexBaseInstance(type.glType, ib.count, ib.type.glType, cast(void*)ib.offset, instances, vb.baseVertex, baseInstance);
	}
}

enum PrimitiveType
{
	points,
	lines,
	triangles
}

private
{
	auto glType(Attribute.Type type)
	{
		final switch(type)
		{
			case Attribute.Type.u8: return GL_UNSIGNED_BYTE;
			case Attribute.Type.u16: return GL_UNSIGNED_SHORT;
			case Attribute.Type.u32: return GL_UNSIGNED_INT;
			case Attribute.Type.f32: return GL_FLOAT;
		}
	}
	
	auto glType(bool value)
	{
		final switch(value)
		{
			case false: return GL_FALSE;
			case true: return GL_TRUE;
		}
	}
	
	auto glType(PrimitiveType type)
	{
		final switch(type)
		{
			case PrimitiveType.points: return GL_POINTS;
			case PrimitiveType.lines: return GL_LINES;
			case PrimitiveType.triangles: return GL_TRIANGLES;
		}
	}
	
	auto glType(IndexType type)
	{
		final switch(type)
		{
			case IndexType.u8: return GL_UNSIGNED_BYTE;
			case IndexType.u16: return GL_UNSIGNED_SHORT;
			case IndexType.u32: return GL_UNSIGNED_INT;
		}
	}
	
	auto glType(InstanceIndexType type)
	{
		final switch(type)
		{
			case InstanceIndexType.u32: return GL_UNSIGNED_INT;
		}
	}
}
