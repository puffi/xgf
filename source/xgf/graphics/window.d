module xgf.graphics.window;

import xgf.core.core;

import derelict.sdl2.sdl;
import xgf.graphics.glad.gl.all;
import xgf.graphics.platform;

import xgf.math.vector;
alias ivec2 = Vector!(int, 2);

static this()
{
	DerelictSDL2.load();
	SDL_Init(SDL_INIT_VIDEO);
	
	// DerelictGL3.load();
}

static ~this()
{
	// DerelictGL3.unload();
	
	SDL_Quit();
	DerelictSDL2.unload();
}

struct Window
{
	private
	{
		Core* mCore;
		SDL_Window* mWindow;
		ivec2 mSize;
		bool mIsOpen;
		bool mVsync;
	}
	
	this(Core* core, in string title, int width, int height)
	{
		mCore = core;
		mSize = ivec2(width, height);
		
		import xgf.util.alloc;
		auto s = allocArray!(char)(title.length + 1);
		s[0..$-1] = title[];
		s[$-1] = '\0';
		scope(exit) free(s);
		
		// SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		// SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
		// SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		// debug
		// {
		// 	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
		// }
		
		mWindow = SDL_CreateWindow(s.ptr, 0, 0, width, height, SDL_WINDOW_SHOWN);
		if(mWindow)
			mIsOpen = true;
		
		// auto context = SDL_GL_CreateContext(mWindow);
		// if(!context)
		// 	mIsOpen = false;
		// DerelictGL3.reload();
		// setup messages
	}
	
	~this()
	{
		import std.stdio;
		writeln("Destroying window.");
		SDL_DestroyWindow(mWindow);
		mIsOpen = false;
	}
	
	@property auto handle() { return mWindow; }
	@property auto isOpen() { return mIsOpen; }
	@property auto size() { return mSize; }
	
	@property auto vsync() { return mVsync; }
	@property void vsync(bool on)
	{
		mVsync = on;
		if(mVsync)
			SDL_GL_SetSwapInterval(1);
		else
			SDL_GL_SetSwapInterval(0);
	}

	@property Platform platform()
	{
		Platform plat;

		SDL_SysWMinfo wmi;
		SDL_VERSION(&wmi.version_);
		assert(SDL_GetWindowWMInfo(mWindow, &wmi), "Can't get WM info.");
		plat.type = Platform.Type.xlib;
		plat.xlib.display = wmi.info.x11.display;
		plat.xlib.window = wmi.info.x11.window;
		return plat;
	}
	
	void resize(int width, int height)
	{
		SDL_SetWindowSize(mWindow, width, height);
	}
	
	void close()
	{
		import std.stdio;
		writeln("Closing window.");
		mCore.broadcast!"WindowClosing"(&this);
		mIsOpen = false;
	}
	
	void display()
	{
		// SDL_GL_SwapWindow(mWindow);
	}
	
	void pollEvents()
	{
		SDL_Event evt;
		while(SDL_PollEvent(&evt))
		{
			switch(evt.type)
			{
				case SDL_QUIT: close(); break;
				case SDL_WINDOWEVENT: handleWindowEvent(&evt); break;
				case SDL_KEYDOWN: handleKeyDownEvent(&evt); break;
				case SDL_KEYUP: handleKeyUpEvent(&evt); break;
				case SDL_MOUSEMOTION: handleMouseMoveEvent(&evt); break;
				case SDL_MOUSEBUTTONDOWN: handleMouseDownEvent(&evt); break;
				case SDL_MOUSEBUTTONUP: handleMouseUpEvent(&evt); break;
				case SDL_TEXTINPUT: handleTextInputEvent(&evt); break;
				case SDL_TEXTEDITING: handleTextEditEvent(&evt); break;
				default: break;
			}
		}
	}
	
	private
	{
		void handleWindowEvent(SDL_Event* evt)
		{
			switch(evt.window.event)
			{
				case SDL_WINDOWEVENT_CLOSE: close(); break;
				default: break;
			}
		}
		
		void handleKeyDownEvent(SDL_Event* evt)
		{
			import std.stdio;
			// writeln("Handling key down: ", evt.key.keysym.scancode);
			mCore.broadcast!"KeyDown"(cast(Scancode)evt.key.keysym.scancode, cast(Modifier)evt.key.keysym.mod);
		}
		
		void handleKeyUpEvent(SDL_Event* evt)
		{
			import std.stdio;
			// writeln("Handling key up: ", evt.key.keysym.scancode);
			mCore.broadcast!"KeyUp"(cast(Scancode)evt.key.keysym.scancode, cast(Modifier)evt.key.keysym.mod);
		}
		
		void handleMouseMoveEvent(SDL_Event* evt)
		{
			import std.stdio;
// 			writeln("Handling mouse move: ", evt.motion.x, " ", evt.motion.y);
			mCore.broadcast!"MouseMove"(evt.motion.x, evt.motion.y, evt.motion.xrel, evt.motion.yrel);
		}
		
		void handleMouseDownEvent(SDL_Event* evt)
		{
			import std.stdio;
// 			writeln("Handling mouse down: ", evt.button.button, " clicks: ", evt.button.clicks);
			mCore.broadcast!"MouseDown"(cast(MouseButton)evt.button.button, evt.button.clicks, evt.button.x, evt.button.y);
		}
		
		void handleMouseUpEvent(SDL_Event* evt)
		{
			import std.stdio;
// 			writeln("Handling mouse up: ", evt.button.button);
			mCore.broadcast!"MouseUp"(cast(MouseButton)evt.button.button, evt.button.x, evt.button.y);
		}

		void handleTextInputEvent(SDL_Event* evt)
		{
			import std.stdio;
			import std.conv : to;
			import core.stdc.string;

			auto len = strlen(evt.text.text.ptr);

			// writeln("Handling text input: ", evt.text.text[0..len]);
			mCore.broadcast!"TextInput"(evt.text.text[0..len]);
		}

		void handleTextEditEvent(SDL_Event* evt)
		{
			import std.stdio;
			import std.conv : to;

			mCore.broadcast!"TextEdit"(evt.edit.text.to!string, evt.edit.start, evt.edit.length);
		}
	}
}

enum WindowFlags
{
	none = 0,
	opengl = SDL_WINDOW_OPENGL
}

enum Scancode
{
	unknown = 0,

	a = 4,
	b = 5,
	c = 6,
	d = 7,
	e = 8,
	f = 9,
	g = 10,
	h = 11,
	i = 12,
	j = 13,
	k = 14,
	l = 15,
	m = 16,
	n = 17,
	o = 18,
	p = 19,
	q = 20,
	r = 21,
	s = 22,
	t = 23,
	u = 24,
	v = 25,
	w = 26,
	x = 27,
	y = 28,
	z = 29,

	_1 = 30,
	_2 = 31,
	_3 = 32,
	_4 = 33,
	_5 = 34,
	_6 = 35,
	_7 = 36,
	_8 = 37,
	_9 = 38,
	_0 = 39,

	return_ = 40,
	escape = 41,
	backspace = 42,
	tab = 43,
	space = 44,

	minus = 45,
	equals = 46,
	leftbracket = 47,
	rightbracket = 48,
	backslash = 49,
	nonushash = 50,
	semicolon = 51,
	apostrophe = 52,
	grave = 53,
	comma = 54,
	period = 55,
	slash = 56,

	capslock = 57,

	f1 = 58,
	f2 = 59,
	f3 = 60,
	f4 = 61,
	f5 = 62,
	f6 = 63,
	f7 = 64,
	f8 = 65,
	f9 = 66,
	f10 = 67,
	f11 = 68,
	f12 = 69,

	printscreen = 70,
	scrolllock = 71,
	pause = 72,
	insert = 73,
	home = 74,
	pageup = 75,
	delete_ = 76,
	end = 77,
	pagedown = 78,
	right = 79,
	left = 80,
	down = 81,
	up = 82,

	numlockclear = 83,
	kp_divide = 84,
	kp_multiply = 85,
	kp_minus = 86,
	kp_plus = 87,
	kp_enter = 88,
	kp_1 = 89,
	kp_2 = 90,
	kp_3 = 91,
	kp_4 = 92,
	kp_5 = 93,
	kp_6 = 94,
	kp_7 = 95,
	kp_8 = 96,
	kp_9 = 97,
	kp_0 = 98,
	kp_period = 99,

	nonusbackslash = 100,
	application = 101,
	power = 102,
	kp_equals = 103,
	f13 = 104,
	f14 = 105,
	f15 = 106,
	f16 = 107,
	f17 = 108,
	f18 = 109,
	f19 = 110,
	f20 = 111,
	f21 = 112,
	f22 = 113,
	f23 = 114,
	f24 = 115,
	execute = 116,
	help = 117,
	menu = 118,
	select = 119,
	stop = 120,
	again = 121,
	undo = 122,
	cut = 123,
	copy = 124,
	paste = 125,
	find = 126,
	mute = 127,
	volumeup = 128,
	volumedown = 129,
	kp_comma = 133,
	kp_equalsas400 = 134,

	international1 = 135,
	international2 = 136,
	international3 = 137,
	international4 = 138,
	international5 = 139,
	international6 = 140,
	international7 = 141,
	international8 = 142,
	international9 = 143,
	lang1 = 144,
	lang2 = 145,
	lang3 = 146,
	lang4 = 147,
	lang5 = 148,
	lang6 = 149,
	lang7 = 150,
	lang8 = 151,
	lang9 = 152,

	alterase = 153,
	sysreq = 154,
	cancel = 155,
	clear = 156,
	prior = 157,
	return2 = 158,
	separator = 159,
	out_ = 160,
	oper = 161,
	clearagain = 162,
	crsel = 163,
	exsel = 164,

	kp_00 = 176,
	kp_000 = 177,
	thousandsseparator = 178,
	decimalseparator = 179,
	currencyunit = 180,
	currencysubunit = 181,
	kp_leftparen = 182,
	kp_rightparen = 183,
	kp_leftbrace = 184,
	kp_rightbrace = 185,
	kp_tab = 186,
	kp_backspace = 187,
	kp_a = 188,
	kp_b = 189,
	kp_c = 190,
	kp_d = 191,
	kp_e = 192,
	kp_f = 193,
	kp_xor = 194,
	kp_power = 195,
	kp_percent = 196,
	kp_less = 197,
	kp_greater = 198,
	kp_ampersand = 199,
	kp_dblampersand = 200,
	kp_verticalbar = 201,
	kp_dblverticalbar = 202,
	kp_colon = 203,
	kp_hash = 204,
	kp_space = 205,
	kp_at = 206,
	kp_exclam = 207,
	kp_memstore = 208,
	kp_memrecall = 209,
	kp_memclear = 210,
	kp_memadd = 211,
	kp_memsubtract = 212,
	kp_memmultiply = 213,
	kp_memdivide = 214,
	kp_plusminus = 215,
	kp_clear = 216,
	kp_clearentry = 217,
	kp_binary = 218,
	kp_octal = 219,
	kp_decimal = 220,
	kp_hexadecimal = 221,

	lctrl = 224,
	lshift = 225,
	lalt = 226,
	lgui = 227,
	rctrl = 228,
	rshift = 229,
	ralt = 230,
	rgui = 231,

	mode = 257,

	audionext = 258,
	audioprev = 259,
	audiostop = 260,
	audioplay = 261,
	audiomute = 262,
	mediaselect = 263,
	www = 264,
	mail = 265,
	calculator = 266,
	computer = 267,
	ac_search = 268,
	ac_home = 269,
	ac_back = 270,
	ac_forward = 271,
	ac_stop = 272,
	ac_refresh = 273,
	ac_bookmarks = 274,

	brightnessdown = 275,
	brightnessup = 276,
	displayswitch = 277,
	kbdillumtoggle = 278,
	kbdillumdown = 279,
	kbdillumup = 280,
	eject = 281,
	sleep = 282,

	app1 = 283,
	app2 = 284,

	num_scancodes = 512
}

enum MouseButton : ubyte
{
	left = 1,
	right = 2,
	middle = 3,
	x1 = 4,
	x2 = 5
}

enum Key : int
{
	unknown = 0,
    return_ = '\r',
    escape = '\033',
    backspace = '\b',
    tab = '\t',
    space = ' ',
    exclaim = '!',
    quotedbl = '"',
    hash = '#',
    percent = '%',
    dollar = '$',
    ampersand = '&',
    quote = '\'',
    leftparen = '(',
    rightparen = ')',
    asterisk = '*',
    plus = '+',
    comma = ',',
    minus = '-',
    period = '.',
    slash = '/',
    zero = '0',
    one = '1',
    two = '2',
    three = '3',
    four = '4',
    five = '5',
    six = '6',
    seven = '7',
    eight = '8',
    nine = '9',
    colon = ':',
    semicolon = ';',
    less = '<',
    equals = '=',
    greater = '>',
    question = '?',
    at = '@',

    leftbracket = '[',
    backslash = '\\',
    rightbracket = ']',
    caret = '^',
    underscore = '_',
    backquote = '`',
    a = 'a',
    b = 'b',
    c = 'c',
    d = 'd',
    e = 'e',
    f = 'f',
    g = 'g',
    h = 'h',
    i = 'i',
    j = 'j',
    k = 'k',
    l = 'l',
    m = 'm',
    n = 'n',
    o = 'o',
    p = 'p',
    q = 'q',
    r = 'r',
    s = 's',
    t = 't',
    u = 'u',
    v = 'v',
    w = 'w',
    x = 'x',
    y = 'y',
    z = 'z',

    capslock = to_keycode( Scancode.capslock ),

    f1 = to_keycode( Scancode.f1 ),
    f2 = to_keycode( Scancode.f2 ),
    f3 = to_keycode( Scancode.f3 ),
    f4 = to_keycode( Scancode.f4 ),
    f5 = to_keycode( Scancode.f5 ),
    f6 = to_keycode( Scancode.f6 ),
    f7 = to_keycode( Scancode.f7 ),
    f8 = to_keycode( Scancode.f8 ),
    f9 = to_keycode( Scancode.f9 ),
    f10 = to_keycode( Scancode.f10 ),
    f11 = to_keycode( Scancode.f11 ),
    f12 = to_keycode( Scancode.f12 ),

    printscreen = to_keycode( Scancode.printscreen ),
    scrolllock = to_keycode( Scancode.scrolllock ),
    pause = to_keycode( Scancode.pause ),
    insert = to_keycode( Scancode.insert ),
    home = to_keycode( Scancode.home ),
    pageup = to_keycode( Scancode.pageup ),
    delete_ = '\177',
    end = to_keycode( Scancode.end ),
    pagedown = to_keycode( Scancode.pagedown ),
    right = to_keycode( Scancode.right ),
    left = to_keycode( Scancode.left ),
    down = to_keycode( Scancode.down ),
    up = to_keycode( Scancode.up ),

    numlockclear = to_keycode( Scancode.numlockclear ),
    kp_divide = to_keycode( Scancode.kp_divide ),
    kp_multiply = to_keycode( Scancode.kp_multiply ),
    kp_minus = to_keycode( Scancode.kp_minus ),
    kp_plus = to_keycode( Scancode.kp_plus ),
    kp_enter = to_keycode( Scancode.kp_enter ),
    kp_1 = to_keycode( Scancode.kp_1 ),
    kp_2 = to_keycode( Scancode.kp_2 ),
    kp_3 = to_keycode( Scancode.kp_3 ),
    kp_4 = to_keycode( Scancode.kp_4 ),
    kp_5 = to_keycode( Scancode.kp_5 ),
    kp_6 = to_keycode( Scancode.kp_6 ),
    kp_7 = to_keycode( Scancode.kp_7 ),
    kp_8 = to_keycode( Scancode.kp_8 ),
    kp_9 = to_keycode( Scancode.kp_9 ),
    kp_0 = to_keycode( Scancode.kp_0 ),
    kp_period = to_keycode( Scancode.kp_period ),

    application = to_keycode( Scancode.application ),
    power = to_keycode( Scancode.power ),
    kp_equals = to_keycode( Scancode.kp_equals ),
    f13 = to_keycode( Scancode.f13 ),
    f14 = to_keycode( Scancode.f14 ),
    f15 = to_keycode( Scancode.f15 ),
    f16 = to_keycode( Scancode.f16 ),
    f17 = to_keycode( Scancode.f17 ),
    f18 = to_keycode( Scancode.f18 ),
    f19 = to_keycode( Scancode.f19 ),
    f20 = to_keycode( Scancode.f20 ),
    f21 = to_keycode( Scancode.f21 ),
    f22 = to_keycode( Scancode.f22 ),
    f23 = to_keycode( Scancode.f23 ),
    f24 = to_keycode( Scancode.f24 ),
    execute = to_keycode( Scancode.execute ),
    help = to_keycode( Scancode.help ),
    menu = to_keycode( Scancode.menu ),
    select = to_keycode( Scancode.select ),
    stop = to_keycode( Scancode.stop ),
    again = to_keycode( Scancode.again ),
    undo = to_keycode( Scancode.undo ),
    cut = to_keycode( Scancode.cut ),
    copy = to_keycode( Scancode.copy ),
    paste = to_keycode( Scancode.paste ),
    find = to_keycode( Scancode.find ),
    mute = to_keycode( Scancode.mute ),
    volumeup = to_keycode( Scancode.volumeup ),
    volumedown = to_keycode( Scancode.volumedown ),
    kp_comma = to_keycode( Scancode.kp_comma ),
    kp_equalsas400 = to_keycode( Scancode.kp_equalsas400 ),
    alterase = to_keycode( Scancode.alterase ),
    sysreq = to_keycode( Scancode.sysreq ),
    cancel = to_keycode( Scancode.cancel ),
    clear = to_keycode( Scancode.clear ),
    prior = to_keycode( Scancode.prior ),
    return2 = to_keycode( Scancode.return2 ),
    separator = to_keycode( Scancode.separator ),
    out_ = to_keycode( Scancode.out_ ),
    oper = to_keycode( Scancode.oper ),
    clearagain = to_keycode( Scancode.clearagain ),
    crsel = to_keycode( Scancode.crsel ),
    exsel = to_keycode( Scancode.exsel ),
    kp_00 = to_keycode( Scancode.kp_00 ),
    kp_000 = to_keycode( Scancode.kp_000 ),
    thousandsseparator = to_keycode( Scancode.thousandsseparator ),
    decimalseparator = to_keycode( Scancode.decimalseparator ),
    currencyunit = to_keycode( Scancode.currencyunit ),
    currencysubunit = to_keycode( Scancode.currencysubunit ),
    kp_leftparen = to_keycode( Scancode.kp_leftparen ),
    kp_rightparen = to_keycode( Scancode.kp_rightparen ),
    kp_leftbrace = to_keycode( Scancode.kp_leftbrace ),
    kp_rightbrace = to_keycode( Scancode.kp_rightbrace ),
    kp_tab = to_keycode( Scancode.kp_tab ),
    kp_backspace = to_keycode( Scancode.kp_backspace ),
    kp_a = to_keycode( Scancode.kp_a ),
    kp_b = to_keycode( Scancode.kp_b ),
    kp_c = to_keycode( Scancode.kp_c ),
    kp_d = to_keycode( Scancode.kp_d ),
    kp_e = to_keycode( Scancode.kp_e ),
    kp_f = to_keycode( Scancode.kp_f ),
    kp_xor = to_keycode( Scancode.kp_xor ),
    kp_power = to_keycode( Scancode.kp_power ),
    kp_percent = to_keycode( Scancode.kp_percent ),
    kp_less = to_keycode( Scancode.kp_less ),
    kp_greater = to_keycode( Scancode.kp_greater ),
    kp_ampersand = to_keycode( Scancode.kp_ampersand ),
    kp_dblampersand = to_keycode( Scancode.kp_dblampersand ),
    kp_verticalbar = to_keycode( Scancode.kp_verticalbar ),
    kp_dblverticalbar = to_keycode( Scancode.kp_dblverticalbar ),
    kp_colon = to_keycode( Scancode.kp_colon ),
    kp_hash = to_keycode( Scancode.kp_hash ),
    kp_space = to_keycode( Scancode.kp_space ),
    kp_at = to_keycode( Scancode.kp_at ),
    kp_exclam = to_keycode( Scancode.kp_exclam ),
    kp_memstore = to_keycode( Scancode.kp_memstore ),
    kp_memrecall = to_keycode( Scancode.kp_memrecall ),
    kp_memclear = to_keycode( Scancode.kp_memclear ),
    kp_memadd = to_keycode( Scancode.kp_memadd ),
    kp_memsubtract = to_keycode( Scancode.kp_memsubtract ),
    kp_memmultiply = to_keycode( Scancode.kp_memmultiply ),
    kp_memdivide = to_keycode( Scancode.kp_memdivide ),
    kp_plusminus = to_keycode( Scancode.kp_plusminus ),
    kp_clear = to_keycode( Scancode.kp_clear ),
    kp_clearentry = to_keycode( Scancode.kp_clearentry ),
    kp_binary = to_keycode( Scancode.kp_binary ),
    kp_octal = to_keycode( Scancode.kp_octal ),
    kp_decimal = to_keycode( Scancode.kp_decimal ),
    kp_hexadecimal = to_keycode( Scancode.kp_hexadecimal ),

    lctrl = to_keycode( Scancode.lctrl ),
    lshift = to_keycode( Scancode.lshift ),
    lalt = to_keycode( Scancode.lalt ),
    lgui = to_keycode( Scancode.lgui ),
    rctrl = to_keycode( Scancode.rctrl ),
    rshift = to_keycode( Scancode.rshift ),
    ralt = to_keycode( Scancode.ralt ),
    rgui = to_keycode( Scancode.rgui ),

    mode = to_keycode( Scancode.mode ),

    audionext = to_keycode( Scancode.audionext ),
    audioprev = to_keycode( Scancode.audioprev ),
    audiostop = to_keycode( Scancode.audiostop ),
    audioplay = to_keycode( Scancode.audioplay ),
    audiomute = to_keycode( Scancode.audiomute ),
    mediaselect = to_keycode( Scancode.mediaselect ),
    www = to_keycode( Scancode.www ),
    mail = to_keycode( Scancode.mail ),
    calculator = to_keycode( Scancode.calculator ),
    computer = to_keycode( Scancode.computer ),
    ac_search = to_keycode( Scancode.ac_search ),
    ac_home = to_keycode( Scancode.ac_home ),
    ac_back = to_keycode( Scancode.ac_back ),
    ac_forward = to_keycode( Scancode.ac_forward ),
    ac_stop = to_keycode( Scancode.ac_stop ),
    ac_refresh = to_keycode( Scancode.ac_refresh ),
    ac_bookmarks = to_keycode( Scancode.ac_bookmarks ),

    brightnessdown = to_keycode( Scancode.brightnessdown ),
    brightnessup = to_keycode( Scancode.brightnessup ),
    displayswitch = to_keycode( Scancode.displayswitch ),
    kbdillumtoggle = to_keycode( Scancode.kbdillumtoggle ),
    kbdillumdown = to_keycode( Scancode.kbdillumdown ),
    kbdillumup = to_keycode( Scancode.kbdillumup ),
    eject = to_keycode( Scancode.eject ),
    sleep = to_keycode( Scancode.sleep )
}

enum Modifier : ushort
{
	none = 0x0000,
	lshift = 0x0001,
	rshift = 0x0002,
	lctrl = 0x0040,
	rctrl= 0x0080,
	lalt= 0x0100,
	ralt = 0x0200,
	lgui = 0x0400,
	rgui = 0x0800,
	num = 0x1000,
	caps = 0x2000,
	mode = 0x4000, // Alt Gr
	reserved = 0x8000,
	ctrl = lctrl | rctrl,
	shift = lshift | rshift,
	alt = lalt | ralt,
	gui = lgui | rgui
}

enum sdlk_scancode_mask = 1<<30;
int to_keycode( int x ) {
    return ( x | sdlk_scancode_mask );
}
