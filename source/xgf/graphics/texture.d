module xgf.graphics.texture;

import xgf.util.handle;

struct Texture
{
	private
	{
		uint mHandle;
		Type mType;
		uint mWidth;
		uint mHeight;
		uint mDepth;
		Format mFormat;
	}
	
	@property auto handle() { return mHandle; }
	@property auto type() { return mType; }
	@property auto width() { return mWidth; }
	@property auto height() { return mHeight; }
	@property auto depth() { return mDepth; }
	@property auto format() { return mFormat; }
}

enum Type
{
	none,
	tex1d,
	tex2d,
	tex3d
}

enum Format
{
	r8,
	rgb8,
	rgba8,
	bgra8
}

mixin(Handle!"Texture");
