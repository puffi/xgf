module xgf.graphics.sampler;

import xgf.util.handle;

mixin(Handle!"Sampler");

struct Sampler
{
	private
	{
		uint mHandle;
		uint mHash;
		Filter mMinFilter;
		Filter mMagFilter;
		Wrap mWrapS;
		Wrap mWrapT;
		Wrap mWrapR;
	}
	
	@property auto handle() { return mHandle; }
	@property auto hash() { return mHash; }
	@property auto minFilter() { return mMinFilter; }
	@property void minFilter(Filter filter) { mMinFilter = filter; }
	@property auto magFilter() { return mMagFilter; }
	@property void magFilter(Filter filter) { mMagFilter = filter; }
	@property auto wrapS() { return mWrapS; }
	@property void wrapS(Wrap wrap) { mWrapS = wrap; }
	@property auto wrapT() { return mWrapT; }
	@property void wrapT(Wrap wrap) { mWrapT = wrap; }
	@property auto wrapR() { return mWrapR; }
	@property void wrapR(Wrap wrap) { mWrapR = wrap; }
}

enum Filter
{
	nearest,
	linear
}

enum Wrap
{
	repeat,
	mirroredRepeat,
	clamp
}
