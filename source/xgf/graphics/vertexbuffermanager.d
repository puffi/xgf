module xgf.graphics.vertexbuffermanager;

import xgf.graphics.buffer;
import xgf.graphics.vertexbuffer;
import xgf.graphics.indexbuffer;
import xgf.graphics.vertexdeclaration;
import xgf.graphics.instanceindexbuffer;

import xgf.util.alloc;
import xgf.util.array;
import xgf.util.handle;

enum VertexBufferSize = 128 * 1024 * 1024;
enum MaxVertexBuffers = 4096;
enum MaxIndexBuffers = 4096;
enum MaxInstanceIndexBuffers = 16;

struct VertexBufferManager
{
	private
	{
		DynamicArray!(Buffer*) mBuffers;
		DynamicArray!(DynamicArray!(FreeSpace)) mFreeSpaces;
		VertexBuffer*[MaxVertexBuffers] mVertexBuffers;
		IndexBuffer*[MaxIndexBuffers] mIndexBuffers;
		InstanceIndexBuffer*[MaxInstanceIndexBuffers] mInstanceIndexBuffers;
		
		auto requestBufferSpace(uint size, uint alignment)
		{
			assert(size <= VertexBufferSize);
			foreach(i, buffer; mBuffers[])
			{
				auto view = buffer.allocate(size, alignment);
				if(view.buffer is null)
					continue;
				
				return view;
			}
			
			import xgf.util.alloc;
			auto buffer = alloc!Buffer(VertexBufferSize);
			mBuffers.pushBack(buffer);
			return buffer.allocate(size, alignment);
		}
		
		void releaseBufferSpace(BufferView view)
		{
			view.buffer.free(view);
		}
	}
	
	void initialize()
	{
	}
	
	auto createVertexBuffer(uint size, VertexDeclaration* vd)
	{
		auto view = requestBufferSpace(size, vd.vertexSize);
		if(view.buffer)
		{
			foreach(ushort i, ref vb; mVertexBuffers[])
			{
				if(vb !is null)
					continue;
				
				vb = alloc!VertexBuffer(view.buffer, view.offset, view.size, view.offset / vd.vertexSize, vd);
				return VertexBufferHandle(i);
			}
			
			releaseBufferSpace(view);
			return VertexBufferHandle.invalid;
		}
		else
			return VertexBufferHandle.invalid;
	}
	
	void destroyVertexBuffer(VertexBufferHandle handle)
	{
		assert(handle != VertexBufferHandle.invalid, "Trying to destroy invalid handle.");
		auto vb = mVertexBuffers[handle.id];
		
		// releaseBufferSpace(BufferView(vb.buffer, vb.size, vb.offset));
		releaseBufferSpace(vb.view);
		
		free(vb);
		mVertexBuffers[handle.id] = null;
	}
	
	auto getVertexBuffer(VertexBufferHandle handle)
	{
		return mVertexBuffers[handle.id];
	}
	
	auto createIndexBuffer(uint size, IndexType type)
	{
		auto view = requestBufferSpace(size, 4); // TODO test if 1/2 alignment is possible -> ubyte/ushort
		if(view.buffer)
		{
			foreach(ushort i, ref ib; mIndexBuffers[])
			{
				if(ib !is null)
					continue;
				
				ib = alloc!IndexBuffer(view.buffer, view.offset, view.size / type, type);
				return IndexBufferHandle(i);
			}
			
			releaseBufferSpace(view);
			return IndexBufferHandle.invalid;
		}
		else
			return IndexBufferHandle.invalid;
	}
	
	void destroyIndexBuffer(IndexBufferHandle handle)
	{
		assert(handle != IndexBufferHandle.invalid);
		auto ib = mIndexBuffers[handle.id];
		
// 		releaseBufferSpace(BufferView(ib.buffer, ib.count * ib.type, ib.offset));
		releaseBufferSpace(ib.view);
		
		free(ib);
		mIndexBuffers[handle.id] = null;
	}
	
	auto getIndexBuffer(IndexBufferHandle handle)
	{
		return mIndexBuffers[handle.id];
	}
	
	auto createInstanceIndexBuffer(uint size, InstanceIndexType type)
	{
		auto view = requestBufferSpace(size, type.size);
		if(view.buffer)
		{
			foreach(ushort i, ref iib; mInstanceIndexBuffers[])
			{
				if(iib !is null)
					continue;
				
				iib = alloc!InstanceIndexBuffer(view.buffer, view.offset, view.size, type);
				return InstanceIndexBufferHandle(i);
			}
			
			releaseBufferSpace(view);
			return InstanceIndexBufferHandle.invalid;
		}
		else
			return InstanceIndexBufferHandle.invalid;
	}
	
	void destroyInstanceIndexBuffer(InstanceIndexBufferHandle handle)
	{
		assert(handle != InstanceIndexBufferHandle.invalid);
		auto iib = mInstanceIndexBuffers[handle.id];
		
		releaseBufferSpace(iib.view);
		
		free(iib);
		mInstanceIndexBuffers[handle.id] = null;
	}
	
	auto getInstanceIndexBuffer(InstanceIndexBufferHandle handle)
	{
		return mInstanceIndexBuffers[handle.id];
	}
}

private
{
	struct FreeSpace
	{
		uint offset;
		uint size;
	}
}
