module xgf.graphics.platform;

/// Platform specific data (e.g. Window)
struct Platform
{
	enum Type
	{
		none,
		xlib,
		xcb
	}
	Type type;
	union
	{
		Xlib xlib;
		Xcb xcb;
	}
}

struct Xlib
{
	import core.stdc.config : c_ulong;
	void* display;
	c_ulong window;
}

struct Xcb
{
	void* connection;
	uint window;
}