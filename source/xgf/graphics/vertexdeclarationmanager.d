module xgf.graphics.vertexdeclarationmanager;

import xgf.graphics.vertexdeclaration;

import xgf.util.alloc;
import xgf.util.array;
import xgf.util.hash;

enum MaxVertexDecls = 256;

struct VertexDeclarationManager
{
	private
	{
		VertexDeclaration*[MaxVertexDecls] mVertexDecls;
	}
	
	auto getVertexDeclarationHandle()(Attribute[] attributes)
	{
		auto hash = attributes.hash;
		
		foreach(ushort i, vd; mVertexDecls[])
		{
			if(vd)
			{
				if(hash == vd.hash)
					return VertexDeclarationHandle(i);
			}
		}
		
		foreach(ushort i, ref vd; mVertexDecls[])
		{
			if(!vd)
			{
				vd = alloc!VertexDeclaration(attributes);
				return VertexDeclarationHandle(i);
			}
		}
		
		return VertexDeclarationHandle.invalid;
	}
	
	auto getVertexDeclarationHandle(Vertex)()
	{
		enum attribs = attributes!Vertex;
		return getVertexDeclarationHandle(attribs);
	}
	
	auto getVertexDeclaration()(VertexDeclarationHandle handle)
	{
		if(handle == VertexDeclarationHandle.invalid)
			return null;
		
		return mVertexDecls[handle.id];
	}
}

private
{
	auto attributes(Vertex)()
	{
		import std.traits : FieldNameTuple, hasUDA, getUDAs;
		Attribute[FieldNameTuple!Vertex.length] ret;
		foreach(i, mem; FieldNameTuple!Vertex)
		{
			static assert(hasUDA!(mixin("Vertex."~mem), Attribute));
			ret[i] = getUDAs!(mixin("Vertex."~mem), Attribute)[0];
		}
		return ret;
	}
	
	template CTRange(size_t start, size_t end, bool inclusive = false)
	{
		import std.typetuple;
		static if(start >= end)
			alias CTRange = TypeTuple!();
		else
			alias CTRange = TypeTuple!(start, CTRange!(start+1, end, inclusive));
	}
}
