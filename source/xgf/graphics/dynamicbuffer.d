module xgf.graphics.dynamicbuffer;

import xgf.graphics.buffer;
import xgf.util.handle;

mixin(Handle!("DynamicBuffer"));

struct DynamicBuffer
{
	private
	{
		Buffer* mBuffer;
		uint mSize;
	}
	
	this(Buffer* buffer, uint size)
	{
		mBuffer = buffer;
		mSize = size;
	}
	
	@property auto buffer() { return mBuffer; }
	@property auto size() { return mSize; }
}

enum DynamicBufferBinding
{
	uniform,
	vertex
}
