module xgf.graphics.glcontext;

import xgf.graphics.egl.egl;
import xgf.graphics.egl.platform;
import xgf.util.log;
import xgf.graphics.glad.gl.loader;
import xgf.graphics.glad.gl.all;
import xgf.graphics.platform;

struct GLContext
{
	EGLDisplay display;
	EGLSurface surface;
	EGLContext context;
	EGLNativeDisplayType nativeDisplay;
	EGLNativeWindowType nativeWindow;

	bool initialize(Platform* plat) nothrow
	{
		EGLNativeDisplayType disp;
		EGLNativeWindowType win;
		switch(plat.type) with(Platform.Type)
		{
			case xlib:
				disp = plat.xlib.display;
				win = plat.xlib.window;
				break;
			default:
				assert(false);
		}

		nativeDisplay = disp;
		nativeWindow = win;

		display = eglGetDisplay(nativeDisplay);
		if(!display)
		{
			Log.error("[GLContext]: No Display connection found.");
			return false;
		}
		else
		{
			Log.info("[GLContext]: Found display connection.");
		}

		EGLint major;
		EGLint minor;
		EGLBoolean success = eglInitialize(display, &major, &minor);
		if(!success)
		{
			Log.error("[GLContext]: Failed to initialize Display connection: ", eglGetError());
			return false;
		}
		else
		{
			Log.info("[GLContext]: Initialized Display Connection.");
			Log.info("[GLContext]: EGL version: ", major, ".", minor);
		}

		success = eglBindAPI(EGL_OPENGL_API);
		if(!success)
		{
			Log.error("[GLContext]: Failed to set API to OpenGL.");
			return false;
		}
		else
		{
			Log.info("[GLContext]: Successfully set API to OpenGL.");
		}

		static EGLint[9] surfaceAttribs = [
			EGL_RED_SIZE, 8,
			EGL_GREEN_SIZE, 8,
			EGL_BLUE_SIZE, 8,
			EGL_CONFORMANT, EGL_OPENGL_BIT,
			EGL_NONE
		];

		EGLConfig config;
		EGLint numConfigs;
		success = eglChooseConfig(display, surfaceAttribs.ptr, &config, 1, &numConfigs);
		if(!success)
		{
			Log.error("[GLContext]: No config found: ", eglGetError());
			return false;
		}
		else
		{
			Log.info("[GLContext]: Acquired a config.");
			Log.info("[GLContext]: Config: ", config);
		}

		surface = eglCreateWindowSurface(display, config, nativeWindow, null);
		if(!surface)
		{
			Log.error("[GLContext]: Failed to create surface: ", eglGetError());
			return false;
		}
		else
		{
			Log.info("[GLContext]: Created surface.");
		}

		static EGLint[9] contextAttribs = [
			EGL_CONTEXT_MAJOR_VERSION, 4,
			EGL_CONTEXT_MINOR_VERSION, 5,
			EGL_CONTEXT_OPENGL_PROFILE_MASK, EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT,
			EGL_CONTEXT_OPENGL_DEBUG, DebugContext,
			EGL_NONE
		];

		context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttribs.ptr);
		if(!context)
		{
			Log.error("[GLContext]: Failed to create context: ", eglGetError());
			return false;
		}
		else
		{
			Log.info("[GLContext]: Created context.");
		}

		return true;
	}

	void terminate() nothrow
	{
		if(context)
		{
			// unbind surfaces and context so they can be destroyed
			eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
		}

		eglTerminate(display);

		this = this.init;
	}

	bool makeCurrent() nothrow
	{
		auto success = eglMakeCurrent(display, surface, surface, context);
		if(!success)
		{
			Log.error("[GLContext]: Failed to make context current: ", eglGetError());
			return false;
		}
		else
		{
			Log.info("[GLContext]: Context made current.");
			return true;
		}
	}

	void swapBuffers() nothrow
	{
		eglSwapBuffers(display, surface);
	}

	void setSwapInterval(int value)
	{
		eglSwapInterval(display, value);
	}

	bool loadGL()
	{
		if(!gladLoadGL())
		{
			Log.error("[GLContext]: Failed to load gl functions.");
			return false;
		}
		return true;
	}

	void setupDebugCallback()
	{
		debug
		{
			glEnable(GL_DEBUG_OUTPUT);
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
			glDebugMessageCallback(cast(GLDEBUGPROC)&logGlMessage, null);
		}
	}
}

debug
{
	enum DebugContext = EGL_TRUE;
}
else
{
	enum DebugContext = EGL_FALSE;
}

private
{
	extern(C)
	{
	void logGlMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const(GLchar*) message, void* userParam)
	{
		import core.stdc.string : strlen;
	// 	info("GLMessageHeader: [Source: ", source, " Type: ", type, " Id: ", id, " Severity: ", severity, " Length: ", length, "]");
		Log.debug_("[GLDebug]: [Source: ", source.GLDebugSourceToString, " Type: ", type.GLDebugTypeToString, " Id: ", id, " Severity: ", severity.GLDebugSeverityToString, " Length: ", length, "]");
		Log.debug_("[GLDebug]: ", message[0..message.strlen]); 
	}
	}

	string GLDebugSourceToString(GLenum source)
	{
		switch(source)
		{
			case GL_DEBUG_SOURCE_API: return "GL_DEBUG_SOURCE_API";
			case GL_DEBUG_SOURCE_WINDOW_SYSTEM: return "GL_DEBUG_SOURCE_WINDOW_SYSTEM";
			case GL_DEBUG_SOURCE_SHADER_COMPILER: return "GL_DEBUG_SOURCE_SHADER_COMPILER";
			case GL_DEBUG_SOURCE_THIRD_PARTY: return "GL_DEBUG_SOURCE_THIRD_PARTY";
			case GL_DEBUG_SOURCE_APPLICATION: return "GL_DEBUG_SOURCE_APPLICATION";
			case GL_DEBUG_SOURCE_OTHER: return "GL_DEBUG_SOURCE_OTHER";
			default: return "Unknown Source";
		}
	}

	string GLDebugTypeToString(GLenum type)
	{
		switch(type)
		{
			case GL_DEBUG_TYPE_ERROR: return "GL_DEBUG_TYPE_ERROR";
			case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: return "GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR";
			case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: return "GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR";
			case GL_DEBUG_TYPE_PORTABILITY: return "GL_DEBUG_TYPE_PORTABILITY";
			case GL_DEBUG_TYPE_PERFORMANCE: return "GL_DEBUG_TYPE_PERFORMANCE";
			case GL_DEBUG_TYPE_MARKER: return "GL_DEBUG_TYPE_MARKER";
			case GL_DEBUG_TYPE_PUSH_GROUP: return "GL_DEBUG_TYPE_PUSH_GROUP";
			case GL_DEBUG_TYPE_POP_GROUP: return "GL_DEBUG_TYPE_POP_GROUP";
			case GL_DEBUG_TYPE_OTHER: return "GL_DEBUG_TYPE_OTHER";
			default: return "Unknown Type.";
		}
	}

	string GLDebugSeverityToString(GLenum severity)
	{
		switch(severity)
		{
			case GL_DEBUG_SEVERITY_HIGH: return "GL_DEBUG_SEVERITY_HIGH";
			case GL_DEBUG_SEVERITY_MEDIUM: return "GL_DEBUG_SEVERITY_MEDIUM";
			case GL_DEBUG_SEVERITY_LOW: return "GL_DEBUG_SEVERITY_LOW";
			case GL_DEBUG_SEVERITY_NOTIFICATION: return "GL_DEBUG_SEVERITY_NOTIFICATION";
			default: return "Unknown severity level";
		}
	}
}