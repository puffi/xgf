module xgf.graphics.indexbuffer;

import xgf.graphics.buffer;

import xgf.util.handle;

struct IndexBuffer
{
	private
	{
		Buffer* mBuffer;
		uint mOffset;
		uint mCount;
		IndexType mType;
	}
	
	@property auto buffer() { return mBuffer; }
	@property auto buffer() const { return mBuffer; }
	@property auto offset() { return mOffset; }
	@property auto offset() const { return mOffset; }
	@property auto count() { return mCount; }
	@property auto count() const { return mCount; }
	@property auto type() { return mType; }
	@property auto type() const { return mType; }
	
	@property auto view() { return BufferView(mBuffer, mCount * mType, mOffset); }
}

enum IndexType
{
	u8 = 1,
	u16 = 2,
	u32 = 4
}

mixin(Handle!"IndexBuffer");

template indexType(Index)
{
	static if(is(Index == ubyte))
		enum indexType = IndexType.u8;
	else static if(is(Index == ushort))
		enum indexType = IndexType.u16;
	else static if(is(Index == uint))
		enum indexType = IndexType.u32;
	else static assert(false, "Type " ~ Index.stringof ~ " is not a valid IndexType.");
}
