module xgf.graphics.samplermanager;

import xgf.graphics.sampler;

import xgf.util.array;
import xgf.util.alloc;

import xgf.graphics.glad.gl.all;

enum MaxSamplers = 256;

struct SamplerManager
{
	private
	{
		StaticArray!(Sampler*, MaxSamplers) mSamplers;
	}
	
	auto getSampler(Filter minFilter, Filter magFilter, Wrap wrapS = Wrap.repeat, Wrap wrapT = Wrap.repeat, Wrap wrapR = Wrap.repeat)
	{
		auto h = hash(minFilter, magFilter, wrapS, wrapT, wrapR);
		
		foreach(ushort i, sampler; mSamplers[])
		{
			if(sampler)
			{
				if(sampler.hash == h)
					return SamplerHandle(i);
			}
		}
		
		foreach(ushort i, ref sampler; mSamplers[])
		{
			if(sampler !is null)
				continue;
			
			uint handle;
			glCreateSamplers(1, &handle);
			sampler = alloc!Sampler(handle, h, minFilter, magFilter, wrapS, wrapT, wrapR);
			
			// set properties
			glSamplerParameteri(handle, GL_TEXTURE_MIN_FILTER, minFilter.glType);
			glSamplerParameteri(handle, GL_TEXTURE_MAG_FILTER, magFilter.glType);
			glSamplerParameteri(handle, GL_TEXTURE_WRAP_S, wrapS.glType);
			glSamplerParameteri(handle, GL_TEXTURE_WRAP_T, wrapT.glType);
			glSamplerParameteri(handle, GL_TEXTURE_WRAP_R, wrapR.glType);
			
			return SamplerHandle(i);
		}
		
		return SamplerHandle.invalid;
	}
	
// 	auto createSampler()
// 	{
// 		foreach(ushort i, ref sampler; mSamplers[])
// 		{
// 			if(sampler !is null)
// 				continue;
// 			
// 			uint handle;
// 			glCreateSamplers(1, &handle);
// 			
// 			sampler = alloc!Sampler(handle);
// 			return SamplerHandle(i);
// 		}
// 		
// 		return SamplerHandle.invalid;
// 	}
	
	auto getSampler(SamplerHandle handle)
	{
		assert(handle != SamplerHandle.invalid);
		assert(handle.id < MaxSamplers);
		
		return mSamplers[handle.id];
	}
	
	void destroySampler(SamplerHandle handle)
	{
		assert(handle != SamplerHandle.invalid);
		assert(handle.id < MaxSamplers);
		
		auto id = mSamplers[handle.id].handle;
		glDeleteSamplers(1, &id);
		free(mSamplers[handle.id]);
		mSamplers[handle.id] = null;
	}
	
	void bindSampler(Sampler* sampler, uint binding)
	{
		if(sampler is null)
		{
			glBindSampler(binding, 0);
		}
		else
		{
			glBindSampler(binding, sampler.handle);
		}
	}
	
// 	void setSamplerMinFilter(SamplerHandle handle, Filter filter)
// 	{
// 		assert(handle != SamplerHandle.invalid);
// 		assert(handle.id < MaxSamplers);
// 		
// 		mSamplers[handle.id].minFilter = filter;
// 		glSamplerParameteri(mSamplers[handle.id].handle, GL_TEXTURE_MIN_FILTER, filter.glType);
// 	}
// 	
// 	void setSamplerMagFilter(SamplerHandle handle, Filter filter)
// 	{
// 		assert(handle != SamplerHandle.invalid);
// 		assert(handle.id < MaxSamplers);
// 		
// 		mSamplers[handle.id].magFilter = filter;
// 		glSamplerParameteri(mSamplers[handle.id].handle, GL_TEXTURE_MAG_FILTER, filter.glType);
// 	}
// 	
// 	void setSamplerWrapS(SamplerHandle handle, Wrap wrap)
// 	{
// 		assert(handle != SamplerHandle.invalid);
// 		assert(handle.id < MaxSamplers);
// 		
// 		mSamplers[handle.id].wrapS = wrap;
// 		glSamplerParameteri(mSamplers[handle.id].handle, GL_TEXTURE_WRAP_S, wrap.glType);
// 	}
// 	
// 	void setSamplerWrapT(SamplerHandle handle, Wrap wrap)
// 	{
// 		assert(handle != SamplerHandle.invalid);
// 		assert(handle.id < MaxSamplers);
// 		
// 		mSamplers[handle.id].wrapT = wrap;
// 		glSamplerParameteri(mSamplers[handle.id].handle, GL_TEXTURE_WRAP_T, wrap.glType);
// 	}
// 	
// 	void setSamplerWrapR(SamplerHandle handle, Wrap wrap)
// 	{
// 		assert(handle != SamplerHandle.invalid);
// 		assert(handle.id < MaxSamplers);
// 		
// 		mSamplers[handle.id].wrapR = wrap;
// 		glSamplerParameteri(mSamplers[handle.id].handle, GL_TEXTURE_WRAP_R, wrap.glType);
// 	}
}

private
{
	enum FilterShift1 = 0;
	enum FilterShift2 = 1;
	enum WrapShift1 = 2;
	enum WrapShift2 = 4;
	enum WrapShift3 = 6;
	
	auto hash(Filter f1, Filter f2, Wrap w1, Wrap w2, Wrap w3)
	{
		uint hash;
		hash |= f1 << FilterShift1;
		hash |= f2 << FilterShift2;
		hash |= w1 << WrapShift1;
		hash |= w2 << WrapShift2;
		hash |= w3 << WrapShift3;
		return hash;
	}
	
	auto glType(Filter filter)
	{
		final switch(filter) with(Filter)
		{
			case nearest: return GL_NEAREST;
			case linear: return GL_LINEAR;
		}
	}

	auto glType(Wrap wrap)
	{
		final switch(wrap) with(Wrap)
		{
			case repeat: return GL_REPEAT;
			case mirroredRepeat: return GL_MIRRORED_REPEAT;
			case clamp: return GL_CLAMP_TO_EDGE;
		}
	}
}
