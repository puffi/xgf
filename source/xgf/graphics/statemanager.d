module xgf.graphics.statemanager;

import xgf.graphics.state;

import xgf.graphics.glad.gl.all;

import xgf.math : ivec4;

struct StateManager
{
	private
	{
		
	}
	
	void enableBlending()
	{
		glEnable(GL_BLEND);
	}
	
	void disableBlending()
	{
		glDisable(GL_BLEND);
	}
	
	void setBlendFunction(BlendState.Func func)
	{
		glBlendFunc(func.source.glType, func.destination.glType);
	}
	
	void setBlendEquation(BlendState.Equation eq)
	{
		glBlendEquation(eq.glType);
	}
	
	void enableDepthTest()
	{
		glEnable(GL_DEPTH_TEST);
	}
	
	void disableDepthTest()
	{
		glDisable(GL_DEPTH_TEST);
	}
	
	void setDepthFunction(DepthState.Func func)
	{
		glDepthFunc(func.glType);
	}
	
	void enableCulling()
	{
		glEnable(GL_CULL_FACE);
	}
	
	void disableCulling()
	{
		glDisable(GL_CULL_FACE);
	}
	
	void setCullMode(CullState.Mode mode)
	{
		glCullFace(mode.glType);
	}
	
	void setCullFrontFace(CullState.FrontFace front)
	{
		glFrontFace(front.glType);
	}
	
	void setViewport(ivec4 vp)
	{
		glViewport(vp.x, vp.y, vp.z, vp.w);
	}
	
	void enableScissorTest()
	{
		glEnable(GL_SCISSOR_TEST);
	}
	
	void disableScissorTest()
	{
		glDisable(GL_SCISSOR_TEST);
	}
	
	void setScissor(ivec4 rect)
	{
		glScissor(rect.x, rect.y, rect.z, rect.w);
	}
}

private
{
	auto glType(BlendState.Factor factor)
	{
		final switch(factor) with(BlendState.Factor)
		{
			case zero: return GL_ZERO;
			case one: return GL_ONE;
			case srcColor: return GL_SRC_COLOR;
			case oneMinusSrcColor: return GL_ONE_MINUS_SRC_COLOR;
			case dstColor: return GL_DST_COLOR;
			case oneMinusDstColor: return GL_ONE_MINUS_DST_COLOR;
			case srcAlpha: return GL_SRC_ALPHA;
			case oneMinusSrcAlpha: return GL_ONE_MINUS_SRC_ALPHA;
			case dstAlpha: return GL_DST_ALPHA;
			case oneMinusDstAlpha: return GL_ONE_MINUS_DST_ALPHA;
			case constantColor: return GL_CONSTANT_COLOR;
			case oneMinusConstantColor: return GL_ONE_MINUS_CONSTANT_COLOR;
			case constantAlpha: return GL_CONSTANT_ALPHA;
			case oneMinusConstantAlpha: return GL_ONE_MINUS_CONSTANT_ALPHA;
			case srcAlphaSaturate: return GL_SRC_ALPHA_SATURATE;
			case src1Color: return GL_SRC1_COLOR;
			case oneMinusSrc1Color: return GL_ONE_MINUS_SRC1_COLOR;
// 			case src1Alpha: return GL_SRC1_ALPHA;
			case oneMinusSrc1Alpha: return GL_ONE_MINUS_SRC1_ALPHA;
		}
	}
	
	auto glType(BlendState.Equation eq)
	{
		final switch(eq) with(BlendState.Equation)
		{
			case add: return GL_FUNC_ADD;
			case subtract: return GL_FUNC_SUBTRACT;
			case reverseSubtract: return GL_FUNC_REVERSE_SUBTRACT;
			case min: return GL_MIN;
			case max: return GL_MAX;
		}
	}
	
	auto glType(DepthState.Func func)
	{
		final switch(func) with(DepthState.Func)
		{
			case never: return GL_NEVER;
			case less: return GL_LESS;
			case equal: return GL_EQUAL;
			case lequal: return GL_LEQUAL;
			case greater: return GL_GREATER;
			case notequal: return GL_NOTEQUAL;
			case gequal: return GL_GEQUAL;
			case always: return GL_ALWAYS;
		}
	}
	
	auto glType(CullState.Mode mode)
	{
		final switch(mode) with(CullState.Mode)
		{
			case front: return GL_FRONT;
			case back: return GL_BACK;
			case frontAndBack: return GL_FRONT_AND_BACK;
		}
	}
	
	auto glType(CullState.FrontFace front)
	{
		final switch(front) with(CullState.FrontFace)
		{
			case cw: return GL_CW;
			case ccw: return GL_CCW;
		}
	}
}
