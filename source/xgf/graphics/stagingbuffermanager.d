module xgf.graphics.stagingbuffermanager;

import xgf.util.array;
import xgf.util.alloc;

import xgf.graphics.buffer;

import xgf.graphics.glad.gl.all;

enum StagingBufferSize = 8 * 1024 * 1024;

struct StagingBufferManager
{
	private
	{
		DynamicArray!(Buffer*) mWriteBuffers;
// 		DynamicArray!(uint)[3] mFreeSpaces;
		
		uint mFrame;
		
		void addWriteBuffer()
		{
			auto buffer = alloc!Buffer(StagingBufferSize, Buffering.triple, BufferFlag.persistent | BufferFlag.coherent | BufferFlag.write);
			mWriteBuffers.pushBack(buffer);
// 			mFreeSpaces[0].pushBack(StagingBufferSize);
// 			mFreeSpaces[1].pushBack(StagingBufferSize);
// 			mFreeSpaces[2].pushBack(StagingBufferSize);
		}
		
		auto requestBufferSpace(uint size)
		{
// 			assert(size <= StagingBufferSize);
// 			foreach(i, ref v; mFreeSpaces[mFrame])
// 			{
// 				if(v < size)
// 					continue;
// 				
// 				auto offset = StagingBufferSize - v + mFrame * StagingBufferSize;
// 				v -= size;
// 				
// 				return BufferView(mWriteBuffers[i], size, offset);
// 			}
// 			
// 			addWriteBuffer();
// 			mFreeSpaces[mFrame][$-1] -= size;
// 			return BufferView(mWriteBuffers[$-1], size, 0 + mFrame * StagingBufferSize);
			assert(size <= StagingBufferSize, "Not enough space.");
			
			foreach(i, buffer; mWriteBuffers[])
			{
				auto view = buffer.allocate(size, 0, mFrame);
				if(view.buffer is null)
					continue;
				
				return view;
			}
			
			auto buffer = alloc!Buffer(StagingBufferSize, Buffering.triple, BufferFlag.persistent | BufferFlag.coherent | BufferFlag.write);
			mWriteBuffers.pushBack(buffer);
			return buffer.allocate(size, 0);
		}
	}
	
	void beginFrame(uint frame)
	{
		mFrame = frame;
		foreach(buffer; mWriteBuffers[])
		{
			buffer.clear(mFrame);
		}
// 		mFreeSpaces[mFrame][][] = StagingBufferSize;
	}
	
	auto upload(ubyte[] data, BufferView target = BufferView.init)
	{
		auto view = requestBufferSpace(cast(uint)data.length);
		if(view.buffer)
		{
			view.buffer.upload(data, view.offset);
			if(target.buffer)
			{
				glCopyNamedBufferSubData(view.buffer.handle, target.buffer.handle, view.offset, target.offset, view.size);
			}
		}
		return view;
	}
}
