module xgf.graphics.uniformbuffer;

import xgf.graphics.buffer;
import xgf.util.handle;

mixin(Handle!"UniformBuffer");

struct UniformBuffer
{
	private
	{
		Buffer* mBuffer;
		uint mSize;
	}
	
	this(Buffer* buffer, uint size)
	{
		mBuffer = buffer;
		mSize = size;
	}
	
	@property auto buffer() { return mBuffer; }
	@property auto size() { return mSize; }
}
