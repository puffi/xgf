module xgf.graphics.instanceindexbuffer;

import xgf.util.handle;
import xgf.graphics.buffer;

struct InstanceIndexBuffer
{
	private
	{
		Buffer* mBuffer;
		uint mOffset;
		uint mSize;
		InstanceIndexType mType;
	}
	
	@property auto buffer() { return mBuffer; }
	@property auto offset() { return mOffset; }
	@property auto size() { return mSize; }
	@property auto type() { return mType; }
	@property auto buffer() const { return mBuffer; }
	@property auto offset() const { return mOffset; }
	@property auto size() const { return mSize; }
	@property auto type() const { return mType; }
	@property auto view() { return BufferView(mBuffer, mSize, mOffset); }
// 	@property auto view() const { return BufferView(mBuffer, mSize, mOffset); }
}

enum InstanceIndexType
{
	u32
}

mixin(Handle!"InstanceIndexBuffer");

auto size(InstanceIndexType type)
{
	final switch(type)
	{
		case InstanceIndexType.u32: return 4;
	}
}
