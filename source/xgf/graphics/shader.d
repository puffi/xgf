module xgf.graphics.shader;

import xgf.util.handle;

mixin(Handle!"VertexShader");
mixin(Handle!"FragmentShader");
// mixin(Handle!"GeometryShader");

struct Shader
{
	private
	{
		uint mHandle;
		ShaderType mType;
	}
	
	this(ShaderType type, uint handle)
	{
		mHandle = handle;
		mType = type;
	}
	
	@property auto handle() { return mHandle; }
	@property auto type() { return mType; }
}

enum ShaderType
{
	vertex,
	fragment,
	geometry
}
