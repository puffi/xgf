module xgf.graphics.state;

import xgf.math;

struct BlendState
{
	bool enabled;
	Func func;
	Equation equation;
	
	static struct Func
	{
		Factor source = Factor.one;
		Factor destination = Factor.zero;
	}
	
	enum Factor
	{
		zero,
		one,
		srcColor,
		oneMinusSrcColor,
		dstColor,
		oneMinusDstColor,
		srcAlpha,
		oneMinusSrcAlpha,
		dstAlpha,
		oneMinusDstAlpha,
		constantColor,
		oneMinusConstantColor,
		constantAlpha,
		oneMinusConstantAlpha,
		srcAlphaSaturate,
		src1Color,
		oneMinusSrc1Color,
	// 	src1Alpha,
		oneMinusSrc1Alpha
	}
	
	enum Equation
	{
		add,
		subtract,
		reverseSubtract,
		min,
		max
	}
}

struct DepthState
{
	bool enabled;
	Func func = Func.less;
	
	enum Func
	{
		never,
		less,
		equal,
		lequal,
		greater,
		notequal,
		gequal,
		always
	}
}

struct CullState
{
	bool enabled;
	Mode mode = Mode.back;
	FrontFace front = FrontFace.ccw;
	
	enum Mode
	{
		front,
		back,
		frontAndBack
	}
	
	enum FrontFace
	{
		cw,
		ccw
	}
}

struct ScissorState
{
	bool enabled;
	ivec4 rect;
}
