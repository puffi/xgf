module xgf.graphics.uniformbuffermanager;

import xgf.util.array;
import xgf.util.alloc;

import xgf.graphics.buffer;
import xgf.graphics.uniformbuffer;

enum MaxUniformBuffers = 4096;
enum MaxUniformSize = 2^^16;

struct UniformBufferManager
{
	private
	{
		StaticArray!(UniformBuffer*, MaxUniformBuffers) mUniformBuffers;
	}
	
	void initialise()
	{
	}
	
	auto createUniformBuffer(uint size)
	{
		assert(size <= MaxUniformSize);
		assert(size % 256 == 0);
		
		foreach(ushort i, ref ub; mUniformBuffers[])
		{
			if(ub !is null)
				continue;
			
			auto buffer = alloc!Buffer(size);
			ub = alloc!UniformBuffer(buffer, size);
			return UniformBufferHandle(i);
		}
		
		return UniformBufferHandle.invalid;
	}
	
	auto getUniformBuffer(UniformBufferHandle handle)
	{
		assert(handle.id < MaxUniformBuffers);
		
		return mUniformBuffers[handle.id];
	}
	
	void destroyUniformBuffer(UniformBufferHandle handle)
	{
		assert(handle.id < MaxUniformBuffers);
		
		free(mUniformBuffers[handle.id].buffer);
		free(mUniformBuffers[handle.id]);
		mUniformBuffers[handle.id] = null;
	}
}
