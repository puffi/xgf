module xgf.graphics.vertexbuffer;

import xgf.graphics.buffer;
import xgf.graphics.vertexdeclaration;

import xgf.util.handle;

struct VertexBuffer
{
	private
	{
		Buffer* mBuffer;
		uint mOffset;
		uint mSize;
		uint mBaseVertex;
		VertexDeclaration* mVertexDecl;
	}
	
	@property auto buffer() { return mBuffer; }
	@property auto offset() { return mOffset; }
	@property auto size() { return mSize; }
	@property auto baseVertex() { return mBaseVertex; }
	@property auto vertexDecl() { return mVertexDecl; }
	@property auto buffer() const { return mBuffer; }
	@property auto offset() const { return mOffset; }
	@property auto size() const { return mSize; }
	@property auto baseVertex() const { return mBaseVertex; }
	@property auto vertexDecl() const { return mVertexDecl; }
	
	@property auto view() { return BufferView(mBuffer, mSize, mOffset); }
// 	@property auto view() const { return BufferView(mBuffer, mSize, mOffset); }
}

mixin(Handle!"VertexBuffer");
