module xgf.graphics.vertexdeclaration;

import xgf.util.handle;
import xgf.util.array;
import xgf.util.hash;

struct VertexDeclaration
{
	private
	{
		Array!Attribute mAttributes;
		Array!uint mOffsets;
		uint mVertexSize;
		uint mHash;
		
		void buildOffsets()
		{
			mOffsets = Array!uint(mAttributes.size);
			uint offset = 0;
			foreach(i, attr; mAttributes[])
			{
				mOffsets[i] = offset;
				offset += attr.size;
			}
		}
	}
	
	this(Attribute[] attributes)
	{
		mAttributes = Array!Attribute(attributes.length);
		mAttributes[][] = attributes[];
		buildOffsets();
		mVertexSize = attributes.vertexSize;
		mHash = attributes.hash;
	}
	
	@property auto attributes() { return mAttributes[]; }
	@property auto offsets() { return mOffsets[]; }
	@property auto vertexSize() { return mVertexSize; }
	@property auto hash() { return mHash; }
}

mixin(Handle!"VertexDeclaration");

struct Attribute
{
	ubyte location;
	Type type;
	ubyte num;
	bool normalized;
	
	enum Type : ubyte
	{
		u8,
		u16,
		u32,
		f32
	}
}

private
{
	auto vertexSize(Attribute[] attributes)
	{
		uint size = 0;
		foreach(attr; attributes)
		{
			size += attr.size;
		}
		return size;
	}
	
	auto size(Attribute attr)
	{
		final switch(attr.type)
		{
			case Attribute.Type.u8: return 1 * attr.num;
			case Attribute.Type.u16: return 2 * attr.num;
			case Attribute.Type.u32: return 4 * attr.num;
			case Attribute.Type.f32: return 4 * attr.num;
		}
	}
}

