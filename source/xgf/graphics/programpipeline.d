module xgf.graphics.programpipeline;

import xgf.graphics.glad.gl.all;
import xgf.graphics.shader;
import xgf.graphics.uniformbuffer;
import xgf.graphics.dynamicbuffer;

struct ProgramPipeline
{
	private
	{
		uint mHandle;
	}
	
	void initialize()
	{
		glCreateProgramPipelines(1, &mHandle);
		glBindProgramPipeline(mHandle);
	}
	
	~this()
	{
		glBindProgramPipeline(0);
		if(mHandle)
			glDeleteProgramPipelines(1, &mHandle);
	}
	
	void setVertexShader(Shader* vs)
	{
		if(vs)
		{
			glUseProgramStages(mHandle, ShaderType.vertex.glBitType, vs.handle);
		}
		else
		{
			glUseProgramStages(mHandle, ShaderType.vertex.glBitType, 0);
		}
	}
	
	void setFragmentShader(Shader* fs)
	{
		if(fs)
		{
			glUseProgramStages(mHandle, ShaderType.fragment.glBitType, fs.handle);
		}
		else
		{
			glUseProgramStages(mHandle, ShaderType.fragment.glBitType, 0);
		}
	}
	
	void bindUniformBuffer(UniformBuffer* ub, uint location, uint offset = 0)
	{
		if(ub)
			glBindBufferRange(GL_UNIFORM_BUFFER, location, ub.buffer.handle, offset, ub.size);
		else
			glBindBufferRange(GL_UNIFORM_BUFFER, location, 0, 0, 0);
	}
	
	void bindDynamicUniformBuffer(DynamicBuffer* db, uint location, uint offset = 0)
	{
		if(db)
			glBindBufferRange(GL_UNIFORM_BUFFER, location, db.buffer.handle, offset, db.size);
		else
			glBindBufferRange(GL_UNIFORM_BUFFER, location, 0, 0, 0);
	}
}

private
{
	auto glBitType(ShaderType type)
	{
		final switch(type)
		{
			case ShaderType.vertex: return GL_VERTEX_SHADER_BIT;
			case ShaderType.fragment: return GL_FRAGMENT_SHADER_BIT;
			case ShaderType.geometry: return GL_GEOMETRY_SHADER_BIT;
		}
	}
}
