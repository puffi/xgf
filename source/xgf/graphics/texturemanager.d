module xgf.graphics.texturemanager;

import xgf.util.array;
import xgf.util.alloc;
import xgf.graphics.glad.gl.all;

import xgf.graphics.texture;

enum MaxTextures = 256;

struct TextureManager
{
	private
	{
		StaticArray!(Texture*, MaxTextures) mTextures;
	}
	
	auto createTexture2D(ubyte[] data, uint width, uint height, Format fmt)
	{
		assert(data.length == width * height * fmt.size);
		
		foreach(ushort i, ref tex; mTextures[])
		{
			if(tex !is null)
				continue;
			
			uint handle;
			glCreateTextures(Type.tex2d.glType, 1, &handle);
			glTextureStorage2D(handle, 1, fmt.glTypeSized, width, height);
			glTextureSubImage2D(handle, 0, 0, 0, width, height, fmt.glType, GL_UNSIGNED_BYTE, data.ptr);
			
			tex = alloc!Texture(handle, Type.tex2d, width, height, 0, fmt);
			return TextureHandle(i);
		}
		
		return TextureHandle.invalid;
	}
	
	void updateTexture2D(TextureHandle handle, ubyte[] data, uint width, uint height, uint x = 0, uint y = 0)
	{
		assert(handle != TextureHandle.invalid);
		auto tex = mTextures[handle.id];
		assert(tex.type == Type.tex2d);
		
		glTextureSubImage2D(tex.handle, 0, x, y, width, height, tex.format.glType, GL_UNSIGNED_BYTE, data.ptr);
	}
	
	auto getTexture(TextureHandle handle)
	{
		assert(handle != TextureHandle.invalid);
		assert(handle.id < MaxTextures);
		
		return mTextures[handle.id];
	}
	
	void destroyTexture(TextureHandle handle)
	{
		assert(handle != TextureHandle.invalid);
		assert(handle.id < MaxTextures);
		
		auto id = mTextures[handle.id].handle;
		glDeleteTextures(1, &id);
		free(mTextures[handle.id]);
		mTextures[handle.id] = null;
	}
	
	void bindTexture(Texture* tex, uint binding, Type type = Type.init)
	{
		glActiveTexture(GL_TEXTURE0 + binding);
		if(tex is null)
		{
			glBindTexture(type, 0); // use previous type
		}
		else
		{
			glBindTexture(tex.type.glType, tex.handle);
		}
	}
}

private
{
	auto size(Format fmt)
	{
		final switch(fmt) with(Format)
		{
			case r8: return 1;
			case rgb8: return 3;
			case rgba8: return 4;
			case bgra8: return 4;
		}
	}

	auto glType(Type type)
	{
		final switch(type) with(Type)
		{
			case none: assert(false);
			case tex1d: return GL_TEXTURE_1D;
			case tex2d: return GL_TEXTURE_2D;
			case tex3d: return GL_TEXTURE_3D;
		}
	}

	auto glTypeSized(Format fmt)
	{
		final switch(fmt) with(Format)
		{
			case r8: return GL_R8;
			case rgb8: return GL_RGB8;
			case rgba8: return GL_RGBA8;
			case bgra8: return GL_RGBA8;
		}
	}

	auto glType(Format fmt)
	{
		final switch(fmt) with(Format)
		{
			case r8: return GL_RED;
			case rgb8: return GL_RGB;
			case rgba8: return GL_RGBA;
			case bgra8: return GL_BGRA;
		}
	}
}
