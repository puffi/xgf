module xgf.graphics.rendertarget;

import xgf.util.handle;

import xgf.graphics.texture;

struct RenderTarget
{
	private
	{
		uint mHandle;
		TextureHandle[Attachment.max+1] mAttachments;
	}
	
	@property auto handle() const { return mHandle; }
	@property void handle(uint v) { mHandle = v; }
	@property auto attachments() const { return mAttachments[]; }
}

enum Attachment
{
	color0,
	color1,
	color2,
	color3,
	color4,
	color5,
	color6,
	color7,
	depth,
	stencil
}

mixin(Handle!"RenderTarget");

