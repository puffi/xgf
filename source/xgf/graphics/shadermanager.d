module xgf.graphics.shadermanager;

import xgf.graphics.shader;
import xgf.util.array;
import xgf.util.alloc;
import xgf.graphics.glad.gl.all;

enum MaxVertexShaders = 2048;
enum MaxFragmentShaders = 2048;

struct ShaderManager
{
	private
	{
		StaticArray!(Shader*, MaxVertexShaders) mVertexShaders;
		StaticArray!(Shader*, MaxFragmentShaders) mFragmentShaders;
		
		
	}
	
	void initialize()
	{
// 		mVertexShaders.resize(MaxVertexShaders);
// 		mFragmentShaders.resize(MaxFragmentShaders);
	}
	
	auto createVertexShader()(in ubyte[] data)
	{
		foreach(ushort i, ref sh; mVertexShaders[])
		{
			if(sh !is null)
				continue;
			
			uint shader = glCreateShader(ShaderType.vertex.glType);
			int size = cast(int)data.length;
			auto ptr = cast(char*)data.ptr;
			glShaderSource(shader, 1, &ptr, &size);
			glCompileShader(shader);
		
			uint handle = glCreateProgram();
			glAttachShader(handle, shader);
			glProgramParameteri(handle, GL_PROGRAM_SEPARABLE, GL_TRUE);
			glLinkProgram(handle);
		
			glDeleteShader(shader);
			
			sh = alloc!Shader(ShaderType.vertex, handle);
			return VertexShaderHandle(i);
		}
		
		return VertexShaderHandle.invalid;
	}
	
	void destroyVertexShader(VertexShaderHandle handle)
	{
		assert(handle != VertexShaderHandle.invalid);
		assert(handle.id < MaxVertexShaders);
		
		glDeleteProgram(mVertexShaders[handle.id].handle);
		free(mVertexShaders[handle.id]);
		mVertexShaders[handle.id] = null;
	}
	
	auto getVertexShader(VertexShaderHandle handle)
	{
		assert(handle != VertexShaderHandle.invalid);
		assert(handle.id < MaxVertexShaders);
		
		return mVertexShaders[handle.id];
	}
	
	auto createFragmentShader()(in ubyte[] data)
	{
		foreach(ushort i, ref sh; mFragmentShaders[])
		{
			if(sh !is null)
				continue;
			
			uint shader = glCreateShader(ShaderType.fragment.glType);
			int size = cast(int)data.length;
			auto ptr = cast(char*)data.ptr;
			glShaderSource(shader, 1, &ptr, &size);
			glCompileShader(shader);
		
			uint handle = glCreateProgram();
			glAttachShader(handle, shader);
			glProgramParameteri(handle, GL_PROGRAM_SEPARABLE, GL_TRUE);
			glLinkProgram(handle);
		
			glDeleteShader(shader);
			
			sh = alloc!Shader(ShaderType.fragment, handle);
			return FragmentShaderHandle(i);
		}
		
		return FragmentShaderHandle.invalid;
	}
	
	void destroyFragmentShader(FragmentShaderHandle handle)
	{
		assert(handle != FragmentShaderHandle.invalid);
		assert(handle.id < MaxFragmentShaders);
		
		glDeleteProgram(mFragmentShaders[handle.id].handle);
		free(mFragmentShaders[handle.id]);
		mFragmentShaders[handle.id] = null;
	}
	
	auto getFragmentShader(FragmentShaderHandle handle)
	{
		assert(handle != FragmentShaderHandle.invalid);
		assert(handle.id < MaxFragmentShaders);
		
		return mFragmentShaders[handle.id];
	}
}

private
{
	auto glType(ShaderType type)
	{
		final switch(type)
		{
			case ShaderType.vertex: return GL_VERTEX_SHADER;
			case ShaderType.fragment: return GL_FRAGMENT_SHADER;
			case ShaderType.geometry: return GL_GEOMETRY_SHADER;
		}
	}
}
