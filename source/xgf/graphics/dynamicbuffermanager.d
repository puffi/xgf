module xgf.graphics.dynamicbuffermanager;

import xgf.graphics.buffer;
import xgf.graphics.dynamicbuffer;

import xgf.util.array;
import xgf.util.alloc;

enum MaxBufferSize = 8 * 1024 * 1024;
enum MaxDynamicBuffers = 64;

struct DynamicBufferManager
{
	private
	{
		StaticArray!(DynamicBuffer*, MaxDynamicBuffers) mDynamicBuffers;
		
		uint mFrame;
	}
	
	void initialise()
	{
	}
	
	void beginFrame(uint frame)
	{
		mFrame = frame;
	}
	
	auto createDynamicBuffer(uint size)
	{
		assert(size % 256 == 0);
		assert(size <= MaxBufferSize);
		
		foreach(ushort i, ref ub; mDynamicBuffers[])
		{
			if(ub !is null)
				continue;
			
			auto buffer = alloc!Buffer(size, Buffering.triple, BufferFlag.persistent | BufferFlag.coherent | BufferFlag.write);
			ub = alloc!DynamicBuffer(buffer, size);
			return DynamicBufferHandle(i);
		}
		
		return DynamicBufferHandle.invalid;
	}
	
	auto getDynamicBuffer(DynamicBufferHandle handle)
	{
		assert(handle.id < MaxDynamicBuffers);
		
		return mDynamicBuffers[handle.id];
	}
	
	void destroyDynamicBuffer(DynamicBufferHandle handle)
	{
		assert(handle.id < MaxDynamicBuffers);
		
		free(mDynamicBuffers[handle.id].buffer);
		free(mDynamicBuffers[handle.id]);
		mDynamicBuffers[handle.id] = null;
	}
	
	void updateDynamicBuffer(DynamicBufferHandle handle, ubyte[] data, uint offset = 0)
	{
		assert(handle.id < MaxDynamicBuffers);
		
		auto db = mDynamicBuffers[handle.id];
		auto bufferOffset = mFrame * db.size + offset;
		db.buffer.upload(data, bufferOffset);
	}
}
