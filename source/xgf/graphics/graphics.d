module xgf.graphics.graphics;

import xgf.core.core;

import xgf.graphics.window;

import xgf.graphics.glcontext;
import xgf.graphics.rendertarget;
import xgf.graphics.rendertargetmanager;
import xgf.graphics.buffer;
import xgf.graphics.vertexbuffermanager;
import xgf.graphics.syncmanager;
import xgf.graphics.stagingbuffermanager;
import xgf.graphics.vertexdeclarationmanager;
import xgf.graphics.vertexdeclaration;
import xgf.graphics.vertexbuffer;
import xgf.graphics.indexbuffer;
import xgf.graphics.vertexarrayobject;
import xgf.graphics.shadermanager;
import xgf.graphics.shader;
import xgf.graphics.programpipeline;
import xgf.graphics.instanceindexbuffer;
import xgf.graphics.uniformbuffer;
import xgf.graphics.uniformbuffermanager;
import xgf.graphics.dynamicbuffer;
import xgf.graphics.dynamicbuffermanager;
import xgf.graphics.texture;
import xgf.graphics.texturemanager;
import xgf.graphics.sampler;
import xgf.graphics.samplermanager;
import xgf.graphics.state;
import xgf.graphics.statemanager;
import xgf.graphics.platform;

import xgf.util.alloc;
import xgf.util.log;

import xgf.math;

enum MaxUniformBufferLocations = 16;
enum MaxTextureLocations = 16;

@gc
struct Graphics
{
	private
	{
		Core* mCore;
// 		Window* mWindow;
		GLContext* mContext;
		RenderTargetManager* mRenderTargetMgr;
		VertexBufferManager* mVertexBufferMgr;
		StagingBufferManager* mStagingBufferMgr;
		SyncManager* mSyncMgr;
		VertexDeclarationManager* mVertexDeclMgr;
		ShaderManager* mShaderMgr;
		UniformBufferManager* mUniformBufferMgr;
		DynamicBufferManager* mDynamicBufferMgr;
		TextureManager* mTextureMgr;
		SamplerManager* mSamplerMgr;
		StateManager* mStateMgr;
		
		VertexArrayObject* mVao;
		ProgramPipeline* mProgramPipeline;
		
		uint mFrame;
		
		// Cache
		
		RenderTargetHandle mReadRenderTarget;
		RenderTargetHandle mWriteRenderTarget;
		
		VertexBufferHandle mVertexBufferHandle;
		IndexBufferHandle mIndexBufferHandle;
		InstanceIndexBufferHandle mInstanceIndexBufferHandle;
		
		VertexBuffer* mVertexBuffer;
		IndexBuffer* mIndexBuffer;
		InstanceIndexBuffer* mInstanceIndexBuffer;
		
		VertexShaderHandle mVertexShaderHandle;
		FragmentShaderHandle mFragmentShaderHandle;
		
		Shader* mVertexShader;
		Shader* mFragmentShader;
		
		UniformBufferHandle[MaxUniformBufferLocations] mUniformBufferHandles;
		UniformBuffer*[MaxUniformBufferLocations] mUniformBuffers;
		DynamicBufferHandle[MaxUniformBufferLocations] mDynamicUniformBufferHandles;
		DynamicBuffer*[MaxUniformBufferLocations] mDynamicUniformBuffers;
		uint[MaxUniformBufferLocations] mDynamicUniformBufferOffsets;
		
		TextureHandle[MaxTextureLocations] mTextureHandles;
		Texture*[MaxTextureLocations] mTextures;
		Type[MaxTextureLocations] mLastTextureTypes;
		SamplerHandle[MaxTextureLocations] mSamplerHandles;
		Sampler*[MaxTextureLocations] mSamplers;
		
		//
		BlendState mBlendState;
		DepthState mDepthState;
		CullState mCullState;
		ScissorState mScissorState;
		
		ivec4 mViewport;
	}
	
	this(Core* core, Platform* plat)
	{
		mCore = core;
// 		mWindow = win;
		mContext = alloc!GLContext();

		if(!mContext.initialize(plat))
		{
			Log.error("[Graphics]: Failed to initialize GLContext.");
			return;
		}
		if(!mContext.makeCurrent())
		{
			Log.error("[Graphics]: Failed to make GLContext current.");
			return;
		}
		if(!mContext.loadGL())
		{
			Log.error("[Graphics]: Failed to load GL functions.");
			return;
		}
		mContext.setupDebugCallback();

		mRenderTargetMgr = alloc!RenderTargetManager;
		mRenderTargetMgr.initialize();
		
		mVertexBufferMgr = alloc!VertexBufferManager;
		mVertexBufferMgr.initialize();
		
		mStagingBufferMgr = alloc!StagingBufferManager;
		mSyncMgr = alloc!SyncManager;
		mVertexDeclMgr = alloc!VertexDeclarationManager;
		
		mShaderMgr = alloc!ShaderManager;
		mUniformBufferMgr = alloc!UniformBufferManager;
		mDynamicBufferMgr = alloc!DynamicBufferManager;
		
		mTextureMgr = alloc!TextureManager;
		mSamplerMgr = alloc!SamplerManager;
		
		mVao = alloc!VertexArrayObject;
		mVao.initialize();
		
		mProgramPipeline = alloc!ProgramPipeline;
		mProgramPipeline.initialize();
		
		mStateMgr = alloc!StateManager;
		
// 		setBlendState(mBlendState, true);
	}
	
	~this()
	{
		free(mRenderTargetMgr);
		free(mVertexBufferMgr);
		free(mStagingBufferMgr);
		free(mSyncMgr);
		free(mVertexDeclMgr);
		free(mShaderMgr);
		free(mVao);
		free(mProgramPipeline);
	}
	
	void beginFrame()
	{
		mFrame = (mFrame + 1) % 3;
		mStagingBufferMgr.beginFrame(mFrame);
// 		mUniformBufferMgr.beginFrame(mFrame);
		mDynamicBufferMgr.beginFrame(mFrame);
		
		mSyncMgr.waitForSyncObject(mFrame);
	}
	
	void endFrame()
	{
// 		mSyncObjects[mFrame] = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
		mSyncMgr.setSyncPoint(mFrame);
	}
	
	void clearRenderTarget()(RenderTargetHandle handle, ClearFlag flags, in auto ref vec4 color, float depth = 1, int stencil = 0)
	{
		mRenderTargetMgr.clearRenderTarget(handle, flags, color, depth, stencil);
	}

	auto getVertexDeclaration()(Attribute[] attributes)
	{
		return mVertexDeclMgr.getVertexDeclarationHandle(attributes);
	}
	
	auto getVertexDeclaration(Vertex)()
	{
		return mVertexDeclMgr.getVertexDeclarationHandle!Vertex;
	}
	
	auto createVertexBuffer()(ubyte[] data, VertexDeclarationHandle vd)
	{
		auto vdPtr = mVertexDeclMgr.getVertexDeclaration(vd);
		auto vbHandle = mVertexBufferMgr.createVertexBuffer(cast(uint)data.length, vdPtr);
		auto vb = mVertexBufferMgr.getVertexBuffer(vbHandle);
		
		mStagingBufferMgr.upload(data, vb.view);
		
		return vbHandle;
	}
	
	auto createVertexBuffer(Vertex)(Vertex[] data)
	{
		auto vd = getVertexDeclaration!Vertex;
		
		return createVertexBuffer(cast(ubyte[])data, vd);
	}
	
	void destroyVertexBuffer()(VertexBufferHandle vb)
	{
		mVertexBufferMgr.destroyVertexBuffer(vb);
		if(vb == mVertexBufferHandle)
		{
			mVertexBufferHandle = VertexBufferHandle.invalid;
			mVertexBuffer = null;
		}
	}
	
	auto createIndexBuffer()(ubyte[] data, IndexType type)
	{
		auto handle = mVertexBufferMgr.createIndexBuffer(cast(uint)data.length, type);
		auto ib = mVertexBufferMgr.getIndexBuffer(handle);
		
		mStagingBufferMgr.upload(data, ib.view);
		
		return handle;
	}
	
	auto createIndexBuffer(Index)(Index[] data)
	{
		enum type = indexType!Index;
		
		return createIndexBuffer(cast(ubyte[])data, type);
	}
	
	void destroyIndexBuffer()(IndexBufferHandle ib)
	{
		mVertexBufferMgr.destroyIndexBuffer(ib);
		if(ib == mIndexBufferHandle)
		{
			mIndexBufferHandle = IndexBufferHandle.invalid;
			mIndexBuffer = null;
		}
	}
	
	auto createInstanceIndexBuffer(InstanceIndex)(InstanceIndex[] data)
	{
		static assert(is(InstanceIndex == uint));
		
		return createInstanceIndexBuffer(cast(ubyte[])data, InstanceIndexType.u32);
	}
	
	auto createInstanceIndexBuffer()(ubyte[] data, InstanceIndexType type)
	{
		auto iibHandle = mVertexBufferMgr.createInstanceIndexBuffer(cast(uint)data.length, type);
		auto iib = mVertexBufferMgr.getInstanceIndexBuffer(iibHandle);
		
		mStagingBufferMgr.upload(data, iib.view);
		
		return iibHandle;
	}
	
	auto createUniformBuffer()(uint size)
	{
		return mUniformBufferMgr.createUniformBuffer(size);
	}
	
	void destroyUniformBuffer()(UniformBufferHandle ub)
	{
		mUniformBufferMgr.destroyUniformBuffer(ub);
	}
	
	void updateUniformBuffer()(UniformBufferHandle ubHandle, ubyte[] data, uint offset = 0)
	{
		auto ub = mUniformBufferMgr.getUniformBuffer(ubHandle);
		
		mStagingBufferMgr.upload(data, BufferView(ub.buffer, ub.size, offset));
	}
	
	void bindUniformBuffer(UniformBufferHandle handle, uint location)
	{
		assert(location < 16);
		
		if(mUniformBufferHandles[location] != handle)
		{
			mUniformBufferHandles[location] = handle;
			auto ub = (handle != UniformBufferHandle.invalid) ? mUniformBufferMgr.getUniformBuffer(handle) : null;
			mUniformBuffers[location] = ub;
			mDynamicUniformBufferHandles[location] = DynamicBufferHandle.invalid;
			mDynamicUniformBuffers[location] = null;
			mProgramPipeline.bindUniformBuffer(ub, location);
		}
// 		glBindBufferRange(GL_UNIFORM_BUFFER, location, ub.buffer.handle, mFrame * ub.size, ub.size);
	}
	
	auto createDynamicBuffer(uint size)
	{		
		return mDynamicBufferMgr.createDynamicBuffer(size);
	}
	
	void destroyDynamicBuffer(DynamicBufferHandle handle)
	{
		mDynamicBufferMgr.destroyDynamicBuffer(handle);
	}
	
	void updateDynamicBuffer(DynamicBufferHandle handle, ubyte[] data, uint offset = 0)
	{
		mDynamicBufferMgr.updateDynamicBuffer(handle, data, offset);
	}
	
	void bindDynamicUniformBuffer(DynamicBufferHandle handle, uint location, uint size, uint offset = 0)
	{
		assert(location < 16);
		
		if(mDynamicUniformBufferHandles[location] != handle || mDynamicUniformBufferOffsets[location] != offset)
		{
			auto db = handle != DynamicBufferHandle.invalid ? mDynamicBufferMgr.getDynamicBuffer(handle) : null;
			mDynamicUniformBufferHandles[location] = handle;
			mDynamicUniformBuffers[location] = db;
			mDynamicUniformBufferOffsets[location] = offset;
			mUniformBufferHandles[location] = UniformBufferHandle.invalid;
			mUniformBuffers[location] = null;
			mProgramPipeline.bindDynamicUniformBuffer(db, location, offset);
		}
// 		final switch(binding) with(DynamicBufferBinding)
// 		{
// 			case uniform:
// 				if(mDynamicUniformBufferHandles[location] != handle)
// 				{
// 					auto db = handle != DynamicBufferHandle.invalid ? mDynamicBufferMgr.getDynamicBuffer(handle) : null;
// 					mDynamicUniformBufferHandles[location] = handle;
// 					mDynamicUniformBuffers[location] = db;
// 					mUniformBufferHandles[location] = UniformBufferHandle.invalid;
// 					mUniformBuffers[location] = null;
// 					mProgramPipeline.bindDynamicBuffer(db, location);
// 				}
// 				break;
// 			case vertex: assert(false);
// 		}
	}
	
	void setVertexBuffer()(VertexBufferHandle vb)
	{
		if(vb != mVertexBufferHandle)
		{
			mVertexBufferHandle = vb;
			mVertexBuffer = mVertexBufferMgr.getVertexBuffer(vb);
			mVao.setVertexBuffer(mVertexBuffer);
		}
	}
	
	void setIndexBuffer()(IndexBufferHandle ib)
	{
		if(ib != mIndexBufferHandle)
		{
			mIndexBufferHandle = ib;
			mIndexBuffer = mVertexBufferMgr.getIndexBuffer(ib);
			mVao.setIndexBuffer(mIndexBuffer);
		}
	}
	
	void setInstanceIndexBuffer()(InstanceIndexBufferHandle iib)
	{
		if(iib != mInstanceIndexBufferHandle)
		{
			mInstanceIndexBufferHandle = iib;
			mInstanceIndexBuffer = mVertexBufferMgr.getInstanceIndexBuffer(iib);
			mVao.setInstanceIndexBuffer(mInstanceIndexBuffer);
		}
	}
	
	void setVertexShader()(VertexShaderHandle vs)
	{
		if(vs != mVertexShaderHandle)
		{
			mVertexShaderHandle = vs;
			mVertexShader = mShaderMgr.getVertexShader(vs);
			mProgramPipeline.setVertexShader(mVertexShader);
		}
	}
	
	void setFragmentShader()(FragmentShaderHandle fs)
	{
		if(fs != mFragmentShaderHandle)
		{
			mFragmentShaderHandle = fs;
			mFragmentShader = mShaderMgr.getFragmentShader(fs);
			mProgramPipeline.setFragmentShader(mFragmentShader);
		}
	}
	
	auto createVertexShader()(in ubyte[] data)
	{
		return mShaderMgr.createVertexShader(data);
	}
	
	void destroyVertexShader()(VertexShaderHandle vs)
	{
		mShaderMgr.destroyVertexShader(vs);
	}
	
	auto createFragmentShader()(in ubyte[] data)
	{
		return mShaderMgr.createFragmentShader(data);
	}
	
	void destroyFragmentShader()(FragmentShaderHandle fs)
	{
		mShaderMgr.destroyFragmentShader(fs);
	}
	
	auto createTexture2D(ubyte[] data, uint width, uint height, Format fmt)
	{
		return mTextureMgr.createTexture2D(data, width, height, fmt);
	}
	
	void updateTexture2D(TextureHandle handle, ubyte[] data, uint width, uint height, uint x = 0, uint y = 0)
	{
		mTextureMgr.updateTexture2D(handle, data, width, height, x, y);
	}
	
	void destroyTexture(TextureHandle handle)
	{
		mTextureMgr.destroyTexture(handle);
	}
	
	auto getSampler(Filter minFilter, Filter magFilter, Wrap wrapS = Wrap.repeat, Wrap wrapT = Wrap.repeat, Wrap wrapR = Wrap.repeat)
	{
		return mSamplerMgr.getSampler(minFilter, magFilter, wrapS, wrapT, wrapR);
	}
	
	void bindTexture(TextureHandle handle, uint binding)
	{
		assert(binding < 16);
		if(mTextureHandles[binding] != handle)
		{
			mTextureHandles[binding] = handle;
			
			if(handle == TextureHandle.invalid)
			{
				mTextures[binding] = null;
				mTextureMgr.bindTexture(null, binding, mLastTextureTypes[binding]);
			}
			else
			{
				mTextures[binding] = mTextureMgr.getTexture(handle);
				mTextureMgr.bindTexture(mTextures[binding], binding);
				mLastTextureTypes[binding] = mTextures[binding].type;
			}
			
		}
	}
	
	void bindSampler(SamplerHandle handle, uint binding)
	{
		assert(binding < 16);
		if(handle != mSamplerHandles[binding])
		{
			if(handle == SamplerHandle.invalid)
			{
				mSamplers[binding] = null;
				mSamplerMgr.bindSampler(null, binding);
			}
			else
			{
				mSamplers[binding] = mSamplerMgr.getSampler(handle);
				mSamplerMgr.bindSampler(mSamplers[binding], binding);
			}
		}
	}
	
	void setBlendState(BlendState state)
	{
		if(mBlendState != state)
		{
			if(mBlendState.enabled != state.enabled)
				state.enabled ? mStateMgr.enableBlending() : mStateMgr.disableBlending();
			if(mBlendState.func != state.func)
				mStateMgr.setBlendFunction(state.func);
			if(mBlendState.equation != state.equation)
				mStateMgr.setBlendEquation(state.equation);
			
			mBlendState = state;
		}
	}
	
	void setDepthState(DepthState state)
	{
		if(mDepthState != state)
		{
			if(mDepthState.enabled != state.enabled)
				state.enabled ? mStateMgr.enableDepthTest() : mStateMgr.disableDepthTest();
			if(mDepthState.func != state.func)
				mStateMgr.setDepthFunction(state.func);
			
			mDepthState = state;
		}
	}
	
	void setCullState(CullState state)
	{
		if(mCullState != state)
		{
			if(mCullState.enabled != state.enabled)
				state.enabled ? mStateMgr.enableCulling() : mStateMgr.disableCulling();
			if(mCullState.mode != state.mode)
				mStateMgr.setCullMode(state.mode);
			if(mCullState.front != state.front)
				mStateMgr.setCullFrontFace(state.front);
			
			mCullState = state;
		}
	}
	
	void setScissor(ScissorState state)
	{
		if(mScissorState != state)
		{
			if(mScissorState.enabled != state.enabled)
				state.enabled ? mStateMgr.enableScissorTest() : mStateMgr.disableScissorTest();
			if(mScissorState.rect != state.rect)
				mStateMgr.setScissor(state.rect);
			
			mScissorState = state;
		}
	}
	
	void setViewport(ivec4 vp)
	{
		mViewport = vp;
		mStateMgr.setViewport(vp);
	}
	
	void draw()(PrimitiveType type, uint instances = 1, uint baseInstance = 0)
	{
// 		auto vb = getVertexBuffer(mVbHandle);
// 		auto vd = getVertexDeclaration(mVdHandle);
		
// 		glDrawArraysInstancedBaseInstance(type.glType, vb.baseVertex, vb.size / vd.size, instances, baseInstance);
// 		import std.stdio;
// 		writeln(*mVertexBuffer);
// 		writeln(mVertexBuffer.vertexDecl.attributes);
		mVao.draw(type, instances, baseInstance, mVertexBuffer);
	}
	
	void drawIndexed()(PrimitiveType type, uint instances = 1, uint baseInstance = 0)
	{
// 		import std.stdio;
// 		auto vb = getVertexBuffer(mVbHandle);
// 		writeln(vb);
// 		auto ib = getIndexBuffer(mIbHandle);
// 		writeln(ib);
		
// 		glDrawElementsInstancedBaseVertexBaseInstance(type.glType, ib.count, ib.type.glType, cast(void*)ib.offset, instances, vb.baseVertex, baseInstance);

		mVao.drawIndexed(type, instances, baseInstance, mVertexBuffer, mIndexBuffer);
	}

	void present()
	{
		mContext.swapBuffers();
	}
}

alias DefaultRenderTarget = xgf.graphics.rendertargetmanager.DefaultRenderTarget;
alias ClearFlag = xgf.graphics.rendertargetmanager.ClearFlag;
alias PrimitiveType = xgf.graphics.vertexarrayobject.PrimitiveType;
alias DynamicBufferBinding = xgf.graphics.dynamicbuffer.DynamicBufferBinding;
alias Format = xgf.graphics.texture.Format;
alias Filter = xgf.graphics.sampler.Filter;
alias Wrap = xgf.graphics.sampler.Wrap;
alias Attribute = xgf.graphics.vertexdeclaration.Attribute;
alias BlendState = xgf.graphics.state.BlendState;
alias DepthState = xgf.graphics.state.DepthState;
alias CullState = xgf.graphics.state.CullState;
alias ScissorState = xgf.graphics.state.ScissorState;

alias InstanceIndexBufferHandle = xgf.graphics.instanceindexbuffer.InstanceIndexBufferHandle;
alias VertexBufferHandle = xgf.graphics.vertexbuffer.VertexBufferHandle;
alias IndexBufferHandle = xgf.graphics.indexbuffer.IndexBufferHandle;
alias IndexType = xgf.graphics.indexbuffer.IndexType;
alias VertexDeclarationHandle = xgf.graphics.vertexdeclaration.VertexDeclarationHandle;
alias DynamicBufferHandle = xgf.graphics.dynamicbuffer.DynamicBufferHandle;
alias UniformBufferHandle = xgf.graphics.uniformbuffer.UniformBufferHandle;
alias RenderTargetHandle = xgf.graphics.rendertarget.RenderTargetHandle;
alias VertexShaderHandle = xgf.graphics.shader.VertexShaderHandle;
alias FragmentShaderHandle = xgf.graphics.shader.FragmentShaderHandle;
alias TextureHandle = xgf.graphics.texture.TextureHandle;
alias SamplerHandle = xgf.graphics.sampler.SamplerHandle;
