module xgf.graphics.egl.platform;

version(linux)
{
	import core.stdc.stdint : uintptr_t;
	alias EGLNativeDisplayType = void*;
	alias EGLNativePixmapType = uintptr_t;
	alias EGLNativeWindowType = uintptr_t;
}
else static assert(false, "Platform not implemented yet.");

alias EGLint = int;