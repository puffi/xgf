module xgf.graphics.rendertargetmanager;

import xgf.graphics.rendertarget;
import xgf.graphics.config;

import xgf.util.alloc;

import xgf.graphics.glad.gl.all;

import xgf.math;

enum DefaultRenderTarget = RenderTargetHandle(DefaultRenderTargetIndex);

struct RenderTargetManager
{
	private
	{
		import xgf.util.array;
		StaticArray!(RenderTarget*, MaxRenderTargets) mRenderTargets;
	}
	
	~this()
	{
		free(mRenderTargets[DefaultRenderTargetIndex]);
		mRenderTargets[DefaultRenderTargetIndex] = null;
		
		uint count = 0;
		foreach(rt; mRenderTargets[])
		{
			if(rt !is null)
				count++;
		}
		
		if(count > 0)
		{
			import std.stdio : writeln;
			writeln(count, " RenderTargets were leaked.");
		}
	}
	
	void initialize()
	{
		auto rt = alloc!RenderTarget();
		rt.handle = 0;
		mRenderTargets[DefaultRenderTargetIndex] = rt;
	}
	
	auto getRenderTarget(RenderTargetHandle handle)
	{
		assert(handle != RenderTargetHandle.invalid);
		
		return mRenderTargets[handle.id];
	}
	
	void clearRenderTarget()(RenderTargetHandle handle, ClearFlag clear, in auto ref vec4 color, float depth = 1, int stencil = 0)
	{
		auto rt = getRenderTarget(handle);
		
		if(clear & ClearFlag.color)
			glClearNamedFramebufferfv(rt.handle, GL_COLOR, 0, color.ptr);
		if(clear & ClearFlag.depth && clear & ClearFlag.stencil)
			glClearNamedFramebufferfi(rt.handle, GL_DEPTH_STENCIL, 0, depth, stencil);
		else
		{
			if(clear & ClearFlag.depth)
				glClearNamedFramebufferfv(rt.handle, GL_DEPTH, 0, &depth);
			if(clear & ClearFlag.stencil)
				glClearNamedFramebufferiv(rt.handle, GL_STENCIL, 0, &stencil);
		}
	}
}

enum ClearFlag
{
	color = 1 << 0,
	depth = 1 << 1,
	stencil = 1 << 2
	
}
