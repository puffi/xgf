module xgf.physics.collision.intersection;

import xgf.physics.collision.aabb;
import xgf.physics.collision.collisioninfo;

import xgf.math;

bool intersects(Aabb left, Aabb right)
{
	auto diff = left.center - right.center;
	diff = abs(diff);
	
	auto exts = left.extents + right.extents;
	
	
	return diff.x < exts.x && diff.y < exts.y && diff.z < exts.z;
}

bool checkCollision(Aabb left, Aabb right, CollisionInfo* ci)
{
	assert(ci);
	
	auto diff = left.center - right.center;
	auto exts = left.extents + left.extents;
	
	import std.math : abs;
	auto x = exts.x - abs(diff.x);
	auto y = exts.y - abs(diff.y);
	auto z = exts.z - abs(diff.z);
	if(x <= 0 || y <= 0 || z <= 0)
		return false;
	
	import std.math : sgn;
	if(x < y && x < z)
	{
		ci.distance = x;
		ci.normal = vec3(diff.x.sgn, 0, 0);
	}
	else if(y < x && y < z)
	{
		ci.distance = y;
		ci.normal = vec3(0, diff.y.sgn, 0);
	}
	else
	{
		ci.distance = z;
		ci.normal = vec3(0, 0, diff.z.sgn);
	}
	return true;
}
