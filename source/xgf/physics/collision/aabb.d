module xgf.physics.collision.aabb;

import xgf.math;

struct Aabb
{
	vec3 center;
	vec3 extents;
	
	@property auto min() { return center - extents; }
	@property auto max() { return center + extents; }
}
