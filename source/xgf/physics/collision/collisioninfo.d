module xgf.physics.collision.collisioninfo;

import xgf.math;

struct CollisionInfo
{
	vec3 normal;
	float distance = 0;
}
