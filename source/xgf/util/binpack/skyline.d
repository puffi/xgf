module xgf.util.binpack.skyline;

import xgf.util.binpack.rect;
import xgf.util.binpack.guillotine;

import xgf.util;

struct Skyline
{
	private
	{
		Guillotine mWasteMap;
		DynamicArray!SkylineNode mSkyline;
		int mWidth;
		int mHeight;

		ulong mUsedSurface;
	}

	this(int width, int height)
	{
		clear(width, height);
	}

	void clear(int width, int height)
	{
		mWidth = width;
		mHeight = height;
		clear();
	}

	void clear()
	{
		mUsedSurface = 0;
		mSkyline.clear();
		mSkyline.pushBack(SkylineNode(0, 0, mWidth));

		mWasteMap.clear(mWidth, mHeight);
		mWasteMap.clearFreeRects();
	}

	/// Inserts a single rectangle into the bin, possibly rotated.
	Rect insert(int width, int height, LevelChoiceHeuristic method)
	{
		// First try to pack this rectangle into the waste map, if it fits.
		Rect node = mWasteMap.insert(width, height, true, FreeRectChoiceHeuristic.RectBestShortSideFit, 
			GuillotineSplitHeuristic.SplitMaximizeArea);

		if(node.height != 0)
		{
			mUsedSurface += width * height;

			return node;
		}
		
		final switch(method) with(LevelChoiceHeuristic)
		{
			case LevelBottomLeft: return insertBottomLeft(width, height);
			case LevelMinWasteFit: return insertMinWaste(width, height);
		}
	}

	private
	{
		Rect insertBottomLeft(int width, int height)
		{
			int bestHeight;
			int bestWidth;
			int bestIndex;
			Rect newNode = findPositionForNewNodeBottomLeft(width, height, bestHeight, bestWidth, bestIndex);

			if(bestIndex != -1)
			{
				// Perform the actual packing.
				addSkylineLevel(bestIndex, newNode);

				mUsedSurface += width * height;
			}
			else
				newNode = Rect.init;

			return newNode;
		}

		Rect insertMinWaste(int width, int height)
		{
			int bestHeight;
			int bestWastedArea;
			int bestIndex;
			Rect newNode = findPositionForNewNodeMinWaste(width, height, bestHeight, bestWastedArea, bestIndex);

			if(bestIndex != -1)
			{
				// Perform the actual packing.
				addSkylineLevel(bestIndex, newNode);

				mUsedSurface += width * height;
			}	
			else
				newNode = Rect.init;

			return newNode;
		}

		Rect findPositionForNewNodeMinWaste(int width, int height, ref int bestHeight, ref int bestWastedArea, ref int bestIndex)
		{
			bestHeight = int.max;
			bestWastedArea = int.max;
			bestIndex = -1;
			Rect newNode;
			for(size_t i = 0; i < mSkyline.length; ++i)
			{
				int y;
				int wastedArea;

				if(rectangleFits(cast(int)i, width, height, y, wastedArea))
				{
					if(wastedArea < bestWastedArea || (wastedArea == bestWastedArea && y + height < bestHeight))
					{
						bestHeight = y + height;
						bestWastedArea = wastedArea;
						bestIndex = cast(int)i;
						newNode.x = mSkyline[i].x;
						newNode.y = y;
						newNode.width = width;
						newNode.height = height;
					}
				}
				// if(rectangleFits(i, height, width, y, wastedArea))
				// {
				// 	if(wastedArea < bestWastedArea || (wastedArea == bestWastedArea && y + width < bestHeight))
				// 	{
				// 		bestHeight = y + width;
				// 		bestWastedArea = wastedArea;
				// 		bestIndex = i;
				// 		newNode.x = mSkyline[i].x;
				// 		newNode.y = y;
				// 		newNode.width = height;
				// 		newNode.height = width;
				// 		debug_assert(disjointRects.Disjoint(newNode));
				// 	}
				// }
			}

			return newNode;
		}

		Rect findPositionForNewNodeBottomLeft(int width, int height, ref int bestHeight, ref int bestWidth, ref int bestIndex)
		{
			bestHeight = int.max;
			bestIndex = -1;
			// Used to break ties if there are nodes at the same level. Then pick the narrowest one.
			bestWidth = int.max;
			Rect newNode;
			for(size_t i = 0; i < mSkyline.length; ++i)
			{
				int y;
				if(rectangleFits(cast(int)i, width, height, y))
				{
					if(y + height < bestHeight || (y + height == bestHeight && mSkyline[i].width < bestWidth))
					{
						bestHeight = y + height;
						bestIndex = cast(int)i;
						bestWidth = mSkyline[i].width;
						newNode.x = mSkyline[i].x;
						newNode.y = y;
						newNode.width = width;
						newNode.height = height;
					}
				}
				// if (RectangleFits(i, height, width, y))
				// {
				// 	if (y + width < bestHeight || (y + width == bestHeight && mSkyline[i].width < bestWidth))
				// 	{
				// 		bestHeight = y + width;
				// 		bestIndex = i;
				// 		bestWidth = mSkyline[i].width;
				// 		newNode.x = mSkyline[i].x;
				// 		newNode.y = y;
				// 		newNode.width = height;
				// 		newNode.height = width;
				// 		debug_assert(disjointRects.Disjoint(newNode));
				// 	}
				// }
			}

			return newNode;
		}

		bool rectangleFits(int skylineNodeIndex, int width, int height, ref int y)
		{
			import std.algorithm : min, max;
			int x = mSkyline[skylineNodeIndex].x;
			if (x + width > mWidth)
				return false;
			int widthLeft = width;
			int i = skylineNodeIndex;
			y = mSkyline[skylineNodeIndex].y;
			while(widthLeft > 0)
			{
				y = max(y, mSkyline[i].y);
				if(y + height > mHeight)
					return false;
				widthLeft -= mSkyline[i].width;
				++i;
				assert(i < cast(int)mSkyline.length || widthLeft <= 0);
			}
			return true;
		}

		bool rectangleFits(int skylineNodeIndex, int width, int height, ref int y, ref int wastedArea)
		{
			bool fits = rectangleFits(skylineNodeIndex, width, height, y);
			if (fits)
				wastedArea = computeWastedArea(skylineNodeIndex, width, height, y);
			
			return fits;
		}

		int computeWastedArea(int skylineNodeIndex, int width, int height, int y)
		{
			import std.algorithm : min, max;
			int wastedArea = 0;
			const int rectLeft = mSkyline[skylineNodeIndex].x;
			const int rectRight = rectLeft + width;
			for(; skylineNodeIndex < cast(int)mSkyline.length && mSkyline[skylineNodeIndex].x < rectRight; ++skylineNodeIndex)
			{
				if(mSkyline[skylineNodeIndex].x >= rectRight || mSkyline[skylineNodeIndex].x + mSkyline[skylineNodeIndex].width <= rectLeft)
					break;

				int leftSide = mSkyline[skylineNodeIndex].x;
				int rightSide = min(rectRight, leftSide + mSkyline[skylineNodeIndex].width);
				assert(y >= mSkyline[skylineNodeIndex].y);
				wastedArea += (rightSide - leftSide) * (y - mSkyline[skylineNodeIndex].y);
			}
			return wastedArea;
		}

		void addWasteMapArea(int skylineNodeIndex, int width, int height, int y)
		{
			import std.algorithm : min, max;
			// int wastedArea = 0; // unused
			const int rectLeft = mSkyline[skylineNodeIndex].x;
			const int rectRight = rectLeft + width;
			for(; skylineNodeIndex < cast(int)mSkyline.length && mSkyline[skylineNodeIndex].x < rectRight; ++skylineNodeIndex)
			{
				if(mSkyline[skylineNodeIndex].x >= rectRight || mSkyline[skylineNodeIndex].x + mSkyline[skylineNodeIndex].width <= rectLeft)
					break;

				int leftSide = mSkyline[skylineNodeIndex].x;
				int rightSide = min(rectRight, leftSide + mSkyline[skylineNodeIndex].width);
				assert(y >= mSkyline[skylineNodeIndex].y);

				Rect waste;
				waste.x = leftSide;
				waste.y = mSkyline[skylineNodeIndex].y;
				waste.width = rightSide - leftSide;
				waste.height = y - mSkyline[skylineNodeIndex].y;

				mWasteMap.addFreeRect(waste);
			}
		}

		void addSkylineLevel(int skylineNodeIndex, Rect rect)
		{
			// First track all wasted areas and mark them into the waste map if we're using one.
			addWasteMapArea(skylineNodeIndex, rect.width, rect.height, rect.y);

			SkylineNode newNode;
			newNode.x = rect.x;
			newNode.y = rect.y + rect.height;
			newNode.width = rect.width;
			mSkyline.insert(skylineNodeIndex, newNode);

			assert(newNode.x + newNode.width <= mWidth);
			assert(newNode.y <= mHeight);

			for(size_t i = skylineNodeIndex+1; i < mSkyline.length; ++i)
			{
				assert(mSkyline[i-1].x <= mSkyline[i].x);

				if(mSkyline[i].x < mSkyline[i-1].x + mSkyline[i-1].width)
				{
					int shrink = mSkyline[i-1].x + mSkyline[i-1].width - mSkyline[i].x;

					mSkyline[i].x += shrink;
					mSkyline[i].width -= shrink;

					if (mSkyline[i].width <= 0)
					{
						mSkyline.remove(cast(uint)i);
						--i;
					}
					else
						break;
				}
				else
					break;
			}
			mergeSkylines();
		}

		/// Merges all skyline nodes that are at the same level.
		void mergeSkylines()
		{
			for(size_t i = 0; i < mSkyline.length-1; ++i)
			if (mSkyline[i].y == mSkyline[i+1].y)
			{
				mSkyline[i].width += mSkyline[i+1].width;
				mSkyline.remove(cast(uint)i+1);
				--i;
			}
		}
	}
}

enum LevelChoiceHeuristic
{
	LevelBottomLeft,
	LevelMinWasteFit
}

private
{
	/// Represents a single level (a horizontal line) of the skyline/horizon/envelope.
	struct SkylineNode
	{
		/// The starting x-coordinate (leftmost).
		int x;

		/// The y-coordinate of the skyline level line.
		int y;

		/// The line width. The ending coordinate (inclusive) will be x+width-1.
		int width;
	}
}
