module xgf.util.binpack.guillotine;

import xgf.util.binpack.rect;

import xgf.util;

struct Guillotine
{
	private
	{
		DynamicArray!Rect mUsedRects;
		DynamicArray!Rect mFreeRects;
		int mWidth;
		int mHeight;
	}

	this(int w, int h)
	{
		clear(w, h);
	}

	void clear(int w, int h)
	{
		mWidth = w;
		mHeight = h;
		clear();
	}

	void clear()
	{
		mUsedRects.clear();
		mFreeRects.clear();
		Rect r = Rect(0, 0, mWidth, mHeight);
		mFreeRects.pushBack(r);
	}

	Rect insert(int width, int height, bool merge, FreeRectChoiceHeuristic rectChoice, GuillotineSplitHeuristic splitMethod)
	{
		// Find where to put the new rectangle.
		int freeNodeIndex = 0;
		Rect newRect = findPositionForNewNode(width, height, rectChoice, freeNodeIndex);

		// Abort if we didn't have enough space in the bin.
		if(newRect.height == 0)
			return newRect;

		// Remove the space that was just consumed by the new rectangle.
		splitFreeRectByHeuristic(mFreeRects[freeNodeIndex], newRect, splitMethod);
		mFreeRects.remove(freeNodeIndex);

		// Perform a Rectangle Merge step if desired.
		if(merge)
			mergeFreeList();

		// Remember the new used rectangle.
		mUsedRects.pushBack(newRect);

		return newRect;
	}

	@property auto occupancy()
	{
		ulong usedSurfaceArea;
		foreach(rect; usedRects)
			usedSurfaceArea += rect.width * rect.height;
		return usedSurfaceArea / cast(float)(mWidth * mHeight);
	}

	@property auto freeRects() { return mFreeRects[]; }
	@property auto usedRects() { return mUsedRects[]; }

	private
	{
		/// Goes through the list of free rectangles and finds the best one to place a rectangle of given size into.
		/// Running time is Theta(|mFreeRects|).
		/// @param nodeIndex [out] The index of the free rectangle in the mFreeRects array into which the new
		///		rect was placed.
		/// @return A Rect structure that represents the placement of the new rect into the best free rectangle.
		Rect findPositionForNewNode(int width, int height, FreeRectChoiceHeuristic rectChoice, ref int nodeIndex)
		{
			Rect bestNode;

			int bestScore = int.max;

			/// Try each free rectangle to find the best one for placement.
			foreach(i, rect; freeRects)
			{
				// If this is a perfect fit upright, choose it immediately.
				if(width == rect.width && height == rect.height)
				{
					bestNode.x = rect.x;
					bestNode.y = rect.y;
					bestNode.width = width;
					bestNode.height = height;
					bestScore = int.min;
					nodeIndex = cast(int)i;
					break;
				}
				// // If this is a perfect fit sideways, choose it.
				// else if(height == rect.width && width == rect.height)
				// {
				// 	bestNode.x = rect.x;
				// 	bestNode.y = rect.y;
				// 	bestNode.width = height;
				// 	bestNode.height = width;
				// 	bestScore = int.min;
				// 	nodeIndex = cast(int)i;
				// 	break;
				// }
				// Does the rectangle fit upright?
				else if(width <= rect.width && height <= rect.height)
				{
					int score = scoreByHeuristic(width, height, rect, rectChoice);

					if(score < bestScore)
					{
						bestNode.x = rect.x;
						bestNode.y = rect.y;
						bestNode.width = width;
						bestNode.height = height;
						bestScore = score;
						nodeIndex = cast(int)i;
					}
				}
				// // Does the rectangle fit sideways?
				// else if(height <= rect.width && width <= rect.height)
				// {
				// 	int score = scoreByHeuristic(height, width, rect, rectChoice);

				// 	if(score < bestScore)
				// 	{
				// 		bestNode.x = rect.x;
				// 		bestNode.y = rect.y;
				// 		bestNode.width = height;
				// 		bestNode.height = width;
				// 		bestScore = score;
				// 		nodeIndex = cast(int)i;
				// 	}
				// }
			}
			return bestNode;
		}

		/// Returns the heuristic score value for placing a rectangle of size width*height into freeRect. Does not try to rotate.
		static int scoreByHeuristic(int width, int height, Rect freeRect, FreeRectChoiceHeuristic rectChoice)
		{
			final switch(rectChoice) with(FreeRectChoiceHeuristic)
			{
				case RectBestAreaFit: return scoreBestAreaFit(width, height, freeRect);
				case RectBestShortSideFit: return scoreBestShortSideFit(width, height, freeRect);
				case RectBestLongSideFit: return scoreBestLongSideFit(width, height, freeRect);
				case RectWorstAreaFit: return scoreWorstAreaFit(width, height, freeRect);
				case RectWorstShortSideFit: return scoreWorstShortSideFit(width, height, freeRect);
				case RectWorstLongSideFit: return scoreWorstLongSideFit(width, height, freeRect);
			}
		}

		static int scoreBestAreaFit(int width, int height, Rect freeRect)
		{
			return freeRect.width * freeRect.height - width * height;
		}

		static int scoreBestShortSideFit(int width, int height, Rect freeRect)
		{
			import std.math : abs;
			import std.algorithm : min, max;
			int leftoverHoriz = abs(freeRect.width - width);
			int leftoverVert = abs(freeRect.height - height);
			int leftover = min(leftoverHoriz, leftoverVert);
			return leftover;
		}

		static int scoreBestLongSideFit(int width, int height, Rect freeRect)
		{
			import std.math : abs;
			import std.algorithm : min, max;
			int leftoverHoriz = abs(freeRect.width - width);
			int leftoverVert = abs(freeRect.height - height);
			int leftover = max(leftoverHoriz, leftoverVert);
			return leftover;
		}

		static int scoreWorstAreaFit(int width, int height, Rect freeRect)
		{
			return -scoreBestAreaFit(width, height, freeRect);
		}

		static int scoreWorstShortSideFit(int width, int height, Rect freeRect)
		{
			return -scoreBestShortSideFit(width, height, freeRect);
		}

		static int scoreWorstLongSideFit(int width, int height, Rect freeRect)
		{
			return -scoreBestLongSideFit(width, height, freeRect);
		}

		void splitFreeRectByHeuristic(Rect freeRect, Rect placedRect, GuillotineSplitHeuristic method)
		{
			// Compute the lengths of the leftover area.
			const int w = freeRect.width - placedRect.width;
			const int h = freeRect.height - placedRect.height;

			// Placing placedRect into freeRect results in an L-shaped free area, which must be split into
			// two disjoint rectangles. This can be achieved with by splitting the L-shape using a single line.
			// We have two choices: horizontal or vertical.	

			// Use the given heuristic to decide which choice to make.

			bool splitHorizontal;
			final switch(method) with(GuillotineSplitHeuristic)
			{
				case SplitShorterLeftoverAxis:
					// Split along the shorter leftover axis.
					splitHorizontal = (w <= h);
					break;
				case SplitLongerLeftoverAxis:
					// Split along the longer leftover axis.
					splitHorizontal = (w > h);
					break;
				case SplitMinimizeArea:
					// Maximize the larger area == minimize the smaller area.
					// Tries to make the single bigger rectangle.
					splitHorizontal = (placedRect.width * h > w * placedRect.height);
					break;
				case SplitMaximizeArea:
					// Maximize the smaller area == minimize the larger area.
					// Tries to make the rectangles more even-sized.
					splitHorizontal = (placedRect.width * h <= w * placedRect.height);
					break;
				case SplitShorterAxis:
					// Split along the shorter total axis.
					splitHorizontal = (freeRect.width <= freeRect.height);
					break;
				case SplitLongerAxis:
					// Split along the longer total axis.
					splitHorizontal = (freeRect.width > freeRect.height);
					break;
			}

			// Perform the actual split.
			splitFreeRectAlongAxis(freeRect, placedRect, splitHorizontal);
		}

		/// This function will add the two generated rectangles into the mFreeRects array. The caller is expected to
		/// remove the original rectangle from the mFreeRects array after that.
		void splitFreeRectAlongAxis(Rect freeRect, Rect placedRect, bool splitHorizontal)
		{
			// Form the two new rectangles.
			Rect bottom;
			bottom.x = freeRect.x;
			bottom.y = freeRect.y + placedRect.height;
			bottom.height = freeRect.height - placedRect.height;

			Rect right;
			right.x = freeRect.x + placedRect.width;
			right.y = freeRect.y;
			right.width = freeRect.width - placedRect.width;

			if(splitHorizontal)
			{
				bottom.width = freeRect.width;
				right.height = placedRect.height;
			}
			else // Split vertically
			{
				bottom.width = placedRect.width;
				right.height = freeRect.height;
			}

			// Add the new rectangles into the free rectangle pool if they weren't degenerate.
			if(bottom.width > 0 && bottom.height > 0)
				mFreeRects.pushBack(bottom);
			if(right.width > 0 && right.height > 0)
				mFreeRects.pushBack(right);
		}

		void mergeFreeList()
		{
			// Do a Theta(n^2) loop to see if any pair of free rectangles could me merged into one.
			// Note that we miss any opportunities to merge three rectangles into one. (should call this function again to detect that)
			for(size_t i = 0; i < mFreeRects.length; ++i)
			{
				for(size_t j = i+1; j < mFreeRects.length; ++j)
				{
					if(mFreeRects[i].width == mFreeRects[j].width && mFreeRects[i].x == mFreeRects[j].x)
					{
						if(mFreeRects[i].y == mFreeRects[j].y + mFreeRects[j].height)
						{
							mFreeRects[i].y -= mFreeRects[j].height;
							mFreeRects[i].height += mFreeRects[j].height;
							mFreeRects.remove(cast(uint)j);
							--j;
						}
						else if(mFreeRects[i].y + mFreeRects[i].height == mFreeRects[j].y)
						{
							mFreeRects[i].height += mFreeRects[j].height;
							mFreeRects.remove(cast(uint)j);
							--j;
						}
					}
					else if(mFreeRects[i].height == mFreeRects[j].height && mFreeRects[i].y == mFreeRects[j].y)
					{
						if(mFreeRects[i].x == mFreeRects[j].x + mFreeRects[j].width)
						{
							mFreeRects[i].x -= mFreeRects[j].width;
							mFreeRects[i].width += mFreeRects[j].width;
							mFreeRects.remove(cast(uint)j);
							--j;
						}
						else if(mFreeRects[i].x + mFreeRects[i].width == mFreeRects[j].x)
						{
							mFreeRects[i].width += mFreeRects[j].width;
							mFreeRects.remove(cast(uint)j);
							--j;
						}
					}
				}
			}
		}
	}

	package
	{
		void clearFreeRects()
		{
			mFreeRects.clear();
		}

		void addFreeRect(Rect rect)
		{
			mFreeRects.pushBack(rect);
		}
	}
}

/// Specifies the different choice heuristics that can be used when deciding which of the free subrectangles
/// to place the to-be-packed rectangle into.
enum FreeRectChoiceHeuristic
{
	RectBestAreaFit, ///< -BAF
	RectBestShortSideFit, ///< -BSSF
	RectBestLongSideFit, ///< -BLSF
	RectWorstAreaFit, ///< -WAF
	RectWorstShortSideFit, ///< -WSSF
	RectWorstLongSideFit ///< -WLSF
}

/// Specifies the different choice heuristics that can be used when the packer needs to decide whether to
/// subdivide the remaining free space in horizontal or vertical direction.
enum GuillotineSplitHeuristic
{
	SplitShorterLeftoverAxis, ///< -SLAS
	SplitLongerLeftoverAxis, ///< -LLAS
	SplitMinimizeArea, ///< -MINAS, Try to make a single big rectangle at the expense of making the other small.
	SplitMaximizeArea, ///< -MAXAS, Try to make both remaining rectangles as even-sized as possible.
	SplitShorterAxis, ///< -SAS
	SplitLongerAxis ///< -LAS
}
