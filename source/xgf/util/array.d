module xgf.util.array;

import xgf.util.alloc;
import std.typecons : Unqual;

struct Array(T)
{
	private
	{
		T[] mPayload;
	}
	
	this(size_t size)
	{
		mPayload = allocArray!T(size);
	}
	
	this(this)
	{
		auto pl = mPayload;
		mPayload = allocArray!T(pl.length);
		mPayload[0..$] = pl[0..$];
// 		free(pl);
	}
	
	~this()
	{
		import xgf.util.alloc;
		free(mPayload);
	}
	
	auto opIndex()
	{
		return mPayload;
	}
	
	auto opIndex() const
	{
		return mPayload;
	}
	
	auto ref opIndex(size_t idx)
	{
		return mPayload[idx];
	}
	
	auto opIndex(size_t idx) const
	{
		return mPayload[idx];
	}
	
	auto opIndexAssign()(auto ref T v, size_t idx)
	{
		mPayload[idx] = v;
	}
	
	@property auto size() const { return mPayload.length; }
	@property auto ptr() const { return mPayload.ptr; }
}

struct StaticArray(T, size_t count)
{
	private
	{
		T[count] mPayload;
	}
	
	auto opIndex()
	{
		return mPayload[];
	}
	
	auto opIndex() const
	{
		return mPayload[];
	}
	
	auto opIndex(size_t idx)
	{
		return mPayload[idx];
	}
	
	auto opIndex(size_t idx) const
	{
		return mPayload[idx];
	}
	
	auto opIndexAssign()(auto ref T v, size_t idx)
	{
		mPayload[idx] = v;
	}
	
	@property auto size() const { return mPayload.length; }
}

struct DynamicArray(T, bool gc = false)
{
	private
	{
		T* mPayload;
		// pragma(msg, typeof(mPayload).stringof);
		size_t mSize;
		size_t mCapacity;
	}
	
	void pushBack()(in auto ref Unqual!T val)
	{
		import std.typecons : Unqual;
		if(mSize == mCapacity)
			reserve(4 + mCapacity * 2);
		
		mSize += 1;
		cast(Unqual!T)mPayload[mSize-1] = cast(Unqual!T)val;
	}
	
	void pushBack(in Unqual!T[] vals)
	{
		foreach(val; vals)
			pushBack(val);
	}
	
	auto popBack()
	{
		assert(mSize > 0);
		mSize -= 1;
	}
	
	auto ref back()
	{
		assert(mSize > 0);
		return mPayload[mSize-1];
	}
	
	auto ref front()
	{
		assert(mSize > 0);
		return mPayload[0];
	}
	
	void insert(uint idx, in Unqual!T val)
	{
		assert(idx <= mSize);
		pushBack(val);
		if(idx == mSize-1)
			return;
		
		import std.algorithm.mutation : copy;
		import std.range : retro;
		import std.typecons : Unqual;
		static if(is(Unqual!T == char))
			copy((cast(ubyte[])mPayload[idx..mSize-1]).retro, (cast(ubyte[])mPayload[idx+1..mSize]).retro);
		else
			copy((cast(Unqual!T[])mPayload[idx..mSize-1]).retro, (cast(Unqual!T[])mPayload[idx+1..mSize]).retro);
		cast(Unqual!T)mPayload[idx] = cast(Unqual!T)val;
	}
	
	void insert(uint idx, in Unqual!T[] val)
	{
		assert(idx <= mSize);
		pushBack(val);
		if(idx == mSize-val.length)
			return;
		import std.algorithm.mutation : copy;
		import std.range : retro;
		import std.typecons : Unqual;
		static if(is(Unqual!T == char))
			copy((cast(ubyte[])mPayload[idx..mSize-val.length]).retro, (cast(ubyte[])mPayload[idx+val.length..mSize]).retro);
		else
			copy((cast(Unqual!T[])mPayload[idx..mSize-val.length]).retro, (cast(Unqual!T[])mPayload[idx+val.length..mSize]).retro);
		cast(Unqual!T[])mPayload[idx..idx+val.length] = cast(Unqual!T[])val[];
	}

	void remove(U...)(U idx)
	{
// 		assert(idx < mSize);
		import std.algorithm : remove, SwapStrategy;
		import std.typecons : Unqual;
		static if(is(Unqual!T == char))
			auto r = (cast(ubyte[])mPayload[0..mSize]).remove!(SwapStrategy.stable)(idx);
		else
			auto r = mPayload[0..mSize].remove!(SwapStrategy.stable)(idx);
		mSize -= mSize - r.length;
	}
	
	void reserve(size_t size)
	{
		import xgf.util.alloc;
		import std.typecons : Unqual;
		if(mCapacity >= size)
			return;
		
		static if(gc)
			auto arr = allocGCAwareArray!T(size);
		else
			auto arr = allocArray!T(size);
		cast(Unqual!T[])arr[0..mSize] = cast(Unqual!T[])mPayload[0..mSize];
		
		free(mPayload[0..mSize]);
		
		mPayload = arr.ptr;
		mCapacity = arr.length;
	}
	
	void resize(size_t size)
	{
		if(size < mSize)
		{
			static if(is(T == struct))
				foreach(obj; mPayload[size..mSize])
				{
					destroy(obj);
				}
			mSize = size;
			return;
		}
			
		reserve(size);
		
		import std.typecons : Unqual;
		foreach(ref obj; mPayload[mSize..size])
			cast(Unqual!T)obj = T.init;
		mSize = size;
	}
	
	@property auto empty() { return mSize == 0; }
	@property auto length() { return mSize; }
	@property void length(size_t len) { resize(len); }
	@property auto ptr() { return mPayload; }
	
	void clear()
	{
		mSize = 0;
	}
	
	this(this)
	{
		import xgf.util.alloc;
		import std.typecons : Unqual;
		auto arr = allocArray!(T)(mCapacity);
		cast(Unqual!T[])arr[0..mSize] = cast(Unqual!T[])mPayload[0..mSize];
		mPayload = arr.ptr;
	}
	
	~this()
	{
		import xgf.util.alloc;
		free(mPayload[0..mSize]);
	}
	
	@property auto opDollar() { return mSize; }
	
	auto ref opIndex()
	{
		return mPayload[0..mSize];
	}
	
	auto ref opIndex(size_t idx)
	{
		assert(idx < mSize);
		return mPayload[idx];
	}
	
	auto opIndex()(auto ref T val, size_t idx)
	{
		assert(idx < mSize);
		mPayload[idx] = val;
	}
	
	auto ref opSlice(size_t start, size_t end)
	{
		assert(start < mSize);
		assert(end <= mSize);
		assert(start <= end);
		return mPayload[start..end];
	}
}

private
{
// 	void copy(S, T)(S source, T target)
// 	{
// 		import std.range : front, popFront;
// 		foreach(v; source)
// 		{
// 			target.front = v;
// 			target.popFront();
// 		}
// 	}
}
