module xgf.util.timer;

class Timer
{
	import xgf.util.time;
	
	private
	{
		Time currentTime;
	}
	
	this()
	{
		import core.time : MonoTime;
		currentTime = Time(MonoTime.currTime.ticks, MonoTime.ticksPerSecond);
	}
	
	
	Time get()
	{
		import core.time : MonoTime;
		Time ret = Time(MonoTime.currTime.ticks, currentTime.frequency) - currentTime;
		return ret;
	}
	
	Time restart()
	{
		import core.time : MonoTime;
		Time now = Time(MonoTime.currTime.ticks, currentTime.frequency);
		Time ret = now - currentTime;
		currentTime = now;
		return ret;
	}
}
