module xgf.util.pool;

import xgf.util;

struct PoolAllocator(T, size_t size)
{
// 	private Pool!(T, size)*[] mPools;
	private DynamicArray!(Pool!(T, size)*) mPools;
	
	~this()
	{
		clear();
	}
	
	T* allocate()
	{
		foreach(p; mPools[])
		{
			if(p.isFull)
				continue;
			return p.allocate();
		}
		auto pool = alloc!(Pool!(T, size));
		mPools.pushBack(pool);
		return pool.allocate();
	}
	
	void free(T* ptr)
	{
		foreach(p; mPools[])
		{
			auto first = p.first;
			if(ptr - first >= 0 && ptr - first < size)
			{
				p.free(ptr);
				return;
			}
		}
		assert(false, "ptr not part of this PoolAllocator");
	}
	
	void clear()
	{
		foreach(p; mPools[])
		{
			xgf.util.alloc.free(p);
		}
		mPools.clear();
// 		mPools.length = 0;
// 		mPools.assumeSafeAppend();
	}
}

struct Pool(T, size_t size)
{
	private T[size] mPayload;
	private BitArray!size mUsed;
	private size_t mInUse;
	
	@property T[] payload() { return mPayload[]; }
	@property bool isFull() { return mInUse == size; }
	@property T* first() { return &mPayload[0]; }
	
	T* allocate()
	{
		assert(!isFull);
		foreach(i, ref t; mPayload)
		{
			if(mUsed[i] == false)
			{
				mUsed[i] = true;
				mInUse++;
				return &t;
			}
		}
		return null;
	}
	
	void free(T* ptr)
	{
		assert(ptr >= &mPayload[0] && ptr <= &mPayload[size-1]);
		auto old = ptr;
		destroy(*ptr);
		auto index = old - &mPayload[0];
		mUsed[index] = false;
		mInUse--;
	}
}