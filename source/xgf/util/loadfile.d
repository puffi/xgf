module xgf.util.loadfile;

import xgf.util.array;
import std.stdio;

bool loadFile(in string filename, ref DynamicArray!ubyte arr)
{
	auto f = File(filename);
	if(!f.isOpen)
		return false;
	auto size = f.size;
	arr.resize(size);
	f.rawRead(arr[]);
	return true;
}
