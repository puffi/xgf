module xgf.util.handle;

import std.traits : isScalarType;

template Handle(string name, T = ushort) if(isScalarType!T)
{
	enum Handle = 
		"struct "~name~"Handle
		{
			// pragma(msg, \"Defined Handle: \", typeof(this), \" with id type: \", "~T.stringof~".stringof);
			private
			{
				"~T.stringof~" mId = typeof(this).invalid.id;
			}
			
			@property auto id() { return mId; }
			@property auto isValid() { return mId != "~T.stringof~".max; }
			
			static @property auto invalid() { return typeof(this)("~T.stringof~".max); }
		}";
}
