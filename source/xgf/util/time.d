module xgf.util.time;

import core.time;

struct Time
{
	private
	{
		long mTicks = 0;
		long mFrequency = 0;
	}
	
	T as(T, string unit = "seconds")() const
	{
		assert(mFrequency > 0);
		
		import std.traits;
		static if(isFloatingPoint!T || isIntegral!T)
			T ret = cast(T)(mTicks) / cast(T)mFrequency;
		else static assert(false, "Type " ~ T.stringof ~ " not supported.");
		
		static if(unit == "s" || unit == "seconds")
			return ret;
		else static if(unit == "ms" || unit == "milliseconds")
			return ret * 1000;
		else static if(unit == "µs" || unit == "microseconds")
			return ret * 1000 * 1000;
		else static assert(false, "Unit " ~ unit ~ " not supported.");
	}
	
	Time opBinary(string op, T)(T val) if(op == "+" || op == "-" || op == "*" || op == "/")
	{
		assert(mFrequency == t.mFrequency);
		assert(mFrequency > 0);
		
		return Time(mixin("mTicks "~op~" val"), mFrequency);
	}
	
	Time opBinary(string op)(Time t) if(op == "+" || op == "-" || op == "*" || op == "/")
	{
		assert(mFrequency == t.mFrequency);
		assert(mFrequency > 0);
		
		return Time(mixin("mTicks "~op~" t.mTicks"), mFrequency);
	}
	
	long opCmp(Time rhs)
	{
		return mTicks - rhs.mTicks;
	}
	
	@property long ticks() { return mTicks; }
	@property long frequency() { return mFrequency; }
}

import std.traits : isFloatingPoint, isIntegral;

Time time(T, string unit = "seconds")(T time) if(isFloatingPoint!T || isIntegral!T)
{
		long frequency = MonoTime.ticksPerSecond;
		
		static if(unit == "s" || unit == "seconds")
			long ticks = cast(long)(time * frequency);
		else static if(unit == "ms" || unit == "milliseconds")
			long ticks = cast(long)((time * frequency) / 1_000);
		else static if(unit == "µs" || unit == "microseconds")
			long ticks = cast(long)((time * frequency) / 1_000_000);
		else static assert(false, "Unit " ~ unit ~ " not supported.");
		
		return Time(ticks, frequency);
}
