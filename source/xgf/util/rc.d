module xgf.util.rc;

import std.traits;
import std.stdio;

mixin template RefCount(T)
{
	public
	{
		static if(is(T == class))
			void delegate(T) mFreeCallback = null;
		else
			void delegate(T*) mFreeCallback = null;
		uint mRefs = 0;
		
		void incRef()
		{
// 			writeln("Increasing ref: ", mRefs);
			++mRefs;
// 			writeln("Now: ", mRefs);
		}
		
		void decRef()
		{
// 			writeln("Decreasing ref: ", mRefs);
			--mRefs;
// 			writeln("Now: ", mRefs);
			if(mRefs == 0)
			{
// 				writeln("mRefs == 0, calling mFreeCallback");
				static if(is(T == class))
				{
					if(mFreeCallback)
						mFreeCallback(this);
				}
				else
				{
					if(mFreeCallback)
						mFreeCallback(&this);
				}
			}
		}
	}
};

auto makeRef(T, Callback, Args...)(Callback cb, auto ref Args args) if(isCallable!Callback)
{
	import xgf.util.alloc;
	auto ptr = alloc!T(args);
// 	__traits(getMember, *ptr, "mFreeCallback") = cb;
	ptr.mFreeCallback = cb;
	ptr.mRefs = 0;
// 	return Rc!T(ptr);
	return ptr;
}

auto makeRef(T, Args...)(auto ref Args args)
{
	import xgf.util.alloc;
	auto ptr = alloc!T(args);
	import std.functional : toDelegate;
// 	__traits(getMember, *ptr, "mFreeCallback") = (&freeRef!T).toDelegate;
	ptr.mFreeCallback = (&freeRef!T).toDelegate;
	ptr.mRefs = 0;
// 	return Rc!T(ptr);
	return ptr;
}

void freeRef(T)(T ptr) if(is(T == class))
{
	import xgf.util.alloc;
	free(ptr);
}

void freeRef(T)(T* ptr) if(!is(T == class))
{
	import xgf.util.alloc;
// 	writeln("Freeing pointer: ", ptr);
// 	writeln("Object: ", *ptr);
	free(ptr);
}

struct Rc(T) if(isRefCounted!T)
{
	static if(is(T == class))
		private T mPtr = null;
	else
		private T* mPtr = null;
	
	static if(is(T == class))
	{
		this(T ptr)
		{
			mPtr = ptr;
			if(mPtr)
				mPtr.incRef();
		}
	}
	else
	{
		this(T* ptr)
		{
	// 		writeln("Calling Rc.__ctor with: ", ptr);
			mPtr = ptr;
	// 		__traits(getMember, *mPtr, "incRef")();
			if(mPtr)
				mPtr.incRef();
	// 		writeln("Rc.mPtr: ", mPtr);
		}
	}
	
	this(this)
	{
// 		writeln("Copying Rc: ", mPtr);
// 		__traits(getMember, *mPtr, "incRef")();
		if(mPtr)
			mPtr.incRef();
	}
	
	~this()
	{
// 		__traits(getMember, *mPtr, "decRef")();
// 		writeln("Destroying Rc: ", mPtr);
		if(mPtr)
			mPtr.decRef();
	}
	
	void opAssign(Rc!T rhs)
	{
// 		writeln(__PRETTY_FUNCTION__);
		if(mPtr)
			mPtr.decRef();
		mPtr = rhs.mPtr;
		mPtr.incRef();
	}
	
	static if(is(T == class))
	{
		void opAssign(T ptr)
		{
			if(mPtr)
				mPtr.decRef();
			mPtr = ptr;
			if(mPtr)
				mPtr.incRef();
		}
	}
	else
	{
		void opAssign(T* ptr)
		{
	// 		writeln(__PRETTY_FUNCTION__);
	// 		writeln("Calling opAssign, old: ", mPtr);
	// 		writeln("Calling opAssign with: ", ptr);
			if(mPtr)
				mPtr.decRef();
			mPtr = ptr;
			if(mPtr)
				mPtr.incRef();
		}
	}
	
// 	void opAssign(Rc!T rc)
// 	{
// 		mPtr = rc.mPtr;
// 	}
	
	@property auto rawPtr() { return mPtr; }
	
	import std.typecons : Proxy;
	mixin Proxy!mPtr;
	auto ref opUnary(string op : "*")()
	{
		assert(mPtr);
		return *mPtr;
	}
	
	alias rawPtr this;
}

auto rc(T)(T* ptr) if(!is(T == class))
{
	return Rc!T(ptr);
}

auto rc(T)(T ptr) if(is(T == class))
{
	return Rc!T(ptr);
}

template isRefCounted(T)
{
	import std.traits;
	static if(hasMember!(T, "incRef") && hasMember!(T, "decRef") && hasMember!(T, "mRefs") && hasMember!(T, "mFreeCallback"))
		enum isRefCounted = true;
	else
		enum isRefCounted = false;
}
