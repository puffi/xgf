module xgf.util.queue;

import xgf.ecs.util;

struct Queue(T, size_t NodePoolSize = 64)
{
	private
	{
		PoolAllocator!(QueueNode!T, NodePoolSize) mAllocator;
		QueueNode!T* mFirst;
		QueueNode!T* mLast;
	}
	
	void put(T val)
	{
		auto node = mAllocator.allocate();
		node.value = val;
		if(mFirst is null)
		{
			mFirst = node;
			mLast = node;
		}
		else
		{
			mLast.next = node;
			node.previous = mLast;
			mLast = node;
		}
	}
	
	auto get()
	{
		auto val = front;
		popFront();
		return val;
	}
	
	private
	{
		auto front()
		{
			return mFirst.value;
		}
		
		void popFront()
		{
			assert(!empty);
			
			auto first = mFirst;
			mFirst = first.next;
			if(mFirst is null)
				mLast = null;
			mAllocator.free(first);
		}
	}
	
	@property auto range()
	{
		return QueueRange!T(mFirst);
	}
	
	@property bool empty()
	{
		return mFirst is null;
	}
	
	@property auto first() { return mFirst; }
	@property auto last() { return mLast; }
	
	void clear()
	{
		mAllocator.clear();
		mFirst = null;
		mLast = null;
	}
}

struct QueueNode(T)
{
	QueueNode!T* next;
	QueueNode!T* previous;
	T value;
}

struct QueueRange(T)
{
	private
	{
		QueueNode!T* mFirst;
	}
	
	@property bool empty() { return mFirst is null; }
	
	@property auto front() { return mFirst.value; }
	void popFront() { mFirst = mFirst.next; }
}

struct PriorityQueue(T, PT, size_t NodePoolSize = 64)
{
	private
	{
		PoolAllocator!(PriorityQueueNode!(T, PT), NodePoolSize) mAllocator;
		PriorityQueueNode!(T, PT)* mFirst;
		PriorityQueueNode!(T, PT)* mLast;
	}
	
	void put(T val, PT priority)
	{
		auto node = mAllocator.allocate();
		node.priority = priority;
		node.value = val;
		if(mFirst is null)
		{
			mFirst = node;
			mLast = node;
		}
		else
		{
			if(mFirst.priority > priority)
			{
				node.next = mFirst;
				mFirst = node;
			}
			else
			{
				auto curr = mFirst;
				while(curr.next !is null && curr.next.priority <= priority)
				{
					curr = curr.next;
				}
				assert(curr.next is null || curr.next.priority > priority);
				if(curr.next is null)
				{
					curr.next = node;
					node.previous = curr;
					mLast = node;
				}
				else
				{
					node.previous = curr;
					node.next = curr.next;
					curr.next = node;
					node.next.previous = node;
				}
			}
		}
	}
	
	auto get()
	{
		auto val = front;
		popFront();
		return val;
	}
	
	private
	{
		auto front()
		{
			import std.typecons : tuple;
			return tuple(mFirst.priority, mFirst.value);
		}
		
		void popFront()
		{
			assert(!empty);
			
			auto first = mFirst;
			mFirst = first.next;
			if(mFirst is null)
				mLast = null;
			mAllocator.free(first);
		}
	}
	
	@property auto range() { return PriorityQueueRange!(T, PT)(mFirst); }
	
	@property bool empty()
	{
		return mFirst is null;
	}
	
	@property auto first() { return mFirst; }
	@property auto last() { return mLast; }
	
	void clear()
	{
		mAllocator.clear();
		mFirst = null;
		mLast = null;
	}
}

struct PriorityQueueNode(T, PT)
{
	PriorityQueueNode!(T, PT)* next;
	PriorityQueueNode!(T, PT)* previous;
	PT priority;
	T value;
}

struct PriorityQueueRange(T, PT)
{
	private
	{
		PriorityQueueNode!(T, PT)* mFirst;
	}
	
	@property bool empty() { return mFirst is null; }
	
	@property auto front() { import std.typecons : tuple; return tuple(mFirst.priority, mFirst.value); }
	void popFront() { mFirst = mFirst.next; }
}
