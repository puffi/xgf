module xgf.util.hash;

import murmurhash3;
import std.string : representation;

enum Seed = 0x123456;

auto hash(in ubyte[] data)
{
	return murmurHash3_x86_32(data, Seed);
}

auto hash(T)(in T[] data)
{
	return hash(cast(ubyte[])data);
}

struct StringHash
{
	Hash32 hash;
	alias hash this;

	this(in string str)
	{
		hash = murmurHash3_x86_32(str.representation, Seed);
		assert(!collides(str, hash));
	}

	this(in Hash32 hash)
	{
		this.hash = hash;
	}

	this(in uint hash)
	{
		this.hash.hash = hash;
	}

	void opAssign(in string str)
	{
		hash = murmurHash3_x86_32(str.representation, Seed);
		assert(!collides(str, hash));
	}

	void opAssign(in Hash32 hash)
	{
		this.hash = hash;
	}

	void opAssign(in uint hash)
	{
		this.hash.hash = hash;
	}
	
	size_t toHash() const nothrow @safe pure
	{
		return hash;
	}
	
	private
	{
		auto collides(in string s, in Hash32 hash)
		{
			if(auto ptr = hash in mHashes)
			{
				if(*ptr != s)
					return true;
			}
			mHashes[hash] = s;
			return false;
		}
		
		static string[Hash32] mHashes;
	}
}

StringHash stringhash(in string str)
{
	auto hash = murmurHash3_x86_32(str.representation, Seed);
	return StringHash(hash);
}

StringHash stringhash(in string str, uint seed)
{
	auto hash = murmurHash3_x86_32(str.representation, seed);
	return StringHash(hash);
}

template HashOf(string s)
{
	enum HashOf = s.stringhash;
	// pragma(msg, "Hash for ", s, " is: ", HashOf);
}

template HashOf(T...)
{
	enum HashOf = HashOf!(T.stringof);
}
