module xgf.util.bitarray;

import xgf.util;


struct DynamicBitArray
{
	enum BitsPerIndex = size_t.sizeof * 8;
	private size_t[] mPayload;
	
	bool opIndex(size_t index)
	{
		auto arrIndex = index / BitsPerIndex;
		auto bit = index % BitsPerIndex;
		return (mPayload[arrIndex] & (size_t(1) << bit)) != 0;
	}
	
	void opIndexAssign(bool val, size_t index)
	{
		auto arrIndex = index / BitsPerIndex;
		auto bit = index % BitsPerIndex;
		mPayload[arrIndex] &= ~(size_t(1) << bit);
		mPayload[arrIndex] |= val ? (size_t(1) << bit) : 0;
	}
	
	@property size_t length() { return mPayload.length * BitsPerIndex; }
	@property void length(size_t val)
	{
		if(mPayload.length * BitsPerIndex < val)
			mPayload.length = (val + BitsPerIndex-1) / BitsPerIndex;
	}
}

struct BitArray(size_t size)
{
	enum BitsPerIndex = size_t.sizeof * 8;
// 	pragma(msg, "BitsPerIndex: ", BitsPerIndex);
	enum ArraySize = (size+BitsPerIndex-1) / BitsPerIndex;
	private size_t[ArraySize] mPayload;
// 	pragma(msg, mPayload.sizeof);
	
	bool opIndex(size_t index)
	{
		auto arrIndex = index / (BitsPerIndex);
		auto bit = index % BitsPerIndex;
		return (mPayload[arrIndex] & (size_t(1) << bit)) != 0;
	}
	
	void opIndexAssign(bool val, size_t index)
	{
		auto arrIndex = index / (BitsPerIndex);
		auto bit = index % BitsPerIndex;
		mPayload[arrIndex] &= ~(size_t(1) << bit);
		mPayload[arrIndex] |= val ? (size_t(1) << bit) : 0;
	}
}