module xgf.ecs.processorset;

import xgf.util.hash;
import xgf.ecs.processor;
import xgf.ecs.util;

struct ProcessorSet
{
	private
	{
		Processor[uint] mProcessors;
		bool mIsActive;
	}
	
	void add(T : Processor)(T proc)
	{
		enum nameHash = HashOf!T;
		mProcessors[nameHash] = proc;
		if(isActive)
			proc.onActivation();
	}
	
	void remove(T : Processor)()
	{
		enum nameHash = HashOf!T;
		if(auto proc = nameHash in mProcessors)
		{
			proc.onDeactivation();
			mProcessors.remove(nameHash);
		}
	}
	
	T get(T : Processor)()
	{
		enum nameHash = HashOf!T;
		if(auto proc = nameHash in mProcessors)
			return *proc;
		return null;
	}
	
	void activate()
	{
		mIsActive = true;
		foreach(proc; mProcessors)
		{
			proc.onActivation();
		}
	}
	
	void deactivate()
	{
		mIsActive = false;
		foreach(proc; mProcessors)
		{
			proc.onDeactivation();
		}
	}
	
	@property bool isActive() { return mIsActive; }
}
