module xgf.ecs.util;

import std.stdio;

import xgf.util;

struct Component
{
	
}

struct Requires(Components...) if(allComponents!Components)
{
	
}

bool allComponents(T...)()
{
	bool ret = true;
	foreach(Type; T)
	{
		import std.traits : hasUDA;
		static if(!hasUDA!(Type, Component))
			ret = false;
	}
	return ret;
}

template EntityIdArrayTypeTuple(EntityId, size_t num)
{
	import std.typetuple : TypeTuple;
	static if(num == 0)
		alias EntityIdArrayTypeTuple = TypeTuple!();
	else
		alias EntityIdArrayTypeTuple = TypeTuple!(EntityId[], EntityIdArrayTypeTuple!(EntityId, num-1));
}

template JoinComponentArrayTypeTuple(Components...)
{
	import std.typetuple : TypeTuple;
	static if(Components.length == 0)
		alias JoinComponentArrayTypeTuple = TypeTuple!();
	else
		alias JoinComponentArrayTypeTuple = TypeTuple!(Components[0]*[], JoinComponentArrayTypeTuple!(Components[1..$]));
}

template JoinComponentTypeTuple(Components...)
{
	import std.typetuple : TypeTuple;
	static if(Components.length == 0)
		alias JoinComponentTypeTuple = TypeTuple!();
	else
		alias JoinComponentTypeTuple = TypeTuple!(Components[0]*, JoinComponentTypeTuple!(Components[1..$]));
}

struct JoinRange(Intersection, EntityId, Components...)
{
	alias components = Components;
	
	import std.typecons : Tuple;
	private
	{
		Intersection mIntersection;
		Tuple!(EntityIdArrayTypeTuple!(EntityId, Components.length)) mEntities;
		Tuple!(JoinComponentArrayTypeTuple!Components) mComponents;
		
		alias EntitiesType = typeof(mEntities);
		alias ComponentsType = typeof(mComponents);
	}
	
	this(Intersection i, EntitiesType ents, ComponentsType comps)
	{
		mIntersection = i;
		mEntities = ents;
		mComponents = comps;
		
		import std.algorithm : countUntil;
		import std.range : popFrontN;
		if(mIntersection.empty)
			return;
		
		foreach(idx, _; Components)
		{
			auto count = mEntities[idx].countUntil(mIntersection.front);
			mEntities[idx].popFrontN(count);
			mComponents[idx].popFrontN(count);
		}
	}
	
	@property auto front()
	{
		import std.typecons : tuple, Tuple;
		import std.range : front;
		Tuple!(JoinComponentTypeTuple!Components) comps;
		foreach(i, _; Components)
		{
			comps[i] = mComponents[i].front;
		}
		return tuple(mIntersection.front, comps.expand);
	}
	
	void popFront()
	{
		mIntersection.popFront();
		if(mIntersection.empty)
			return;
		
		import std.algorithm: countUntil;
		import std.range: popFrontN;
		foreach(i, _; Components)
		{
			auto count = mEntities[i].countUntil(mIntersection.front);
			mEntities[i].popFrontN(count);
			mComponents[i].popFrontN(count);
		}
	}
	
	@property bool empty()
	{
		return mIntersection.empty;
	}
	
	auto select(Comps...)() if(isComponentSubset!(typeof(this), Comps))
	{
		return SelectRange!(typeof(this), Comps)(this);
	}
	
	auto eager()
	{
		return EagerRange!(typeof(this), Components)(this);
	}
}

struct EagerRange(JR, Components...)
{
	private
	{
		import xgf.util;
		DynamicArray!(typeof(JR.front)) mArray;
		size_t mIndex;
	}
	
	this(JR jr)
	{
		import std.algorithm : each;
		jr.each!(a => mArray.pushBack(a));
	}
	
	@property auto each()
	{
		return EachRange!(Components)(mArray[]);
	}
	
	@property auto values() { return mArray[]; }
}

struct EachRange(Components...)
{
	import std.range;
	import std.typecons;
	import xgf.ecs.entityid;
	alias components = Components;
	alias Type = Tuple!(EntityId, JoinComponentTypeTuple!Components);
	
	private
	{
		Type[] mValues;
	}
	
	this(Type[] values)
	{
		mValues = values;
	}
	
	@property auto front() { return mValues.front; }
	@property auto empty() { return mValues.empty; }
	void popFront() { mValues.popFront(); }
	
	auto select(Comps...)()
	{
		return SelectRange!(typeof(this), Comps)(this);
	}
	
	auto opIndex(size_t index)
	{
		return mValues[index];
	}
	
	@property auto length() { return mValues.length; }
}

template SelectComponentTypeTuple(Components...)
{
	import std.typetuple : TypeTuple;
	static if(Components.length == 0)
		alias SelectComponentTypeTuple = TypeTuple!();
	else
		alias SelectComponentTypeTuple = TypeTuple!(Components[0]*, SelectComponentTypeTuple!(Components[1..$]));
}

struct SelectRange(JR, Components...) if(isComponentSubset!(JR, Components))
{
	import std.typecons : tuple, Tuple;
	
	private
	{
		JR mJoinRange;
	}
	
	this(JR jr)
	{
		mJoinRange = jr;
	}
	
	@property auto front()
	{
		Tuple!(SelectComponentTypeTuple!Components) comps;
		auto joined = mJoinRange.front;
		foreach(i, type; Components)
		{
			foreach(j, jType; JR.components)
			{
				static if(is(type == jType))
				{
					comps[i] = joined[j+1]; // entity id ignored
				}
			}
		}
		
		return tuple(joined[0], comps.expand);
	}
	
	void popFront()
	{
		mJoinRange.popFront();
	}
	
	@property bool empty()
	{
		return mJoinRange.empty();
	}
}

bool isComponentSubset(JR, Components...)()
{
	bool isSubset = true;
	foreach(t1; Components)
	{
		bool found = false;
		foreach(t2; JR.components)
		{
			static if(is(t1 == t2))
			{
				found = true;
			}
		}
		isSubset &= found;
	}
	return isSubset;
}
