module xgf.ecs.ecs;

import xgf.ecs.entitymanager;
import xgf.ecs.entityid;
import xgf.ecs.componentmanager;
import xgf.ecs.processorset;
import xgf.core.core;
import xgf.util.hash;
import xgf.util.alloc : gc;

struct Entity
{
	ECS* ecs;
	EntityId id;
	
	auto add(T, Args...)(Args args)
	{
		return ecs.add!T(id, args);
	}
	void remove(T)() { ecs.remove!T(id); }
	
	void destroy() { ecs.destroy(this); }
	bool isAlive() { return ecs.isAlive(id); }
}

@gc
struct ECS
{
	private
	{
		Core* mCore;
		EntityManager* mEntityMgr;
		ComponentManager* mComponentManager;
		ProcessorSet*[uint] mProcessorSets;
		ProcessorSet* mActiveProcessorSet;
	}
	
	this(Core* core)
	{
		mCore = core;
		mEntityMgr = new EntityManager();
		mEntityMgr.initialize(1024, 1024);
		mComponentManager = new ComponentManager();
		addProcessorSet("Default".hash, new ProcessorSet());
		activateProcessorSet("Default".hash);
		mCore.subscribe!"EntityDestroyed"(&mComponentManager.onEntityDestroyed);
	}
	
	Entity create()
	{
		return Entity(&this, mEntityMgr.create());
	}
	
	void destroy(Entity e)
	{
		destroy(e.id);
	}
	
	void destroy(EntityId id)
	{
		mEntityMgr.destroy(id);
		mCore.broadcast!"EntityDestroyed"(id);
	}
	
	void addProcessorSet(uint id, ProcessorSet* set)
	{
		mProcessorSets[id] = set;
	}
	
	void removeProcessorSet(uint id)
	{
		if(auto set = id in mProcessorSets)
			mProcessorSets.remove(id);
	}
	
	void activateProcessorSet(uint id)
	{
		assert(id in mProcessorSets, "No set with id found.");
		if(mActiveProcessorSet)
			mActiveProcessorSet.deactivate();
		mActiveProcessorSet = mProcessorSets[id];
		if(mActiveProcessorSet)
			mActiveProcessorSet.activate();
	}
	
	@property ProcessorSet* activeProcessorSet() { return mActiveProcessorSet; }
	@property auto core() { return mCore; }
	
	auto add(T, Args...)(EntityId id, Args args)
	{
		// import xgf.ecs.util : Component;
		// import std.traits : hasUDA;
		// static assert(hasUDA!(T, Component), "Template type must be annotated with @Component.");
		static assert(is(T == struct), "Template type must be a struct.");
		
		if(!isAlive(id))
			return null;
		auto comp = mComponentManager.create!T(id);
		assert(comp);
		(*comp) = T(args);
		return comp;
	}
	
	void remove(T)(EntityId id)
	{
		mComponentManager.destroy!T(id);
	}
	
	auto get(T)(EntityId id)
	{
		return mComponentManager.get!T(id);
	}
	
	bool isAlive(EntityId id)
	{
		return mEntityMgr.isAlive(id);
	}
	
	auto select(Components...)() if(Components.length == 1)
	{
		import std.range : zip;
		return zip(mComponentManager.getEntities!Components, mComponentManager.getComponents!Components);
	}
	
	auto join(Components...)() if(Components.length > 1)
	{
		import std.algorithm : setIntersection;
		import std.typecons : Tuple;
		import xgf.ecs.util : EntityIdArrayTypeTuple, JoinComponentArrayTypeTuple, JoinRange;
		Tuple!(EntityIdArrayTypeTuple!(EntityId, Components.length)) entities;
		Tuple!(JoinComponentArrayTypeTuple!Components) components;
		
		foreach(i, _; Components)
		{
			entities[i] = mComponentManager.getEntities!(Components[i]);
			components[i] = mComponentManager.getComponents!(Components[i]);
		}
		
		auto intersected = setIntersection!"a < b"(entities.expand);
		
		return JoinRange!(typeof(intersected), EntityId, Components)(intersected, entities, components);
	}
}

alias Component = xgf.ecs.util.Component;
