module xgf.ecs.entitymanager;

import xgf.ecs.entityid;
import xgf.ecs.util;
import xgf.util;

struct EntityManager
{
	private
	{
		EntityId[] mEntities;
		DynamicBitArray mUsedEntities;
		size_t mFirstFreeEntity;
		size_t mAllocateStep;
		
		void resizeAll()
		{
			mEntities.length = mEntities.length + mAllocateStep;
			mUsedEntities.length = mUsedEntities.length + mAllocateStep;
			foreach(i, ref e; mEntities[mFirstFreeEntity..$])
			{
				e.id = i + mFirstFreeEntity;
			}
		}
		
		void findNextFreeEntity()
		{
			auto old = mFirstFreeEntity;
			foreach(i; mFirstFreeEntity..mUsedEntities.length)
			{
// 				auto used = mUsedEntities[i];
// 				if(!used)
				if(!mUsedEntities[i])
				{
					mFirstFreeEntity = i;
					return;
				}
			}
			mFirstFreeEntity = mEntities.length;
		}
	}
	
	void initialize(size_t preallocated = 1024, size_t allocateStep = 1024)
	in { assert(preallocated <= 0xFFFFFFFFFFFF,
				"Can't allocate more than 48bits can handle, upper bits are used for revision"); }
	body
	{
		mAllocateStep = allocateStep;
		mEntities.length = preallocated;
		mUsedEntities.length = preallocated;
		// 0 is special entity
		mFirstFreeEntity = 1;
		
		foreach(i, ref e; mEntities)
		{
			e.id = i;
		}
	}
	
	EntityId create()
	{
		if(mFirstFreeEntity == mEntities.length)
		{
			resizeAll();
		}
		
		mEntities[mFirstFreeEntity].incRev();
		EntityId ret = mEntities[mFirstFreeEntity];
		mUsedEntities[mFirstFreeEntity] = true;
		findNextFreeEntity();
		
		return ret;
	}
	
	void destroy(EntityId id)
	{
		mUsedEntities[id.id] = false;
		if(mFirstFreeEntity > id.id)
			mFirstFreeEntity = id.id;
	}
	
	bool isAlive(EntityId id)
	{
		return mUsedEntities[id.id];
	}
}
