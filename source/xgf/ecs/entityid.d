module xgf.ecs.entityid;

enum RevisionSize = RevSize!size_t;
alias RevisionType = RevType!size_t;
enum RevisionShift = RevShift!size_t;
enum IdMask = _IdMask!size_t;
enum InvalidEntityId = EntityId(0);

bool isInvalidEntityId(EntityId id) { return id.id == 0; }

private static char[21+5+15] EntityIdStringBuffer = ' ';

struct EntityId
{
	package size_t mId;
	
	@property RevisionType rev() const @safe nothrow pure { return mId >> RevisionShift; }
	@property size_t id() const @safe nothrow pure { return mId & IdMask; }
	
	package void incRev() @safe nothrow pure
	{
		auto r = rev;
		mId &= IdMask;
		mId |= cast(size_t)(++r) << RevisionShift;
	}
	
	package @property void id(size_t val) @safe nothrow pure
	{
		mId &= ~IdMask;
		mId |= val & IdMask;
	}
	
	string toString() const
	{
// 		import std.string;
// 		return format("[EID]: [REV]: %s, [ID]: %s", rev, id);
// 		import std.conv : /*to,*/ toChars;
// 		return "[EID]: [REV]: " ~ rev.to!string ~ " [ID]: " ~ id.to!string;
		import std.conv : toChars;
		EntityIdStringBuffer[0..14] = "[EID]: [REV]: ";
		EntityIdStringBuffer[19..26] = " [ID]: ";
		auto revChars = (cast(uint)rev).toChars;
		auto idChars = id.toChars;
		auto revOffset = 5-revChars.length;
		auto idOffset = 15-idChars.length;
		
		import std.range : enumerate;
		foreach(i, c; revChars.enumerate)
		{
			EntityIdStringBuffer[14+revOffset+i] = c;
		}
		foreach(i, c; idChars.enumerate)
		{
			EntityIdStringBuffer[26+idOffset+i] = c;
		}
		return cast(string)EntityIdStringBuffer[];
	}
	
	long opCmp(EntityId rhs) const @safe nothrow pure
	{
		return id - rhs.id;
	}
}

private
{
	template RevSize(T)
	{
		static if(T.sizeof == 4)
			enum RevSize = 1;
		else static if(T.sizeof == 8)
			enum RevSize = 2;
		else static assert(false);
	}
	
	template RevType(T)
	{
		static if(T.sizeof == 4)
			alias RevType = ubyte;
		else static if(T.sizeof == 8)
			alias RevType = ushort;
		else static assert(false);
	}
	
	template RevShift(T)
	{
		static if(T.sizeof == 4)
			enum RevShift = 24;
		else static if(T.sizeof == 8)
			enum RevShift = 48;
		else static assert(false);
	}
	
	template _IdMask(T)
	{
		static if(T.sizeof == 4)
			enum size_t _IdMask = 0x00FFFFFF;
		else static if(T.sizeof == 8)
			enum size_t _IdMask = 0x0000FFFFFFFFFFFF;
		else static assert(false);
	}
}
