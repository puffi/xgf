module xgf.ecs.processor;

import xgf.ecs.ecs;

class Processor
{
	private
	{
		ECS* mECS;
	}
	
	this(ECS* ecs)
	{
		mECS = ecs;
	}
	
	void onActivation() {}
	void onDeactivation() {}
	
	final @property ECS* ecs() { return mECS; }
}
