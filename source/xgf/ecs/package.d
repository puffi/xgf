module xgf.ecs;

public import xgf.ecs.ecs;
public import xgf.ecs.entityid;
public import xgf.ecs.processor;
public import xgf.ecs.processorset;
public import xgf.ecs.util : Component;
