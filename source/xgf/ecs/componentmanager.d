module xgf.ecs.componentmanager;

import xgf.ecs.util;
import xgf.ecs.entityid;
import xgf.util.hash;
import xgf.util.pool;

import std.stdio;

struct ComponentManager
{
	private
	{
		IComponentStorage[uint] mComponentStorages;
	}
	
	void onEntityDestroyed(EntityId id)
	{
		foreach(storage; mComponentStorages.byValue)
		{
			storage.destroy(id);
		}
	}
	
	T* create(T)(EntityId id)
	{
		if(auto ptr = HashOf!T in mComponentStorages)
		{
			auto p = ptr.allocate(id);
			writeln("Entity: ", id, " Comp: ", p);
			return cast(T*)p;
		}
		else
		{
			auto cs = new DefaultComponentStorage!T;
			mComponentStorages[HashOf!T] = cs;
			auto p = cs.allocate(id);
			writeln("Entity: ", id, " Comp: ", p);
			return p;
		}
	}
	
	void destroy(T)(EntityId id)
	{
		if(auto ptr = HashOf!T in mComponentStorages)
		{
			ptr.destroy(id);
		}
	}
	
	void clear(T)()
	{
		if(auto ptr = HashOf!T in mComponentStorages)
		{
			ptr.destroyAll;
		}
	}
	
	T* get(T)(EntityId id)
	{
		if(auto ptr = HashOf!T in mComponentStorages)
		{
			return cast(T*)(ptr.get(id));
		}
		return null;
	}
	
	auto getEntities(T)()
	{
		import std.stdio;
// 		writeln("Component: ", T.stringof, " Hash: ", HashOf!T);
// 		writeln("Storage: ", &mComponentStorages);
// 		writeln("AA empty: ", mComponentStorages.empty);
		
		if(auto ptr = HashOf!T in mComponentStorages)
		{
			return ptr.entities;
		}
		return null;
	}
	
	auto getComponents(T)()
	{
		if(auto ptr = HashOf!T in mComponentStorages)
		{
			return cast(T*[])(ptr.components);
		}
		return null;
	}
}

interface IComponentStorage
{
	void* allocate(EntityId id);
	void* get(EntityId id);
	void destroy(EntityId id);
	void destroyAll();
	@property EntityId[] entities();
	@property void*[] components();
}

final class DefaultComponentStorage(T) : IComponentStorage
{
	private
	{
		EntityId[] mEntityIds;
		T*[] mComponents;
		PoolAllocator!(T, 64) mPoolAllocator;
		bool mSorted = true;
		
		void sort()
		{
			import std.algorithm : sort;
			import std.range : zip;
			
			auto zipped = zip(mEntityIds, mComponents);
			zipped.sort!("a[0] < b[0]");
			mSorted = true;
		}
	}
	
	final override T* allocate(EntityId id)
	{
		auto comp = get(id);
		if(comp)
		{
			return comp;
		}
		comp = mPoolAllocator.allocate();
		mEntityIds ~= id;
		mComponents ~= comp;
		mSorted = false;
		return comp;
	}
	
	final override T* get(EntityId id)
	{
		import std.algorithm : countUntil;
		if(!mSorted)
			sort();
		
		import std.range : assumeSorted;
		auto sorted = mEntityIds.assumeSorted!("a < b");
		auto index = sorted.countUntil(id);
		if(index == -1)
		{
// 			writeln("Component for ", id, " not found.");
			return null;
		}
		return mComponents[index];
	}
	
	final override void destroy(EntityId id)
	{
		if(!mSorted)
			sort();
		
		import std.range : assumeSorted;
		import std.algorithm : countUntil, remove, SwapStrategy;
		auto sorted = mEntityIds.assumeSorted!("a < b");
		auto index = sorted.countUntil(id);
		if(index == -1)
		{
// 			writeln("Can't destroy component for ", id, ". No entry.");
			return;
		}
		mEntityIds = mEntityIds.remove!(SwapStrategy.unstable)(index);
		mPoolAllocator.free(mComponents[index]);
		mComponents = mComponents.remove!(SwapStrategy.unstable)(index);
		mSorted = false;
	}
	
	final override void destroyAll()
	{
		mEntityIds.length = 0;
		mEntityIds.assumeSafeAppend();
		mComponents.length = 0;
		mComponents.assumeSafeAppend();
		mPoolAllocator.clear();
	}
	
	final override @property EntityId[] entities() { if(!mSorted) sort(); return mEntityIds; }
	final override @property void*[] components() { if(!mSorted) sort(); return cast(void*[])mComponents; }
	final @property auto both() { import std.range : zip; if(!mSorted) sort(); return zip(mEntityIds, mComponents); }
}
