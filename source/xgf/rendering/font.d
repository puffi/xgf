module xgf.rendering.font;

import stb.truetype;

import xgf.graphics.graphics;
import xgf.graphics.texture;
import xgf.graphics.sampler;
import xgf.graphics.state;

import xgf.math : vec4;

import xgf.util.array;

import std.stdio;

enum MaxGlyphsPerRow = 32;
enum MaxGlyphsPerColumn = 32;
enum MaxGlyphsPerFont = MaxGlyphsPerColumn * MaxGlyphsPerRow;

struct Font
{
	private
	{
		string mFontName;
		uint mSize;
		Graphics* mGraphics;
		Array!ubyte mFontData;
		stbtt_fontinfo mFont;
		float mScale;
		float mAscent;
		float mDescent;
		float mLinegap;
		DynamicArray!(Glyph) mGlyphs;
		DynamicArray!ubyte mData;
		bool mOutOfDate;
		
		TextureHandle mTexture;
		SamplerHandle mSampler;
		BlendState mBlendState;
	}
	
	this(in ubyte[] data, uint size, Graphics* graphics)
	{
// 		mFontName = fontname;
		mSize = size;
		mGraphics = graphics;
		
// 		auto file = File(fontname);
		import std.range : array;
		import std.algorithm : joiner;
// 		auto fileSize = file.size;
// 		DynamicArray!ubyte fontdata;
// 		mFontData.resize(fileSize);
		mFontData = Array!ubyte(data.length);
		mFontData[][] = data;
// 		file.rawRead(mFontData[]);
		stbtt_InitFont(&mFont, mFontData.ptr, stbtt_GetFontOffsetForIndex(mFontData.ptr, 0));
		
		mScale = stbtt_ScaleForPixelHeight(&mFont, mSize);
		int ascent;
		int descent;
		int linegap;
		stbtt_GetFontVMetrics(&mFont, &ascent, &descent, &linegap);
		mAscent = ascent * mScale;
		mDescent = descent * mScale;
		mLinegap = linegap * mScale;
		
		mData.resize(mSize.roundToPow2 * mSize.roundToPow2 * MaxGlyphsPerFont);
		mData[][] = 0;
		
		mTexture = mGraphics.createTexture2D(mData[], mSize.roundToPow2 * MaxGlyphsPerRow, mSize.roundToPow2 * MaxGlyphsPerColumn, Format.r8);//, Format.r8);
		mSampler = mGraphics.getSampler(Filter.nearest, Filter.nearest);
// 		mGraphics.setSamplerMinFilter(mSampler, Filter.nearest);
// 		mGraphics.setSamplerMagFilter(mSampler, Filter.nearest);
		mBlendState.enabled = true;
		mBlendState.func = BlendState.Func(BlendState.Factor.srcAlpha, BlendState.Factor.oneMinusSrcAlpha);
		mBlendState.equation = BlendState.Equation.add;
		mOutOfDate = true;
	}
	
	auto getGlyph(dchar c)
	{
		foreach(g; mGlyphs[])
		{
			if(g.character == c)
				return g;
		}
		
		return loadGlyph(c);
	}
	
	auto loadGlyph(dchar c)
	{
		if(mGlyphs.length >= MaxGlyphsPerFont)
			return Glyph.init;
		
		static ubyte[] data;
		data.length = mSize * mSize;
		data[] = 0;
		
		auto index = stbtt_FindGlyphIndex(&mFont, c);
		int advance;
		int leftBearing;
		stbtt_GetGlyphHMetrics(&mFont, index, &advance, &leftBearing);
		
		int xMin;
		int xMax;
		int yMin;
		int yMax;
		stbtt_GetGlyphBitmapBox(&mFont, index, mScale, mScale, &xMin, &yMin, &xMax, &yMax);
		
		auto width = xMax - xMin;
		auto height = yMax - yMin;
		
		auto numGlyphs = mGlyphs.length;
		auto idxX = numGlyphs % MaxGlyphsPerRow;
		auto idxY = numGlyphs / MaxGlyphsPerColumn;
		
		Glyph glyph;
		glyph.character = c;
		glyph.advance = advance * mScale;
		glyph.leftBearing = leftBearing * mScale;
		glyph.xMin = xMin;
		glyph.yMin = yMin;
		glyph.xMax = xMax;
		glyph.yMax = yMax;
		glyph.width = width;
		glyph.height = height;
		auto uMin = cast(float)idxX / MaxGlyphsPerRow;
		auto uMax = cast(float)width / (MaxGlyphsPerRow * mSize.roundToPow2) + uMin;
		auto vMin = cast(float)idxY / MaxGlyphsPerColumn;
		auto vMax = cast(float)height / (MaxGlyphsPerColumn * mSize.roundToPow2) + vMin;
		glyph.uv = vec4(uMin, vMin, uMax, vMax);
		
		mGlyphs.pushBack(glyph);
		
		stbtt_MakeGlyphBitmap(&mFont, data.ptr + (mSize - height) * mSize, mSize, mSize, mSize, mScale, mScale, index);
		
		foreach(h; 0..mSize)
		{
			foreach(w; 0..mSize)
			{
				auto flippedHeight = mSize - 1 - h;
				auto texdataIndex = (w + idxX * mSize.roundToPow2) + (flippedHeight + idxY * mSize.roundToPow2) * MaxGlyphsPerRow * mSize.roundToPow2;
				mData[texdataIndex] = data[w + h * mSize];
			}
		}
		
		mOutOfDate = true;
		return glyph;
	}
	
	void update()
	{
		if(!mOutOfDate)
			return;
		
		mGraphics.updateTexture2D(mTexture, mData[], mSize.roundToPow2 * MaxGlyphsPerRow, mSize.roundToPow2 * MaxGlyphsPerColumn);
		
		mOutOfDate = false;
	}
	
	@property auto texture() { return mTexture; }
	@property auto sampler() { return mSampler; }
	@property auto blendState() { return mBlendState; }
	
	@property auto size() { return mSize; }
	@property auto ascent() { return mAscent; }
	@property auto descent() { return mDescent; }
	@property auto scale() { return mScale; }
}

struct Glyph
{
	dchar character;
	float advance;
	float leftBearing;
	int xMin;
	int yMin;
	int xMax;
	int yMax;
	int width;
	int height;
	vec4 uv;
}

auto roundToPow2(uint v)
{
	v--;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;
	v++;
	return v;
}
