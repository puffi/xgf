module xgf.rendering.draw2d.texturemanager;

import xgf.graphics;
import xgf.math;
import xgf.rendering.draw2d.texture;
import xgf.util;

// import imageformats;

// @gc
// struct TextureManager
// {
// 	private
// 	{
// 		Texture*[string] mTextures;
// 		Graphics* mGraphics;
// 	}
	
// 	this(Graphics* graphics)
// 	{
// 		mGraphics = graphics;
// 	}
	
// 	auto get(string path)
// 	{
// 		if(auto tex = path in mTextures)
// 			return *tex;
		
// 		auto img = read_image(path, ColFmt.RGBA);
// 		img.pixels.flip(img.w, img.h);
// 		auto handle = mGraphics.createTexture2D(img.pixels, img.w, img.h, Format.rgba8);
		
// 		auto tex = makeRef!Texture(handle, uvec2(img.w, img.h));
// 		mTextures[path] = tex;
// 		return tex;
// 	}
	
// 	private
// 	{
// 		void destroyTexture(Texture* tex)
// 		{
// 			foreach(kv; mTextures.byKeyValue)
// 			{
// 				if(kv.value == tex)
// 				{
// 					mTextures.remove(kv.key);
// 					break;
// 				}
// 			}
			
// 			mGraphics.destroyTexture(tex.handle);
// 			free(tex);
// 		}
// 	}
// }

// private
// {
// 	void flip(ref ubyte[] data, int w, int h)
// 	{
// 		DynamicArray!ubyte arr;
// 		arr.resize(w * 4);
// 		foreach(line; 0..h/2)
// 		{
// 			arr[][] = data[line*w*4..(line+1)*w*4];
// 			data[line*w*4..(line+1)*w*4] = data[(h-line-1)*w*4..(h-line)*w*4];
// 			data[(h-line-1)*w*4..(h-line)*w*4] = arr[][];
// 		}
// 	}
// }
