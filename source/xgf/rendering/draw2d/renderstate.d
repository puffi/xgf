module xgf.rendering.draw2d.renderstate;

import xgf.rendering.draw2d.shader;
// import xgf.rendering.draw2d.uniform;
import xgf.rendering.draw2d.blendmode;
import xgf.rendering.draw2d.renderwindow : DrawableData;

struct RenderState
{
	BlendMode blendMode;
	Shader* shader;
	DrawableData* data;
	
	static @property auto defaultState()
	{
		RenderState state;
		state.blendMode.enabled = true;
		state.blendMode.func = BlendMode.Func(BlendMode.Factor.srcAlpha, BlendMode.Factor.oneMinusSrcAlpha);
		state.blendMode.equation = BlendMode.Equation.add;
		return state;
	}
}
