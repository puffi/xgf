module xgf.rendering.draw2d.texture;

import xgf.graphics.graphics;
import xgf.math;
// import xgf.util;


// alias Texture = xgf.graphics.graphics.TextureHandle;
alias Format = xgf.graphics.graphics.Format;

struct Texture
{
	private
	{
		uvec2 mSize;
		TextureHandle mHandle;
		Format mFormat;
	}

	@property auto size() { return mSize; }
	@property auto handle() { return mHandle; }
	@property auto format() { return mFormat; }
}

// struct Texture
// {
// 	private
// 	{
// 		mixin RefCount!(typeof(this));
		
// 		TextureHandle mHandle;
// 		Filter mMinFilter;
// 		Filter mMagFilter;
// 		Wrap mWrapS;
// 		Wrap mWrapT;
// 		// Wrap mWrapR;
		
// 		uvec2 mSize;
// 	}
	
// 	this(TextureHandle handle, uvec2 size)
// 	{
// 		mHandle = handle;
// 		mSize = size;
// 	}
	
// 	@property auto handle() { return mHandle; }
// 	@property auto isValid() { return mHandle != TextureHandle.invalid; }
// 	@property auto size() { return mSize; }
// 	@property auto minFilter() { return mMinFilter; }
// 	@property void minFilter(Filter f) { mMinFilter = f; }
// 	@property auto magFilter() { return mMagFilter; }
// 	@property void magFilter(Filter f) { mMagFilter = f; }
// 	@property auto wrapS() { return mWrapS; }
// 	@property void wrapS(Wrap w) { mWrapS = w; }
// 	@property auto wrapT() { return mWrapT; }
// 	@property void wrapT(Wrap w) { mWrapT = w; }
// 	// @property auto wrapR() { return mWrapR; }
// 	// @property void wrapR(Wrap w) { mWrapR = w; }
// }
