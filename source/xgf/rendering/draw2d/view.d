module xgf.rendering.draw2d.view;

// import xgf.rendering.draw2d.transform;
import xgf.math;

struct View
{
	private
	{
		vec2 mPosition;
		vec2 mScale = vec2(1);
		Radian!float mRotation;
		vec2 mSize;
		vec4 mViewport = vec4(0, 0, 1, 1);
	}
	
	@property auto position() { return mPosition; }
	@property void position(vec2 p) { mPosition = p; }
	@property auto rotation() { return mRotation; }
	@property void rotation(Radian!float a) { mRotation = a; }
	@property auto scale() { return mScale; }
	@property void scale(vec2 s) { mScale = s; }
	alias zoom = scale;
	@property auto size() { return mSize; }
	@property void size(vec2 s) { mSize = s; }
	@property auto viewport() { return mViewport; }
	@property void viewport(vec4 vp) { mViewport = vp; }
}
