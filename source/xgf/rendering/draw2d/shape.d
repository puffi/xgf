module xgf.rendering.draw2d.shape;

import xgf.math;
import xgf.util;
import xgf.graphics.vertexdeclaration;
import xgf.rendering.draw2d.texture;
import xgf.rendering.draw2d.sampler;

struct Shape
{
	DynamicArray!ShapeVertex vertices;
// 	ubvec4 color = ubvec4(255);
}

struct TexturedShape
{
	DynamicArray!TexturedShapeVertex vertices;
	Texture* texture;
	Sampler sampler;
// 	ubvec4 color = ubvec4(255);
}

struct ShapeVertex
{
	@Attribute(0, Attribute.Type.f32, 2, false)
	vec2 pos;
	
	@Attribute(1, Attribute.Type.u8, 4, true)
	ubvec4 col;
}

struct TexturedShapeVertex
{
	@Attribute(0, Attribute.Type.f32, 2, false)
	vec2 pos;
	
	@Attribute(1, Attribute.Type.u8, 4, true)
	ubvec4 col;
	
	@Attribute(2, Attribute.Type.f32, 2, false)
	vec2 uv;
}

auto createRectangleShape(vec2 size, ubvec4 color = ubvec4(255), float outline = 0, ubvec4 outlineColor = ubvec4(255))
{
	Shape shape;
	if(outline != 0)
	{
		vec4 outlineSize;
		outlineSize.x = 0;
		outlineSize.x = 0;
		outlineSize.w = outline > 0 ? size.x : size.x - outline*2;
		outlineSize.z = outline > 0 ? size.y : size.y - outline*2;
		shape.vertices.pushBack(ShapeVertex(vec2(outlineSize.x, outlineSize.y), outlineColor));
		shape.vertices.pushBack(ShapeVertex(vec2(outlineSize.w, outlineSize.y), outlineColor));
		shape.vertices.pushBack(ShapeVertex(vec2(outlineSize.w, outlineSize.z), outlineColor));
		shape.vertices.pushBack(ShapeVertex(vec2(outlineSize.w, outlineSize.z), outlineColor));
		shape.vertices.pushBack(ShapeVertex(vec2(outlineSize.x, outlineSize.z), outlineColor));
		shape.vertices.pushBack(ShapeVertex(vec2(outlineSize.x, outlineSize.y), outlineColor));
		
		outlineSize.x = outline > 0 ? 0 + outline : 0 - outline;
		outlineSize.y = outline > 0 ? 0 + outline : 0 - outline;
		outlineSize.w = size.x - outline;
		outlineSize.z =  size.y - outline;
		
		shape.vertices.pushBack(ShapeVertex(vec2(outlineSize.x, outlineSize.y), color));
		shape.vertices.pushBack(ShapeVertex(vec2(outlineSize.w, outlineSize.y), color));
		shape.vertices.pushBack(ShapeVertex(vec2(outlineSize.w, outlineSize.z), color));
		shape.vertices.pushBack(ShapeVertex(vec2(outlineSize.w, outlineSize.z), color));
		shape.vertices.pushBack(ShapeVertex(vec2(outlineSize.x, outlineSize.z), color));
		shape.vertices.pushBack(ShapeVertex(vec2(outlineSize.x, outlineSize.y), color));
	}
	else
	{
		shape.vertices.pushBack(ShapeVertex(vec2(0, 0), color));
		shape.vertices.pushBack(ShapeVertex(vec2(size.x, 0), color));
		shape.vertices.pushBack(ShapeVertex(vec2(size.x, size.y), color));
		shape.vertices.pushBack(ShapeVertex(vec2(size.x, size.y), color));
		shape.vertices.pushBack(ShapeVertex(vec2(0, size.y), color));
		shape.vertices.pushBack(ShapeVertex(vec2(0, 0), color));
	}
	
	return shape;
}

auto createCircle(float radius, ubvec4 color = ubvec4(255), float outline = 0, ubvec4 outlineColor = ubvec4(255))
{
	Shape shape;
	if(outline != 0)
	{
		auto outerRadius = outline > 0 ? radius : radius - outline;
		auto innerRadius = outline > 0 ? radius - outline : radius;
		
		shape.vertices.pushBack(ShapeVertex(vec2(-outerRadius, -outerRadius), outlineColor));
		shape.vertices.pushBack(ShapeVertex(vec2(outerRadius, -outerRadius), outlineColor));
		shape.vertices.pushBack(ShapeVertex(vec2(outerRadius, outerRadius), outlineColor));
		shape.vertices.pushBack(ShapeVertex(vec2(outerRadius, outerRadius), outlineColor));
		shape.vertices.pushBack(ShapeVertex(vec2(-outerRadius, outerRadius), outlineColor));
		shape.vertices.pushBack(ShapeVertex(vec2(-outerRadius, -outerRadius), outlineColor));
		
		shape.vertices.pushBack(ShapeVertex(vec2(-innerRadius, -innerRadius), color));
		shape.vertices.pushBack(ShapeVertex(vec2(innerRadius, -innerRadius), color));
		shape.vertices.pushBack(ShapeVertex(vec2(innerRadius, innerRadius), color));
		shape.vertices.pushBack(ShapeVertex(vec2(innerRadius, innerRadius), color));
		shape.vertices.pushBack(ShapeVertex(vec2(-innerRadius, innerRadius), color));
		shape.vertices.pushBack(ShapeVertex(vec2(-innerRadius, -innerRadius), color));
	}
	else
	{
		shape.vertices.pushBack(ShapeVertex(vec2(-radius, -radius), color));
		shape.vertices.pushBack(ShapeVertex(vec2(radius, -radius), color));
		shape.vertices.pushBack(ShapeVertex(vec2(radius, radius), color));
		shape.vertices.pushBack(ShapeVertex(vec2(radius, radius), color));
		shape.vertices.pushBack(ShapeVertex(vec2(-radius, radius), color));
		shape.vertices.pushBack(ShapeVertex(vec2(-radius, -radius), color));
	}
	
	return shape;
}
