module xgf.rendering.draw2d.blendmode;

import xgf.graphics.state;

alias BlendMode = BlendState;
