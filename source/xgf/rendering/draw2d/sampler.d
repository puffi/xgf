module xgf.rendering.draw2d.sampler;

import xgf.graphics.graphics;

alias Filter = xgf.graphics.graphics.Filter;
alias Wrap = xgf.graphics.graphics.Wrap;

struct Sampler
{
	Filter minFilter;
	Filter magFilter;
	Wrap wrapS;
	Wrap wrapT;
}