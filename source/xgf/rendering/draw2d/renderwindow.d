module xgf.rendering.draw2d.renderwindow;

import xgf.core.core;
import xgf.graphics.window;
import xgf.graphics.graphics;
import xgf.math;
import xgf.util.array;
import xgf.util.loadfile;

import xgf.rendering.draw2d.view;
import xgf.rendering.draw2d.sprite;
import xgf.rendering.draw2d.transform;
import xgf.rendering.draw2d.texture;
import xgf.rendering.draw2d.shape;
import xgf.rendering.draw2d.renderstate;

struct RenderWindow
{
	private
	{
		Core* mCore;
		Window* mWindow;
		Graphics* mGraphics;
		
		VertexBufferHandle mSpriteVb;
		IndexBufferHandle mSpriteIb;
		
		static struct ViewProjection
		{
			mat4 view;
			mat4 projection;
			mat4[2] padding;
		}
		
		ViewProjection mViewProjection;
		
		UniformBufferHandle mViewProjectionBuffer;
		
		VertexShaderHandle mDefaultSpriteVs;
		FragmentShaderHandle mDefaultSpriteFs;
		
		VertexShaderHandle mDefaultShapeVs;
		FragmentShaderHandle mDefaultShapeFs;
		
		VertexShaderHandle mDefaultTexturedShapeVs;
		FragmentShaderHandle mDefaultTexturedShapeFs;
		
		View mView;
		
		DynamicBufferHandle mTransformBuffer;
		DynamicBufferHandle mDrawableDataBuffer;
		
		enum MaxDrawables = 2^^16;
		
		uint mCurrentDrawable = 0;
		
		BlendState mBlendState;
	}
	
	this(string title, int width, int height)
	{
		import xgf.util.alloc;
		mCore = alloc!Core();
		mWindow = alloc!Window(core, title, width, height);
		// mGraphics = alloc!Graphics(core); TODO revert
		
		// setup sprite buffers
		SpriteVertex[4] spriteVertices = [
			SpriteVertex(vec2(0, 0)),
			SpriteVertex(vec2(1, 0)),
			SpriteVertex(vec2(1, 1)),
			SpriteVertex(vec2(0, 1))
		];
		uint[6] spriteIndices = [
			0, 1, 2,
			2, 3, 0
		];
		
		mSpriteVb = mGraphics.createVertexBuffer(spriteVertices[]);
		mSpriteIb = mGraphics.createIndexBuffer(spriteIndices[]);
		
		// setup shaders
		mDefaultSpriteVs = mGraphics.createVertexShader(cast(ubyte[])DefaultSpriteVS);
		mDefaultSpriteFs = mGraphics.createFragmentShader(cast(ubyte[])DefaultSpriteFS);
		mDefaultShapeVs = mGraphics.createVertexShader(cast(ubyte[])DefaultShapeVS);
		mDefaultShapeFs = mGraphics.createFragmentShader(cast(ubyte[])DefaultShapeFS);
		mDefaultTexturedShapeVs = mGraphics.createVertexShader(cast(ubyte[])DefaultTexturedShapeVS);
		mDefaultTexturedShapeFs = mGraphics.createFragmentShader(cast(ubyte[])DefaultTexturedShapeFS);
		
		// setup buffers
		import std.range : iota, array;
		uint[1024] instanceIndices = iota(0u,1024u).array;
		auto iib = graphics.createInstanceIndexBuffer(instanceIndices[]);
		mGraphics.setInstanceIndexBuffer(iib);
		
		mViewProjectionBuffer = mGraphics.createUniformBuffer(ViewProjection.sizeof);
		mTransformBuffer = mGraphics.createDynamicBuffer(MaxDrawables * mat4.sizeof);
		mDrawableDataBuffer = mGraphics.createDynamicBuffer(MaxDrawables * DrawableData.sizeof);

		// setup view
		view = defaultView;
		
		// setup blend state
		mBlendState.enabled = true;
		mBlendState.func = BlendState.Func(BlendState.Factor.srcAlpha, BlendState.Factor.oneMinusSrcAlpha);
		mBlendState.equation = BlendState.Equation.add;
		mGraphics.setBlendState(mBlendState);
	}

	~this()
	{
		import xgf.util.alloc : free;

		// destroy buffers
		mGraphics.destroyDynamicBuffer(mDrawableDataBuffer);
		mGraphics.destroyDynamicBuffer(mTransformBuffer);
		mGraphics.destroyUniformBuffer(mViewProjectionBuffer);

		mGraphics.destroyIndexBuffer(mSpriteIb);
		mGraphics.destroyVertexBuffer(mSpriteVb);

		// destroy shaders
		mGraphics.destroyVertexShader(mDefaultSpriteVs);
		mGraphics.destroyVertexShader(mDefaultShapeVs);
		mGraphics.destroyVertexShader(mDefaultTexturedShapeVs);
		mGraphics.destroyFragmentShader(mDefaultSpriteFs);
		mGraphics.destroyFragmentShader(mDefaultShapeFs);
		mGraphics.destroyFragmentShader(mDefaultTexturedShapeFs);

		// destroy subsystems
		free(mGraphics);
		free(mWindow);
		free(mCore);
	}
	
	@property auto core() { return mCore; }
	@property auto window() { return mWindow; }
	@property auto graphics() { return mGraphics; }
	@property auto size() { return mWindow.size; }
	@property auto isOpen() { return mWindow.isOpen; }

	void resize(int width, int height) { mWindow.resize(width, height); }

	void prepare() { mGraphics.beginFrame(); }

	@property auto defaultView()
	{
		View view;
		view.position = vec2(mWindow.size.x / 2.0f, mWindow.size.y / 2.0f);
		view.size = vec2(mWindow.size.x, mWindow.size.y);
		return view;
	}
	
	@property auto view() { return mView; }
	@property void view()(in auto ref View v) // template for auto ref/rvalues
	{
		mView = v;
		auto view = view;
		ivec4 viewport;
		viewport.x = cast(int)(mWindow.size.x * view.viewport.x + 0.5f);
		viewport.y = cast(int)(mWindow.size.y * view.viewport.y + 0.5f);
		viewport.z = cast(int)(mWindow.size.x * view.viewport.z + 0.5f);
		viewport.w = cast(int)(mWindow.size.y * view.viewport.w + 0.5f);
		mGraphics.setViewport(viewport);
		
		mViewProjection.view = view.position.negate.translate.scale(view.scale).rotate(view.rotation);
		mViewProjection.projection = orthoPP(view.size.x / 2.0f, view.size.y / 2.0f, -1, 1);
		
		mGraphics.updateUniformBuffer(mViewProjectionBuffer, (cast(ubyte*)(&mViewProjection))[0..ViewProjection.sizeof]);
	}
	
	void draw()(ref Sprite sprite, in auto ref mat4 transform)
	{
		RenderState state = RenderState.defaultState;
		draw(sprite, transform, state);
	}
	
	void draw()(ref Sprite sprite, in auto ref mat4 transform, in ref RenderState state)
	{
		if(mCurrentDrawable >= MaxDrawables)
		{
			import std.stdio;
			writeln("Reached drawable limit. Clear the render target to reset the count. Current limit: ", MaxDrawables);
			return;
		}
		
		mGraphics.updateDynamicBuffer(mTransformBuffer, (cast(ubyte*)&transform)[0..mat4.sizeof], cast(uint)(mat4.sizeof * mCurrentDrawable));
		
		DrawableData drawableData;
		if(state.data !is null)
		{
			drawableData = *state.data;
		}
		else
		{
			drawableData.v0 = vec4(sprite.color.r / 255.0f, sprite.color.g / 255.0f, sprite.color.b / 255.0f, sprite.color.a / 255.0f);
			drawableData.v1 = vec4(mWindow.size, sprite.texture.size);
			drawableData.v2 = vec4(sprite.texrect.x, sprite.texrect.y, sprite.texrect.z, sprite.texrect.w);
		}

		mGraphics.updateDynamicBuffer(mDrawableDataBuffer, (cast(ubyte*)(&drawableData))[0..DrawableData.sizeof], cast(uint)(DrawableData.sizeof * mCurrentDrawable));
		
		
		mGraphics.bindUniformBuffer(mViewProjectionBuffer, 0);
		mGraphics.bindDynamicUniformBuffer(mTransformBuffer, 1, mat4.sizeof * 1024, cast(uint)(mat4.sizeof * 1024 * (mCurrentDrawable / 1024)));
		mGraphics.bindDynamicUniformBuffer(mDrawableDataBuffer, 2, DrawableData.sizeof * 1024, cast(uint)(DrawableData.sizeof * 1024 * (mCurrentDrawable / 1024)));
		
		auto tex = sprite.texture;
		mGraphics.bindTexture(tex.handle, 0);
		auto sampler = mGraphics.getSampler(sprite.sampler.minFilter, sprite.sampler.magFilter,
			sprite.sampler.wrapS, sprite.sampler.wrapT);
		mGraphics.bindSampler(sampler, 0);
		
		if(state.shader !is null)
		{
			mGraphics.setVertexShader(state.shader.vertexShader);
			mGraphics.setFragmentShader(state.shader.fragmentShader);
		}
		else
		{
			mGraphics.setVertexShader(mDefaultSpriteVs);
			mGraphics.setFragmentShader(mDefaultSpriteFs);
		}
		
		mGraphics.setBlendState(state.blendMode);
		
		mGraphics.setVertexBuffer(mSpriteVb);
		mGraphics.setIndexBuffer(mSpriteIb);
		
		mGraphics.drawIndexed(PrimitiveType.triangles, 1, mCurrentDrawable % 1024);
		
		mCurrentDrawable += 1;
	}
	
	void draw(S)(ref S shape, in auto ref mat4 transform) if(is(S == Shape) || is(S == TexturedShape))
	{
		RenderState state = RenderState.defaultState;
		draw(shape, transform, state);
	}

	void draw(S)(ref S shape, in auto ref mat4 transform, in ref RenderState state) if(is(S == Shape) || is(S == TexturedShape))
	{
		if(mCurrentDrawable >= MaxDrawables)
		{
			import std.stdio;
			writeln("Reached drawable limit. Clear the render target to reset the count. Current limit: ", MaxDrawables);
			return;
		}
		
		mGraphics.updateDynamicBuffer(mTransformBuffer, (cast(ubyte*)&transform)[0..mat4.sizeof], cast(uint)(mat4.sizeof * mCurrentDrawable));
		
		DrawableData drawableData;
		if(state.data !is null)
			drawableData = *state.data;

		mGraphics.updateDynamicBuffer(mDrawableDataBuffer, (cast(ubyte*)(&drawableData))[0..DrawableData.sizeof], cast(uint)(DrawableData.sizeof * mCurrentDrawable));
		
		
		mGraphics.bindUniformBuffer(mViewProjectionBuffer, 0);
		mGraphics.bindDynamicUniformBuffer(mTransformBuffer, 1, mat4.sizeof * 1024, cast(uint)(mat4.sizeof * 1024 * (mCurrentDrawable / 1024)));
		mGraphics.bindDynamicUniformBuffer(mDrawableDataBuffer, 2, DrawableData.sizeof * 1024, cast(uint)(DrawableData.sizeof * 1024 * (mCurrentDrawable / 1024)));
		
		static if(is(S == TexturedShape))
		{
			mGraphics.bindTexture(shape.texture.handle, 0);
			auto sampler = mGraphics.getSampler(shape.sampler.minFilter, shape.sampler.magFilter,
				shape.sampler.wrapS, shape.sampler.wrapT);
			mGraphics.bindSampler(sampler, 0);
		}
		
		if(state.shader !is null)
		{
			mGraphics.setVertexShader(state.shader.vertexShader);
			mGraphics.setFragmentShader(state.shader.fragmentShader);
		}
		else
		{
			static if(is(S == Shape))
			{
				mGraphics.setVertexShader(mDefaultShapeVs);
				mGraphics.setFragmentShader(mDefaultShapeFs);
			}
			else
			{
				mGraphics.setVertexShader(mDefaultTexturedShapeVs);
				mGraphics.setFragmentShader(mDefaultTexturedShapeFs);
			}
		}
		
		mGraphics.setBlendState(state.blendMode);
		
		auto vb = mGraphics.createVertexBuffer(shape.vertices[]);
		mGraphics.setVertexBuffer(vb);
		
		mGraphics.draw(PrimitiveType.triangles, 1, mCurrentDrawable % 1024);
		
		mGraphics.destroyVertexBuffer(vb);
		
		mCurrentDrawable += 1;
	}
	
	void beginFrame()
	{
		mGraphics.beginFrame();
	}
	
	void endFrame()
	{
		mGraphics.endFrame();
	}
	
	void clear(vec4 color = vec4(0, 0, 0, 1))
	{
		mGraphics.clearRenderTarget(DefaultRenderTarget, ClearFlag.color | ClearFlag.depth | ClearFlag.stencil, color);
		mCurrentDrawable = 0;
	}

	void display()
	{
		mWindow.display();
	}

	void pollEvents()
	{
		mWindow.pollEvents();
		mGraphics.endFrame();
	}

	Texture* createTexture(uint width, uint height, Format fmt, ubyte[] data)
	{
		import xgf.util.alloc;
		return alloc!Texture(uvec2(width, height), mGraphics.createTexture2D(data, width, height, fmt), fmt);
	}

	void destroyTexture(Texture* tex)
	{
		import xgf.util.alloc;
		mGraphics.destroyTexture(tex.handle);
		free(tex);
	}

	void updateTexture(Texture* tex, uint width, uint height, uint x, uint y, ubyte[] data)
	{
		mGraphics.updateTexture2D(tex.handle, data, width, height, x, y);
	}
}

struct DrawableData
{
	vec4 v0;
	vec4 v1;
	vec4 v2;
	vec4 v3;
}

private
{
	struct SpriteVertex
	{
		@Attribute(0, Attribute.Type.f32, 2, false)
		vec2 pos;
	}
	
	enum DefaultSpriteVS = "#version 330 core

		#extension GL_ARB_separate_shader_objects: require
		#extension GL_ARB_shading_language_420pack: require
		#extension GL_ARB_explicit_uniform_location: require

		layout(location = 0) in vec2 pos;
		layout(location = 15) in uint idx;

		layout(location = 0) out vec4 o_col;
		layout(location = 1) out vec2 o_uv;

		out gl_PerVertex
		{
			vec4 gl_Position;
		};

		layout(binding = 0, std140) uniform VP
		{
			mat4 view;
			mat4 proj;
			mat4 unused1;
			mat4 unused2;
		} vp;

		layout(binding = 1, std140) uniform Transform
		{
			mat4 transform[1024];
		} transform;

		struct DrawableData
		{
			vec4 color;
			vec4 winTexSize;
			vec4 texrect;
			vec4 padding;
		};

		layout(binding = 2, std140) uniform DrawableDataBuffer
		{
			DrawableData data[1024];
		} ddb;

		void main()
		{
			DrawableData dd = ddb.data[idx];
			vec4 texrect = dd.texrect;
			vec4 position = vec4(pos * vec2(texrect.z, texrect.w), 0, 1);
			gl_Position = vp.proj * vp.view * transform.transform[idx] * position;
			o_col = dd.color;
			o_uv = texrect.xy / dd.winTexSize.zw + (pos * texrect.zw / dd.winTexSize.zw);
		}";
	
	enum DefaultSpriteFS = "#version 330 core

		#extension GL_ARB_separate_shader_objects: require
		#extension GL_ARB_shading_language_420pack: require
		#extension GL_ARB_explicit_uniform_location: require

		layout(location = 0) in vec4 col;
		layout(location = 1) in vec2 uv;

		out vec4 color;

		layout(binding = 0) uniform sampler2D tex0;

		void main()
		{
			color = texture(tex0, uv) * col;
		}";
	
	enum DefaultShapeVS = "#version 330 core

		#extension GL_ARB_separate_shader_objects: require
		#extension GL_ARB_shading_language_420pack: require
		#extension GL_ARB_explicit_uniform_location: require

		layout(location = 0) in vec2 pos;
		layout(location = 1) in vec4 col;
		layout(location = 15) in uint idx;

		layout(location = 0) out vec4 o_col;

		out gl_PerVertex
		{
			vec4 gl_Position;
		};

		layout(binding = 0, std140) uniform VP
		{
			mat4 view;
			mat4 proj;
			mat4 unused1;
			mat4 unused2;
		} vp;

		layout(binding = 1, std140) uniform Transform
		{
			mat4 transform[1024];
		} transform;

		void main()
		{
			vec4 position = vec4(pos, 0, 1);
			gl_Position = vp.proj * vp.view * transform.transform[idx] * position;
			
			o_col = col;
		}";
	
	enum DefaultShapeFS = "#version 330 core

		#extension GL_ARB_separate_shader_objects: require
		#extension GL_ARB_shading_language_420pack: require
		#extension GL_ARB_explicit_uniform_location: require

		layout(location = 0) in vec4 col;

		out vec4 color;

		void main()
		{
			color = col;
		}
		";
	
	enum DefaultTexturedShapeVS = "#version 330 core

		#extension GL_ARB_separate_shader_objects: require
		#extension GL_ARB_shading_language_420pack: require
		#extension GL_ARB_explicit_uniform_location: require

		layout(location = 0) in vec2 pos;
		layout(location = 1) in vec4 col;
		layout(location = 2) in vec2 uv;
		layout(location = 15) in uint idx;

		layout(location = 0) out vec4 o_col;
		layout(location = 1) out vec2 o_uv;

		out gl_PerVertex
		{
			vec4 gl_Position;
		};

		layout(binding = 0, std140) uniform VP
		{
			mat4 view;
			mat4 proj;
			mat4 unused1;
			mat4 unused2;
		} vp;

		layout(binding = 1, std140) uniform Transform
		{
			mat4 transform[1024];
		} transform;

		void main()
		{
			vec4 position = vec4(pos, 0, 1);
			gl_Position = vp.proj * vp.view * transform.transform[idx] * position;

			o_col = col;
			o_uv = uv;
		}";
	
	enum DefaultTexturedShapeFS = "#version 330 core

		#extension GL_ARB_separate_shader_objects: require
		#extension GL_ARB_shading_language_420pack: require
		#extension GL_ARB_explicit_uniform_location: require

		layout(location = 0) in vec4 col;
		layout(location = 1) in vec2 uv;

		out vec4 color;

		layout(binding = 0) uniform sampler2D tex0;

		void main()
		{
			color = texture(tex0, uv) * col;
		}";
}
