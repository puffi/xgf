module xgf.rendering.draw2d.transform;

import xgf.math;

struct Transform
{
	vec2 position;
	vec2 scale = vec2(1);
	Radian!float rotation;
}
