module xgf.rendering.draw2d.shader;

import xgf.graphics;
import xgf.util;

struct Shader
{
	private
	{
		Graphics* mGraphics;
		VertexShaderHandle mVertexShader;
		FragmentShaderHandle mFragmentShader;
	}
	
	this(Graphics* graphics)
	{
		mGraphics = graphics;
	}
	
	~this()
	{
		if(mVertexShader != VertexShaderHandle.invalid)
			mGraphics.destroyVertexShader(mVertexShader);
		if(mFragmentShader != FragmentShaderHandle.invalid)
			mGraphics.destroyFragmentShader(mFragmentShader);
	}
	
	bool loadFromFile(in string vs, in string fs)
	{
		DynamicArray!ubyte arr;
		if(!loadFile(vs, arr))
			return false;
		mVertexShader = mGraphics.createVertexShader(arr[]);
		if(!loadFile(fs, arr))
			return false;
		mFragmentShader = mGraphics.createFragmentShader(arr[]);
		
		return true;
	}

	bool loadFromMemory(in ubyte[] vs, in ubyte[] fs)
	{
		mVertexShader = mGraphics.createVertexShader(vs);
		mFragmentShader = mGraphics.createFragmentShader(fs);
		return true;
	}
	
	@property auto vertexShader() const { return mVertexShader; }
	@property auto fragmentShader() const { return mFragmentShader; }
	@property auto isValid() const { return mVertexShader != VertexShaderHandle.invalid && mFragmentShader != FragmentShaderHandle.invalid; }
}
