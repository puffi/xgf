module xgf.rendering.draw2d.sprite;

import xgf.math;
import xgf.rendering.draw2d.texture;
import xgf.rendering.draw2d.sampler;
import xgf.util;

struct Sprite
{
	ubvec4 color = ubvec4(255);
	Texture* texture;
	Sampler sampler;
	ivec4 texrect;
}
