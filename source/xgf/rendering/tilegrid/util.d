module xgf.rendering.tilegrid.util;

import xgf.rendering.tilegrid.font;
import xgf.rendering.tilegrid.tileindex;
import xgf.rendering.tilegrid.tileset;

import xgf.util;

void padGlyphToTile(Font* font, Glyph glyph, ubyte[] bitmap, ubyte[] tile)
{
	auto paddingX = (TileSize - glyph.width + 1) / 2;
	auto baseline = font.descent;
	auto offsetY = cast(int)(cast(int)(-baseline) - glyph.yMax);
	
	foreach(y; 0..glyph.height)
	{
		tile[paddingX + (y+offsetY) * TileSize..paddingX + (y+offsetY) * TileSize + glyph.width] = bitmap[y * glyph.width..(y+1) * glyph.width];
	}
}

void flipY(uint width, ubyte[] data)
{
	Array!ubyte arr = Array!ubyte(width);
	
	foreach(y; 0..(data.length / width)/2)
	{
		auto flippedPos = (data.length / width) - y - 1;
		arr[][] = data[flippedPos * width .. (flippedPos+1) * width];
		data[flippedPos * width .. (flippedPos+1) * width] = data[y * width .. (y+1) * width];
		data[y * width .. (y+1) * width] = arr[][];
	}
}

void padTo4Bytes(ubyte[] data, ubyte[] dst)
{
	assert(dst.length == data.length*4);
	
	foreach(i, d; data)
	{
		dst[i*4 + 0] = 255;
		dst[i*4 + 1] = 255;
		dst[i*4 + 2] = 255;
		dst[i*4 + 3] = d;
	}
}

auto registerGlyphTile(TileSet* set, Font* font, dchar c)
{
	DynamicArray!ubyte glyphBitmap;
	auto glyph = font.getGlyph(c, glyphBitmap);
	
	Array!ubyte tile = Array!ubyte(TileSize * TileSize);
	flipY(glyph.width, glyphBitmap[]);
	padGlyphToTile(font, glyph, glyphBitmap[], tile[]);
	Array!ubyte rgba = Array!ubyte(TileSize * TileSize * 4);
	padTo4Bytes(tile[], rgba[]);
	
	import std.conv : to;
	return set.registerTile(c.to!string, rgba[]);
}
