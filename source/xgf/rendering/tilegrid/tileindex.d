module xgf.rendering.tilegrid.tileindex;

struct TileIndex
{
	byte x = -1;
	byte y = -1;
	ubyte width = 0;
	ubyte height = 0;
	
	@property auto isValid() { return x >= 0 && y >= 0 && width > 0 && height > 0; }
}
