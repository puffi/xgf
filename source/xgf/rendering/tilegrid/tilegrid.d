module xgf.rendering.tilegrid.tilegrid;

import xgf.rendering.tilegrid.tileindex;
import xgf.rendering.tilegrid.tileset;
import xgf.graphics;
import xgf.util;
import xgf.math;

struct TileGrid
{
	private
	{
		Array!Tile mTiles;
		uint mWidth;
		uint mHeight;
		
		Graphics* mGraphics;
		TileSet* mTileSet;
		
		VertexBufferHandle mBgVb;
		IndexBufferHandle mBgIb;
		VertexBufferHandle mFgVb;
		IndexBufferHandle mFgIb;
		
		
		bool mOutOfDate;
	}
	
	this(Graphics* graphics, TileSet* tileSet, uint width, uint height)
	{
		mGraphics = graphics;
		mTileSet = tileSet;
		mWidth = width;
		mHeight = height;
		
		mTiles = Array!Tile(mWidth * mHeight);
		
		mOutOfDate = true;
	}
	
	void resize(uint width, uint height)
	{
		mWidth = width;
		mHeight = height;

		mTiles = Array!Tile(mWidth * mHeight);

		if(mBgVb != VertexBufferHandle.invalid)
		{
			mGraphics.destroyVertexBuffer(mBgVb);
			mGraphics.destroyIndexBuffer(mBgIb);
			mGraphics.destroyVertexBuffer(mFgVb);
			mGraphics.destroyIndexBuffer(mFgIb);
		}
		
		mOutOfDate = true;
	}

	void clear()
	{
		mTiles[][] = Tile.init;
		mOutOfDate = true;
	}
	
	void clear(uint x, uint y)
	{
		mTiles[x + y * mWidth] = Tile.init;
		mOutOfDate = true;
	}
	
	void set(uint x, uint y, TileIndex idx, ubvec4 fgColor, TileIndex bgIdx, ubvec4 bgColor)
	{
		if(!idx.isValid)
		{
			clear(x, y);
			return;
		}
		foreach(yOff; 0..idx.height)
		{
			if(y + yOff >= mHeight)
				continue;
			foreach(xOff; 0..idx.width)
			{
				if(x + xOff >= mWidth)
					continue;
				
				mTiles[x + xOff + (y + yOff) * mWidth] = Tile(bgColor, fgColor, bvec2(cast(byte)(idx.x + xOff), cast(byte)(idx.y + yOff)), bvec2(bgIdx.x, bgIdx.y));
				mOutOfDate = true;
			}
		}
// 		mTiles[x + y * mWidth] = Tile(bgColor, fgColor, xIdx, yIdx);
// 		mOutOfDate = true;
	}
	
	void set(uint x, uint y, string tileName, ubvec4 fgColor, ubvec4 bgColor = ubvec4(0))
	{
		auto tile = mTileSet.getTile(tileName);
		auto bgTile = mTileSet.getTile("empty");
		set(x, y, tile, fgColor, bgTile, bgColor);
	}
	
	void set(uint x, uint y, string tileName, ubvec4 fgColor, string bgTileName, ubvec4 bgColor)
	{
		auto tile = mTileSet.getTile(tileName);
		auto bgTile = mTileSet.getTile(bgTileName);
		set(x, y, tile, fgColor, bgTile, bgColor);
	}
	
	@property auto bgVb() { if(mOutOfDate) update(); return mBgVb; }
	@property auto bgIb() { if(mOutOfDate) update(); return mBgIb; }
	@property auto fgVb() { if(mOutOfDate) update(); return mFgVb; }
	@property auto fgIb() { if(mOutOfDate) update(); return mFgIb; }
	@property auto tileSet() { return mTileSet; }
	@property auto texture() { return mTileSet.texture; }
	@property auto sampler() { return mTileSet.sampler; }
	@property auto size() { return ivec2(mWidth, mHeight); }

	private
	{
		void update()
		{
			if(mBgVb != VertexBufferHandle.invalid)
				mGraphics.destroyVertexBuffer(mBgVb);
			if(mBgIb != IndexBufferHandle.invalid)
				mGraphics.destroyIndexBuffer(mBgIb);
			if(mFgVb != VertexBufferHandle.invalid)
				mGraphics.destroyVertexBuffer(mFgVb);
			if(mFgIb != IndexBufferHandle.invalid)
				mGraphics.destroyIndexBuffer(mFgIb);
			
			DynamicArray!TileVertex bgVertices;
			DynamicArray!uint bgIndices;
			DynamicArray!TileVertex fgVertices;
			DynamicArray!uint fgIndices;
			
			uint bgIdx;
			uint fgIdx;
			
			foreach(y; 0..mHeight)
			{
				foreach(x; 0..mWidth)
				{
					auto tile = mTiles[x + y * mWidth];
					if(tile.fgIdx.x < 0 || tile.fgIdx.y < 0)
						continue;
					
					if(tile.bgColor.a != 0)
					{
// 						auto emptyIdx = mTileSet.emptyTile;
						auto emptyIdx = tile.bgIdx;
						bgVertices.pushBack(TileVertex(vec2(x * TileSize, y * TileSize), tile.bgColor, vec2((emptyIdx.x * TileSize + 0.5f) / 1024.0f, (emptyIdx.y * TileSize + 0.5f) / 1024.0f)));
						bgVertices.pushBack(TileVertex(vec2(x * TileSize + TileSize, y * TileSize), tile.bgColor, vec2((emptyIdx.x * TileSize + TileSize - 0.5f) / 1024.0f, (emptyIdx.y * TileSize + 0.5f) / 1024.0f)));
						bgVertices.pushBack(TileVertex(vec2(x * TileSize + TileSize, y * TileSize + TileSize), tile.bgColor, vec2((emptyIdx.x * TileSize + TileSize - 0.5f) / 1024.0f, (emptyIdx.y * TileSize + TileSize - 0.5f) / 1024.0f)));
						bgVertices.pushBack(TileVertex(vec2(x * TileSize, y * TileSize + TileSize), tile.bgColor, vec2((emptyIdx.x * TileSize + 0.5f) / 1024.0f, (emptyIdx.y * TileSize + TileSize - 0.5f) / 1024.0f)));
						
						bgIndices.pushBack(bgIdx + 0);
						bgIndices.pushBack(bgIdx + 1);
						bgIndices.pushBack(bgIdx + 2);
						bgIndices.pushBack(bgIdx + 2);
						bgIndices.pushBack(bgIdx + 3);
						bgIndices.pushBack(bgIdx + 0);
						
						bgIdx += 4;
					}
					
					if(tile.fgColor.a != 0)
					{
						fgVertices.pushBack(TileVertex(vec2(x * TileSize, y * TileSize), tile.fgColor, vec2((tile.fgIdx.x * TileSize + 0.5f) / 1024.0f, (tile.fgIdx.y * TileSize + 0.5f) / 1024.0f)));
						fgVertices.pushBack(TileVertex(vec2(x * TileSize + TileSize, y * TileSize), tile.fgColor, vec2((tile.fgIdx.x * TileSize + TileSize - 0.5f) / 1024.0f, (tile.fgIdx.y * TileSize + 0.5f) / 1024.0f)));
						fgVertices.pushBack(TileVertex(vec2(x * TileSize + TileSize, y * TileSize + TileSize), tile.fgColor, vec2((tile.fgIdx.x * TileSize + TileSize - 0.5f) / 1024.0f, (tile.fgIdx.y * TileSize + TileSize - 0.5f) / 1024.0f)));
						fgVertices.pushBack(TileVertex(vec2(x * TileSize, y * TileSize + TileSize), tile.fgColor, vec2((tile.fgIdx.x * TileSize + 0.5f) / 1024.0f, (tile.fgIdx.y * TileSize + TileSize - 0.5f) / 1024.0f)));
						
						fgIndices.pushBack(fgIdx + 0);
						fgIndices.pushBack(fgIdx + 1);
						fgIndices.pushBack(fgIdx + 2);
						fgIndices.pushBack(fgIdx + 2);
						fgIndices.pushBack(fgIdx + 3);
						fgIndices.pushBack(fgIdx + 0);
						
						fgIdx += 4;
					}
				}
			}
			
			mBgVb = mGraphics.createVertexBuffer(bgVertices[]);
			mBgIb = mGraphics.createIndexBuffer(bgIndices[]);
			mFgVb = mGraphics.createVertexBuffer(fgVertices[]);
			mFgIb = mGraphics.createIndexBuffer(fgIndices[]);
			
			mOutOfDate = false;
		}
	}
}

private
{
	struct Tile
	{
		ubvec4 bgColor;
		ubvec4 fgColor;
		
		bvec2 fgIdx = bvec2(-1, -1);
		bvec2 bgIdx = bvec2(-1, -1);
	}
	
	struct TileVertex
	{
		@Attribute(0, Attribute.Type.f32, 2, false)
		vec2 pos;
		
		@Attribute(1, Attribute.Type.u8, 4, true)
		ubvec4 color;
		
		@Attribute(2, Attribute.Type.f32, 2, false)
		vec2 uv;
	}
}
