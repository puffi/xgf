module xgf.rendering.tilegrid.font;

import stb.truetype;
import xgf.util;

struct Font
{
	private
	{
		string mName;
		stbtt_fontinfo mFontInfo;
		uint mSize;
		
		float mScale;
		float mAscent;
		float mDescent;
		float mLinegap;
		
		DynamicArray!ubyte mFontData;
	}
	
	this(in string name, uint size)
	{
		mName = name;
		mSize = size;
		
		loadFile(mName, mFontData);
		
		stbtt_InitFont(&mFontInfo, mFontData.ptr, stbtt_GetFontOffsetForIndex(mFontData.ptr, 0));
		
		mScale = stbtt_ScaleForPixelHeight(&mFontInfo, mSize);
		int ascent;
		int descent;
		int linegap;
		stbtt_GetFontVMetrics(&mFontInfo, &ascent, &descent, &linegap);
		mAscent = ascent * mScale;
		mDescent = descent * mScale;
		mLinegap = linegap * mScale;
	}
	
	auto getGlyph(dchar c, ref DynamicArray!ubyte arr)
	{
		auto index = stbtt_FindGlyphIndex(&mFontInfo, c);
		int advance;
		int leftBearing;
		stbtt_GetGlyphHMetrics(&mFontInfo, index, &advance, &leftBearing);
		
		int xMin;
		int xMax;
		int yMin;
		int yMax;
		stbtt_GetGlyphBitmapBox(&mFontInfo, index, mScale, mScale, &xMin, &yMin, &xMax, &yMax);
		
		auto width = xMax - xMin;
		auto height = yMax - yMin;
		
		Glyph glyph;
		glyph.advance = advance * mScale;
		glyph.leftBearing = leftBearing * mScale;
		glyph.xMin = xMin;
		glyph.yMin = yMin;
		glyph.xMax = xMax;
		glyph.yMax = yMax;
		glyph.width = width;
		glyph.height = height;
		
		arr.resize(width * height);
		stbtt_MakeGlyphBitmap(&mFontInfo, arr.ptr, width, height, width, mScale, mScale, index);
		
// 		foreach(h; 0..mSize)
// 		{
// 			foreach(w; 0..mSize)
// 			{
// 				auto flippedHeight = mSize - 1 - h;
// 				auto texdataIndex = (w + idxX * mSize.roundToPow2) + (flippedHeight + idxY * mSize.roundToPow2) * MaxGlyphsPerRow * mSize.roundToPow2;
// 				mData[texdataIndex] = data[w + h * mSize];
// 			}
// 		}
		
// 		mOutOfDate = true;
		return glyph;
	}
	
	@property auto ascent() { return mAscent; }
	@property auto descent() { return mDescent; }
}

struct Glyph
{
	float advance;
	float leftBearing;
	int xMin;
	int yMin;
	int xMax;
	int yMax;
	int width;
	int height;
}
