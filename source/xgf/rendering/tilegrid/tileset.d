module xgf.rendering.tilegrid.tileset;

import xgf.rendering.tilegrid.tileindex;
import xgf.graphics;
import xgf.util;

enum TileSetSize = 1024;
enum TileSize = 16;
enum TileSetGridSize = TileSetSize / TileSize;

// pragma(msg, "TileSetGridSize: ", TileSetGridSize);

@gc
struct TileSet
{
	private
	{
		bool[TileSetGridSize][TileSetGridSize] mTileInUse;
		ubyte[TileSetSize*TileSetSize*4] mTextureData;
		TileIndex[string] mTiles;
		bool mTextureOutOfDate;
		
		Graphics* mGraphics;
		TextureHandle mTexture;
		SamplerHandle mSampler;
		TileIndex mEmptyTileIndex;
	}
	
	this(Graphics* graphics)
	{
		mGraphics = graphics;
		
		ubyte[TileSize*TileSize*4] emptyTile = 255;
		mEmptyTileIndex = registerTile("empty", emptyTile[]);
		mTexture = mGraphics.createTexture2D(mTextureData[], TileSetSize, TileSetSize, Format.rgba8);
		mSampler = mGraphics.getSampler(Filter.nearest, Filter.nearest);
		
		mTextureOutOfDate = false;
	}
	
	TileIndex registerTile(string name, ubyte[] data, uint tilesX = 1, uint tilesY = 1)
	{
		assert(data.length == TileSize*TileSize * tilesX * tilesY * 4);
		import std.stdio;

		foreach(byte y, ref arr; mTileInUse[])
		{
			foreach(byte x, ref val; arr[])
			{
				if(val)
					continue;
				
				if(x + tilesX >= TileSetGridSize)
					continue;
				if(y + tilesY >= TileSetGridSize)
					continue;
				
				foreach(row; y..y+tilesY)
				{
					foreach(col; x..x+tilesX)
					{
						if(mTileInUse[row][col])
							goto CONTINUE;
					}
				}
				
				foreach(row; y..y+tilesY)
				{
					foreach(col; x..x+tilesX)
					{
						mTileInUse[row][col] = true;
					}
				}
				
				updateData(x, y, data, tilesX, tilesY);
				return registerTile(name, TileIndex(x, y, cast(ubyte)(tilesX), cast(ubyte)(tilesY)));
				
				CONTINUE: continue;
			}
		}
		
		return TileIndex(-1, -1, 0, 0);
	}
	
	bool contains(string name)
	{
		return !!(name in mTiles);
	}

	auto getTile(string name)
	{
		import std.stdio;
		if(!(name in mTiles))
		{
			writeln("Name: ", name, " not in mTiles.");
			writeln("Code: ", (cast(ubyte*)name.ptr)[0..name.length]);
			assert(false);
		}
		return mTiles[name];
	}
	
	private
	{
		void updateData(byte x, byte y, ubyte[] data, uint tilesX = 1, uint tilesY = 1)
		{
			import std.stdio;
			foreach(row; 0..TileSize * tilesY)
			{
				mTextureData[x * TileSize * 4 + y * TileSize * TileSetSize * 4 + row * TileSetSize * 4 .. (x * TileSize * 4 + tilesX * TileSize * 4) + y * TileSize * TileSetSize * 4 + row * TileSetSize * 4] = 
					data[row * TileSize * 4 .. (row + 1) * TileSize * 4];
				// mTextureData[x * TileSize * 4 + y * TileSetSize * TileSize * 4 + row * TileSetSize * 4.. (x * TileSize * 4 + TileSize * 4) + y * TileSetSize * TileSize * 4 + row * TileSetSize * 4] = data[row * TileSize * 4..(row+1) * TileSize * 4];
			}
			
			mTextureOutOfDate = true;
		}
		
		void update()
		{
			mGraphics.updateTexture2D(mTexture, mTextureData[], TileSetSize, TileSetSize);
			mTextureOutOfDate = false;
		}
		
		auto registerTile(string name, TileIndex idx)
		{
			mTiles[name] = idx;
			return idx;
		}
	}
	
	@property auto texture() { if(mTextureOutOfDate) update(); return mTexture; }
	@property auto sampler() { return mSampler; }
	@property auto emptyTile() { return mEmptyTileIndex; }
}
