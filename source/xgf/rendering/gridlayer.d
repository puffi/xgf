module xgf.rendering.gridlayer;

import xgf.util.array;

import xgf.math : uvec2, ubvec4;

struct GridLayer
{
	private
	{
		Array!Tile mTiles;
		uvec2 mSize;
	}
	
	this(uvec2 size)
	{
		mSize = size;
		
		mTiles = Array!Tile(mSize.x * mSize.y);
	}
	
	@property auto tiles() { return mTiles[]; }
}

struct Tile
{
	enum Type
	{
		none,
		character,
		custom
	}
	
	Type type;
	ubvec4 background;
	ubvec4 foreground;
	union
	{
		dchar character;
		uint custom;
	}
}
