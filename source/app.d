import std.stdio;

import xgf.application.text;

import xgf.core.core;

import xgf.ecs.ecs;

import xgf.graphics.window;
import xgf.graphics;
import xgf.graphics.state;

import xgf.math;

import xgf.rendering.font;
import xgf.rendering.tilegrid;

import xgf.util.alloc;
import xgf.util.array;
import xgf.util.timer;
import xgf.util.time;
import xgf.util.log;

import xgf.graphics.glad.gl.all;
import derelict.sdl2.sdl;

// alias vec4 = Vector!(float, 4);

void main()
{
	auto core = alloc!Core;
	auto win = alloc!Window(core, "xgf", 1024, 768);
	// win.vsync = false;
	auto ecs = alloc!ECS(core);

	auto pd = win.platform;
	Log.info(pd.xlib);
	auto graphics = alloc!Graphics(core, &pd);
	
	vec4 bgColor = vec4(0, 0, 0, 1);
	
// 	auto vd = graphics.getVertexDeclaration!Vertex;
// 	writeln(vd);
// 	
// 	Vertex[4] vertices = [
// 		Vertex(vec2(0, 0), vec2(0, 0)),
// 		Vertex(vec2(1, 0), vec2(1, 0)),
// 		Vertex(vec2(1, 1), vec2(1, 1)),
// 		Vertex(vec2(0, 1), vec2(0, 1))
// 	];
// // 	Vertex[4] vertices = [
// // 		Vertex(vec2(0, 0), Vector!(ubyte, 4)(63, 127, 255, 255)),
// // 		Vertex(vec2(1, 0), Vector!(ubyte, 4)(127, 127, 255, 255)),
// // 		Vertex(vec2(1, 1), Vector!(ubyte, 4)(127, 63, 255, 255)),
// // 		Vertex(vec2(0, 1), Vector!(ubyte, 4)(63, 63, 255, 255))
// // 	];
// 	
// 	uint[6] indices = [
// 		0, 1, 2,
// 		2, 3, 0
// 	];
// 	
// 	import std.range : iota, array;
// 	uint[1024] instanceIndices = iota(0u,1024u).array;
// 	
// 	auto vb = graphics.createVertexBuffer(vertices[]);
// 	auto ib = graphics.createIndexBuffer(indices[]);
// 	writeln(vb);
// 	writeln(ib);
// 	
// 	auto e0 = ecs.create();
// 	auto r0 = e0.add!Renderable(vb, ib);
// 	auto e1 = ecs.create();
// 	auto r1 = e1.add!Renderable(vb, ib);
// 	auto e2 = ecs.create();
// 	auto r2 = e2.add!Renderable(vb, ib);
// 	auto e3 = ecs.create();
// 	auto r3 = e3.add!Renderable(vb, ib);
// 	e0.add!TestComp();
// 	e1.add!TestComp();
// 	e2.add!TestComp();
// 	e3.add!TestComp();
// 	
// 	auto iib = graphics.createInstanceIndexBuffer(instanceIndices[]);
	
// 	graphics.setVertexBuffer(vb);
// 	graphics.setIndexBuffer(ib);
// 	graphics.setInstanceIndexBuffer(iib);
// 	
// 	DynamicArray!ubyte arr;
// 	loadFile("resources/shaders/xgf/start.vs.glsl", arr);
// 	auto vs = graphics.createVertexShader(arr[]);
// 	loadFile("resources/shaders/xgf/start.fs.glsl", arr);
// 	auto fs = graphics.createFragmentShader(arr[]);
// 	
// 	graphics.setVertexShader(vs);
// 	graphics.setFragmentShader(fs);
// 	
// 	auto camUb = graphics.createUniformBuffer(mat4.sizeof * 4);
// 	mat4[4] camMats = [
// // 		ortho(-512.0f, 512.0f, -384.0f, 384.0f, -1, 1),
// // 		ortho(384.0f, 1024.0f/768.0f, -1, 1),
// 		orthoPP(1024.0f/2, 768.0f/2, -1, 1),
// // 		mat4.identity,
// 		vec2(-1024.0f/2, -768.0f/2).translate,
// 		mat4.identity,
// 		mat4.identity
// 	];
// 	graphics.setViewport(ivec4(0, 0, 1024, 768));
// 	
// 	graphics.updateUniformBuffer(camUb, cast(ubyte[])camMats[]);
// 	
// 	graphics.bindUniformBuffer(camUb, 0);
// 	
// 	mat4[4096] transforms;
// 	foreach(y; 0..64)
// 	{
// 		foreach(x; 0..64)
// 		{
// 			transforms[x + y * 64] = vec3(x, y, 0).translate;
// 		}
// 	}
// 	auto transformDb = graphics.createDynamicBuffer(transforms.sizeof);
// 	
// 	graphics.updateDynamicBuffer(transformDb, cast(ubyte[])transforms[]);
// // 	graphics.bindDynamicUniformBuffer(transformDb, 1, mat4.sizeof * 1024);
// 	
// 	ubyte[4 * 4] texData = [
// 		255, 0, 0, 255,
// 		0, 255, 0, 255,
// 		0, 0, 255, 255,
// 		127, 127, 127, 255
// 	];
// 	
// 	auto texHandle = graphics.createTexture2D(texData[], 2, 2, Format.rgba8);
// 	auto samplerHandle = graphics.getSampler(Filter.nearest, Filter.nearest);
// 	
// 	graphics.bindTexture(texHandle, 0);
// 	graphics.bindSampler(samplerHandle, 0);
	
// 	auto fontfile = "/usr/share/fonts/TTF/DejaVuSansMono-Bold.ttf";
// 	auto fontfile = "DejaVuSansMono-Bold.ttf";
// 	loadFile(fontfile, arr);
// 	auto font = alloc!(xgf.rendering.font.Font)(arr[], 16, graphics);
// 	auto text = alloc!Text(font, graphics);
// 	text.setText("Hello, World! ", 3, " @ł€¶ŧ←↓→abcdefghijklmnopq");
// 	
// 	loadFile("resources/shaders/text.vs.glsl", arr);
// 	auto textVs = graphics.createVertexShader(arr[]);
// 	loadFile("resources/shaders/text.fs.glsl", arr);
// 	auto textFs = graphics.createFragmentShader(arr[]);
// 	
// 	graphics.setVertexShader(textVs);
// 	graphics.setFragmentShader(textFs);
// 	
// 	graphics.setBlendState(font.blendState);
// 	
// 	graphics.bindTexture(font.texture, 0);
// 	graphics.bindSampler(font.sampler, 0);
// 	
// 	graphics.setVertexBuffer(text.vb);
// 	graphics.setIndexBuffer(text.ib);
// 	
// 	auto tilegrid = alloc!TileGrid(graphics, 64, 48, 16, font);
// 	tilegrid.setTileCharacter(0, 0, ubvec4(0, 0, 0, 255), ubvec4(255), 'H');
// 	tilegrid.setTileCharacter(2, 0, ubvec4(0, 0, 0, 255), ubvec4(255), 'e');
// 	tilegrid.setTileCharacter(4, 0, ubvec4(0, 0, 0, 255), ubvec4(255), 'l');
// 	tilegrid.setTileCharacter(6, 0, ubvec4(0, 0, 0, 255), ubvec4(255), 'l');
// 	tilegrid.setTileCharacter(8, 0, ubvec4(0, 0, 0, 255), ubvec4(255), 'o');
// 	tilegrid.setTileCharacter(10, 0, ubvec4(0, 0, 0, 255), ubvec4(255), ' ');
// 	tilegrid.setTileCharacter(12, 0, ubvec4(0, 0, 0, 255), ubvec4(255), 'W');
// 	tilegrid.setTileCharacter(7*2, 0, ubvec4(0, 0, 0, 255), ubvec4(255), 'o');
// 	tilegrid.setTileCharacter(8*2, 0, ubvec4(0, 0, 0, 255), ubvec4(255), 'r');
// 	tilegrid.setTileCharacter(9*2, 0, ubvec4(0, 0, 0, 255), ubvec4(255), 'l');
// 	tilegrid.setTileCharacter(10*2, 0, ubvec4(0, 0, 0, 255), ubvec4(255), 'd');
// 	tilegrid.setTileCharacter(11*2, 0, ubvec4(0, 0, 0, 255), ubvec4(255), '!');
// 	tilegrid.setTileCharacter(12*2, 0, ubvec4(0, 0, 0, 255), ubvec4(255), '€');
// 	tilegrid.setTileCharacter(13*2, 0, ubvec4(0, 0, 0, 255), ubvec4(255), 'þ');
// 	tilegrid.setTileCharacter(14*2, 0, ubvec4(0, 0, 0, 255), ubvec4(255), 'ø');
// 	tilegrid.setTileCharacter(15*2, 0, ubvec4(0, 0, 0, 255), ubvec4(255), '→');
// 	tilegrid.setTileCharacter(16*2, 1, ubvec4(0, 0, 0, 255), ubvec4(255), '|');
// 	tilegrid.clearTile(1, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*1, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*2, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*3, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*4, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*5, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*6, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*7, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*8, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*9, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*10, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*11, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*12, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*13, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*14, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*15, 0, ubvec4(127, 127, 127, 255));
// 	tilegrid.clearTile(1 + 2*16, 0, ubvec4(127, 127, 127, 255));
// 	
// 	loadFile("resources/shaders/tiles_background.vs.glsl", arr);
// 	auto tilesBgVs = graphics.createVertexShader(arr[]);
// 	loadFile("resources/shaders/tiles_background.fs.glsl", arr);
// 	auto tilesBgFs = graphics.createFragmentShader(arr[]);
// 	
// 	loadFile("resources/shaders/tiles_foreground.vs.glsl", arr);
// 	auto tilesFgVs = graphics.createVertexShader(arr[]);
// 	loadFile("resources/shaders/tiles_foreground.fs.glsl", arr);
// 	auto tilesFgFs = graphics.createFragmentShader(arr[]);
	
	Timer timer = new Timer();
	Time dt;
	
	while(win.isOpen)
	{
		dt = timer.restart();
// 		writeln("DeltaTime: ", dt.as!float);
		
		graphics.beginFrame();
		
		graphics.clearRenderTarget(DefaultRenderTarget, ClearFlag.color | ClearFlag.depth | ClearFlag.stencil, bgColor);
		
// 		foreach(y; 0..64)
// 			foreach(x; 0..64)
// 				transforms[x + y * 64] = translate(transforms[x + y * 64], vec3(0.01, 0.01, 0));
// 		graphics.updateDynamicBuffer(transformDb, cast(ubyte[])transforms[]);
		
// 		graphics.bindDynamicUniformBuffer(transformDb, 1, mat4.sizeof * 1024, mat4.sizeof * 1024 * 0);
// 		graphics.drawIndexed(PrimitiveType.triangles, 1024, 0);
// 		graphics.bindDynamicUniformBuffer(transformDb, 1, mat4.sizeof * 1024, mat4.sizeof * 1024 * 1);
// 		graphics.drawIndexed(PrimitiveType.triangles, 1024, 0);
// 		graphics.bindDynamicUniformBuffer(transformDb, 1, mat4.sizeof * 1024, mat4.sizeof * 1024 * 2);
// 		graphics.drawIndexed(PrimitiveType.triangles, 1024, 0);
// 		graphics.bindDynamicUniformBuffer(transformDb, 1, mat4.sizeof * 1024, mat4.sizeof * 1024 * 3);
// 		graphics.drawIndexed(PrimitiveType.triangles, 1024, 0);
		
// 		import std.range : enumerate;
// 		foreach(i, e, r; ecs.join!(Renderable, TestComp).select!(Renderable).enumerate)
// 		{
// 			graphics.bindDynamicUniformBuffer(transformDb, 1, mat4.sizeof * 1024, cast(uint)(mat4.sizeof * 1024 * i));
// 			graphics.drawIndexed(PrimitiveType.triangles, 1024, 0);
// 		}
		
// 		graphics.bindDynamicUniformBuffer(transformDb, 1, mat4.sizeof * 1024, 0);
// 		graphics.drawIndexed(PrimitiveType.triangles, 1, 0);
		
// 		tilegrid.update();
		{
			
			// background
// 			graphics.setVertexShader(tilesBgVs);
// 			graphics.setFragmentShader(tilesBgFs);
// 			
// 			graphics.setVertexBuffer(tilegrid.backgroundVb);
// 			graphics.setIndexBuffer(tilegrid.backgroundIb);
			
// 			graphics.drawIndexed(PrimitiveType.triangles, 1, 0);
// 			
// 			// foreground
// 			graphics.bindTexture(tilegrid.fontTexture, 0);
// 			graphics.bindSampler(tilegrid.fontSampler, 0);
// // 			graphics.bindTexture(tilegrid.customTexture, 1);
// 			
// 			graphics.setVertexShader(tilesFgVs);
// 			graphics.setFragmentShader(tilesFgFs);
// 			
// 			graphics.setVertexBuffer(tilegrid.foregroundVb);
// 			graphics.setIndexBuffer(tilegrid.foregroundIb);
// 			
// 			graphics.drawIndexed(PrimitiveType.triangles, 1, 0);
		}
		
		// win.display();
		graphics.present();
		
		win.pollEvents();

		graphics.endFrame();
	}
	
	writeln("Transformed: ", orthoPP(1024.0f/2, 768.0f/2, -1, 1) * vec3(-1024.0f/2 + 32, -768.0f/2 + 32, 0).translate * vec4(0, 0, 0, 1));
	writeln("Transformed: ", orthoPP(1024.0f/2, 768.0f/2, -1, 1) * vec3(-1024.0f/2 + 32, -768.0f/2 + 32, 0).translate * vec4(64*16, 48*16, 0, 1));
	writeln("Ortho: ", ortho(-1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f));
	
	free(graphics);
	// free(win);
	free(core);
}

struct Vertex
{
	@Attribute(0, Attribute.Type.f32, 2, false)
	vec2 pos;
	
	@Attribute(1, Attribute.Type.f32, 2, false)
	Vector!(float, 2) uv;
}

void loadFile(in string filename, ref DynamicArray!ubyte arr)
{
	auto f = File(filename);
	auto size = f.size;
	arr.resize(size);
	f.rawRead(arr[]);
}

@Component
struct Renderable
{
	VertexBufferHandle vb;
	IndexBufferHandle ib;
}

@Component
struct Transform
{
	quat orientation;
	vec3 position;
}

@Component
struct TestComp
{
	
}
